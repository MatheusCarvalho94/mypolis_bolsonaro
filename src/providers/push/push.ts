import { HttpClient } from '@angular/common/http';
import { Injectable, ViewChild } from '@angular/core';
import { Nav, Platform, App, AlertController } from 'ionic-angular';
import {Observable} from 'rxjs/Observable';

import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { AuthProvider } from '../auth/auth';
import { CONFIG_PROJECT } from '../app-config';
import { NotificacoesNotificacoesDetalhePage } from '../../pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe';


@Injectable()
export class PushProvider {

  @ViewChild(Nav) nav: Nav;
  constructor(
    public http: HttpClient,
    public auth: AuthProvider,
    public app: App,
    public platform: Platform,
    public push: Push,
    private localNotifications: LocalNotifications
  ) {

    console.log('Hello PushProvider Provider');
  }
  initPush (){
    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }
    const options: PushOptions = {
      android: {
        senderID: '318490138469'

      },
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);

    pushObject.on('registration').subscribe((data: any) => {
      console.log('device token -> ' + data.registrationId);
      localStorage.setItem('TokenPush', JSON.stringify(data.registrationId))
      
      let plataforma = '';

      if (navigator.userAgent.match(/Android/i)) {
          plataforma = 'android';
      } else {
          plataforma = 'ios';
      }

      let body = {
        AppId : CONFIG_PROJECT.appId,
        Token :data.registrationId,
        Plataforma:plataforma
      };

      let url = `${CONFIG_PROJECT.baseApi}/Push/Register/`;
      var token = this.auth.getToken();
      let header = { "headers": { "authorization": 'bearer ' + token } };

      return new Promise((resolve, reject) => {
      this.http.post(url, body, header).map(res => res)
        .subscribe(
          data => {
           console.log(data, `retorno do token`)
          },
          err => {
            console.log('deuruim', err)
          }
        );
      })
    }); 
    // pushObject.on('notification').subscribe((notificacao: any) => {
    //     console.log('message -> ' + notificacao);  [];

    //   if (notificacao.additionalData.foreground==true) {
    //     this.localNotifications.schedule({
    //       title: 'Voluntários da Pátria',
    //       text: notificacao.message,
    //       icon: 'ic_notifications',
    //       smallIcon: 'ic_notification_small',
    //       id: notificacao.additionalData.MensagemCod
    //     });

    //   }else if(notificacao.additionalData.foreground==false){
    //     console.log(notificacao, 'LoginNotificacao')

    //   this.nav.push(NotificacoesNotificacoesDetalhePage, notificacao.additionalData.notificacaoId);
    //     // .push()
    //   }
    // });
    // pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
  }


}
