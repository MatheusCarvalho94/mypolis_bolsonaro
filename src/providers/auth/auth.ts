import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

import { Storage } from '@ionic/storage';

export class User {
  username: string;
  password: string;
}

@Injectable()
export class AuthProvider {

  constructor(public http: HttpClient, public storage: Storage) {}

  postLogin(credentials) {


    let url = `${CONFIG_PROJECT.baseApi}/token`;
    let body = `grant_type=password&username=${credentials.username}&password=${credentials.password}&appId=${CONFIG_PROJECT.appId}`;
    let header = { "headers": { "Content-Type": 'application/x-www-form-urlencoded' } };

    return new Promise((resolve, reject) => {
      this.http.post(url, body, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });

  }

  postResetPassWord(email: string) {


    return new Promise((resolve, reject) => {

      let url = `${CONFIG_PROJECT.baseApi}/pessoa/RecuperarSenha`;

      var data = {
        Email: email,
        ClienteId: CONFIG_PROJECT.clienteId
      };

      this.http.post(url, data)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });

    });
  }

  public saveSection(data) {
    localStorage.setItem('access_token', data.access_token);
    this.storage.set('access_token', data.access_token);
    
  }

  public isLogged(){
    if (this.getToken()) {
      return true;
    }
    return false;

  }

  public logoutUser() {
    localStorage.removeItem('profileName');
    localStorage.removeItem('access_token');
    this.storage.remove('profileName')
    this.storage.remove('access_token')
  }

  public getToken() {
    return localStorage.getItem('access_token');
    // return this.storage.get('access_token')

  }



}
