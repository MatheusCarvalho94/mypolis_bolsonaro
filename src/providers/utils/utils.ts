import { HttpClient } from '@angular/common/http';
import { Injectable, ViewChild } from '@angular/core';
import { AlertController, Platform, ModalController, Nav, App, NavController } from 'ionic-angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { ModalSucessoPage } from '../../pages/modal-sucesso/modal-sucesso';
import { Events } from 'ionic-angular/util/events';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { CONFIG_PROJECT } from '../app-config';

declare var cordova: any;

@Injectable()
export class UtilsProvider {
  @ViewChild(Nav) nav: Nav;
  constructor(
    public http: HttpClient,
    public alertCtrl: AlertController,
    public transfer: FileTransfer,
    protected app: App,
    public platform: Platform,
    public androidPermissions: AndroidPermissions,
    public modalCtrl: ModalController,
    public events: Events,
    private localNotifications: LocalNotifications,
  ) {
    this.initialize();
    
  }

  get navCtrl(): NavController {
    return this.app.getActiveNav();
  }

  private estados: any = null;

  initialize() {
    this.http.get('assets/estados-cidades.json')
      .subscribe((result: any) => {
        this.estados = result.estados;
      }, error => {
        console.error('Error: ', error);
      });
  }

  getEstados() {
    if (this.estados) {
      return new Promise((resolve, reject) => {
        resolve(this.estados);
      });
    }

    return new Promise((resolve, reject) => {
      let url = 'assets/estados-cidades.json';
      this.http.get(url)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result.estados);
        },
          (error) => {
            reject(error);
          });
    });
  }

  getCidades(uf: string) {
    if (this.estados) {
      return new Promise((resolve, reject) => {
        let cidades = [];
        for (var i = 0; i < this.estados.length; i++) {
          if (this.estados[i].sigla == uf) {
            cidades = this.estados[i].cidades;
            break;
          }
        }
        resolve(cidades);
      });
    }

    return new Promise((resolve, reject) => {
      let url = 'assets/estados-cidades.json';
      this.http.get(url)
        .map(res => res)
        .subscribe((result: any) => {
          let cidades = [];
          for (var i = 0; i < result.estados.length; i++) {
            if (result.estados[i].sigla == uf) {
              cidades = result.estados[i].cidades;
              break;
            }
          }
          resolve(cidades);
        },
          (error) => {
            reject(error);
          });
    });
  }

  getNameFromUf(uf: string) {
    let nome = uf;
    if (this.estados) {
      for (var i = 0; i < this.estados.length; i++) {
        if (this.estados[i].sigla == uf) {
          nome = this.estados[i].nome;
          break;
        }
      }
    }
    return nome;
  }

  getFromUf(uf: string) {
    let estado = null;
    if (this.estados) {
      for (var i = 0; i < this.estados.estados.length; i++) {
        if (this.estados.estados[i].sigla == uf) {
          estado = this.estados.estados[i];
          break;
        }
      }
    }
    return estado;
  }

  showAlert(text, message = undefined) {
    let alert = this.alertCtrl.create({
      title: "OK",
      buttons: [
        {
          text: 'Fechar',
          role: 'cancel'
        }
      ]
    })
    alert.setTitle(text);
    if (message != undefined && message != '' && message != null) {
      alert.setMessage(message);
    }
    alert.present();
  }

  showModalSucesso(titulo, mensagem, botao, clbk) {
    let modal = this.modalCtrl.create(ModalSucessoPage, { Titulo: titulo, Mensagem: mensagem, Btn: botao, error: false, callBack : clbk });
    modal.present();
  }

  showModalError(titulo, mensagem, botao) {
    let modal = this.modalCtrl.create(ModalSucessoPage, { Titulo: titulo, Mensagem: mensagem, Btn: botao, error: true });
    modal.present();
  }

  showModalLogin(titulo, mensagem, botao) {
    this.alertCtrl.create({
      // title: this.service.config.name,
      // subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
      // buttons: [{ text: 'Ok' }]
      title: titulo,
      subTitle: mensagem,
      buttons: [
        {
          text: 'Faça seu Login',
          role: 'login',
          handler: () => {
            // this.nav.setRoot();
          this.events.publish('clickLogin', true);
          }
        },
        {
          text: 'X',
          // text: '<i class="fa fa-times" aria-hidden="true"></i>',
          role: 'cancelar'
        }
      ],
      cssClass: 'alertLoginClass'
    }).present();
  }

  downloadFile(arquivo: any) {
    this.platform.ready().then(
      () => {
        if (!this.platform.is('cordova')) {
          return false;
        }
    
        let fileTransfer: FileTransferObject = this.transfer.create();
        let fileName = arquivo
        let directory = '';
    
        if (this.platform.is('ios')) {
          directory = cordova.file.documentsDirectory;
        }
        else if (this.platform.is('android')) {
          directory = cordova.file.externalRootDirectory + 'Dowload/';
        }
    
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
          .then(result => {
    
            console.log(result.hasPermission)
            if (!result.hasPermission) {
              this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
            }
            else {
    
              let downloadProgress = 0;
              fileTransfer.onProgress((progressEvent) => {
                let number = (progressEvent.loaded / progressEvent.total) * 100;
                downloadProgress = parseInt(parseFloat(number.toString()).toFixed(0));
    
                this.localNotifications.update(
                  {
                    id: 1,
                    text: downloadProgress + '%',
                    // progressBar: { value: downloadProgress }
                  }
                );
              });
              
    
              this.localNotifications.schedule({
                id: 1,
                title: 'Baixando Material..',
                text: 0 + '%',
                  smallIcon: 'res://iconedown',
                 icon: 'res://icon',
                // vibrate: true,
                sound: "file://img/ok.mp3",
                // progressBar: { enabled: true, value: 0 }
              });
    
              fileTransfer.download(arquivo.Url, directory + fileName).then((entry) => {
    
                cordova.plugins.notification.local.clearAll();
                cordova.plugins.notification.local.schedule({
                  vibrate: true, 
                  id:1,
                  title: 'Download Concluído',
                  text: '100%',
                    smallIcon: 'res://iconedown',
                   icon: 'res://icon',
                  lockscreen: true,
                  attachments: [entry.nativeURL],
                  // progressBar: { value: 100 }
                });

                
                cordova.plugins.notification.local.on('click', function (notification) {
                  window.open(encodeURI(entry.nativeURL), "_blank", "location=no,enableViewportScale=yes");
                 });
                // this.showModalSucesso('Download concluído', null, "OK", null);
    
              }, (error) => {
    
                this.showModalError('Erro no download', null, "OK");
                console.log(error);
                // handle error
              });
            }
          },
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
          );
      }
    );
  
  }

  cpf(cpf: string): boolean {
    if (cpf == null) {
      return false;
    }
    if (cpf.length != 11) {
      return false;
    }
    if ((cpf == '00000000000') || (cpf == '11111111111') || (cpf == '22222222222') || (cpf == '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') || (cpf == '66666666666') || (cpf == '77777777777') || (cpf == '88888888888') || (cpf == '99999999999')) {
      return false;
    }
    let numero: number = 0;
    let caracter: string = '';
    let numeros: string = '0123456789';
    let j: number = 10;
    let somatorio: number = 0;
    let resto: number = 0;
    let digito1: number = 0;
    let digito2: number = 0;
    let cpfAux: string = '';
    cpfAux = cpf.substring(0, 9);
    for (let i: number = 0; i < 9; i++) {
      caracter = cpfAux.charAt(i);
      if (numeros.search(caracter) == -1) {
        return false;
      }
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito1 = 11 - resto;
    if (digito1 > 9) {
      digito1 = 0;
    }
    j = 11;
    somatorio = 0;
    cpfAux = cpfAux + digito1;
    for (let i: number = 0; i < 10; i++) {
      caracter = cpfAux.charAt(i);
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito2 = 11 - resto;
    if (digito2 > 9) {
      digito2 = 0;
    }
    cpfAux = cpfAux + digito2;
    if (cpf != cpfAux) {
      return false;
    }
    else {
      return true;
    }
  }

  getCep(cep: any) {

    return new Promise((resolve, reject) => {
      this.http.get('https://viacep.com.br/ws/' + cep + '/json/')
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
          (error) => {
            reject(error);
          });
    });
  }

  getGeoLocation(lat,lng)
  { return new Promise((resolve, reject) => {
    let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=` + lat + `,` + lng + `&key=` + CONFIG_PROJECT.ApiKeyGoogleMaps+ `&sensor=true`;
    this.http.get(url)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
          (error) => {
            reject(error);
          });
    });
    
  }
}
