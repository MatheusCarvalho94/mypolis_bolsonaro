import { Injectable, Component } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Platform, AlertController } from 'ionic-angular';
import { ConteudoProvider } from '../conteudo/conteudo';
import { UtilsProvider } from '../utils/utils';
import { AppAvailability } from '@ionic-native/app-availability';


@Injectable()
export class SharedService {

    constructor(
        private SocialSharing: SocialSharing,
        public platform: Platform,
        private _alertCtrl: AlertController,
        public conteudoprovider: ConteudoProvider,
        private appAvailability: AppAvailability,
        public utils: UtilsProvider) {
        console.log('Hello SharedServiceProvider Provider');
    }


    SwitchShare(conteudo, rede) {
        switch (rede) {
            case 'facebook':
                this.shareViaFacebook(conteudo);
                break;
            case 'twitter':
                this.shareViaTwitter(conteudo);
                break;
            case 'instagram':
                this.shareViaInstagram(conteudo);
                break;
            case 'whatsapp':
                this.shareViaWhatsApp(conteudo);
                break;

            case 'facebookArquivo':
                this.shareViaFacebookArquivo(conteudo);
                break;
            case 'twitterArquivo':
                this.shareViaTwitterArquivo(conteudo);
                break;
            case 'instagramArquivo':
                this.shareViaInstagramArquivo(conteudo);
                break;
            case 'whatsappArquivo':
                this.shareViaWhatsAppArquivo(conteudo);
                break;
            default:
                this.sharePicker(conteudo);
                break;
        }
    }

    shareViaEmail(conteudo) {
        this.platform.ready()
            .then(() => {
                this.SocialSharing.canShareViaEmail()
                    .then(() => {   
                        this.SocialSharing.shareViaEmail(conteudo.message + ' ' + conteudo.UrlExterna, conteudo.subject, conteudo.sendTo)
                            .then((data) => {
                                console.log('Shared via Email');
                            })
                            .catch((err) => {
                                console.log('Not able to be shared via Email');
                            });
                    })
                    .catch((err) => {
                        console.log('Sharing via Email NOT enabled');
                    });
            });
    }

    
    shareViaFacebook(conteudo) {
        if(this.platform.is('android')){
             conteudo.Titulo += '#APPVoluntariosdaPatria17';
            this.SocialSharing.shareViaFacebookWithPasteMessageHint(conteudo.Titulo, conteudo.Imagem, conteudo.UrlExterna, conteudo.Titulo + conteudo.UrlExterna)
            .then((data) => {
                console.log('data', data)
                this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'facebook')
                .then((response: any) => {
                    setTimeout(() => {
                        this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                    }, 5000);
                })
            }).catch((error) => {
                console.log('Deu RUim', error)
            })
        }else{
            this.SocialSharing.shareViaFacebook(conteudo.Titulo + ' #APPVoluntariosdaPatria17', conteudo.Imagem, conteudo.UrlExterna)
            .then((data) => {
                this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'facebook')
                .then((response: any) => {
                    this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                })
            }).catch((error) => {
                console.log('caiu no erro', error)
            });
        }                
    }
    shareViaFacebookArquivo(conteudo) {
        this.SocialSharing.shareViaFacebook(conteudo.Titulo + '#APPVoluntariosdaPatria17', conteudo.ArquivoUrl, conteudo.ImagemUrl)
        .then((data) => {
            // Sharing via email is possible
            this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'facebook')
            .then((response: any) => {

                    if (this.platform.is('ios')) {
                        this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                    }else{
                        setTimeout(() => {
                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                        }, 5000);

                    }
            }, (error) => {
                console.log('não tem facebook')
            });
            console.log(data,'funcionando')
        },error => {
            console.log('nao funciona')
        });
}

    shareViaInstagram(conteudo) {
        this.platform.ready()
            .then(() => {
                this.SocialSharing.shareViaInstagram(conteudo.Titulo + ' ' +  conteudo.UrlExterna, conteudo.Imagem)
                    .then((data) => {
                        this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'instagram')
                        .then((response: any) => {
                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                        });
                        console.log('Shared via shareViaInstagram');
                    })
                    .catch((err) => {
                        console.log('Was not shared via Instagram');
                    });
            });
    }

    shareViaInstagramArquivo(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.shareViaInstagram(conteudo.Titulo + ' ' +  conteudo.ImagemUrl, conteudo.ArquivoUrl)
                    .then((data) => {
                        this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'instagram')
                        .then((response: any) => {
                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                        });
                        console.log('Shared via shareViaInstagram');
                    })
                    .catch((err) => {
                        console.log('Was not shared via Instagram');
                    });

            });
    }

    sharePicker(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.share(conteudo.Titulo, conteudo.UrlExterna, conteudo.Imagem)
                    .then((data) => {
                        console.log('Shared via SharePicker');
                    })
                    .catch((err) => {
                        console.log('Was not shared via SharePicker');
                    });

            });
    }

    shareViaTwitter(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.canShareVia('twitter', conteudo.Titulo  + '#APPVoluntariosdaPatria17'+ ' ' + conteudo.UrlExterna, conteudo.Imagem)
                    .then((data) => {

                        this.SocialSharing.shareViaTwitter(conteudo.Titulo, conteudo.Imagem)
                            .then((data) => {
                                this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'twitter')
                                    .then((response: any) => {
                                        // setTimeout(() => {
                                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                                        // }, 5000);
                                    }, (error) => {
                                        console.log('não tem twitter')
                                    });
                                console.log('Shared via Twitter');
                            })
                            .catch((err) => {
                                console.log('Was not shared via Twitter');
                            });

                    });

            })
            .catch((err) => {
                console.log('Not able to be shared via Twitter');
            });
    }

    shareViaTwitterArquivo(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.canShareVia('twitter', conteudo.Titulo + ' ' + conteudo.UrlExterna, conteudo.ImagemUrl)
                    .then((data) => {

                        this.SocialSharing.shareViaTwitter(conteudo.Titulo, conteudo.Imagem)
                            .then((data) => {
                                this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'twitter')
                                    .then((response: any) => {
                                        // setTimeout(() => {
                                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                                        // }, 5000);
                                    }, (error) => {
                                        console.log('não tem twitter')
                                    });
                                console.log('Shared via Twitter');
                            })
                            .catch((err) => {
                                console.log('Was not shared via Twitter');
                            });

                    });

            })
            .catch((err) => {
                console.log('Not able to be shared via Twitter');
            });
    }



    shareViaWhatsApp(conteudo) {
        this.SocialSharing.shareViaWhatsApp(conteudo.Titulo, conteudo.Imagem, conteudo.UrlExterna)
            .then((data) => {
                console.log(data, 'sucesso')
                this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId,'whatsapp')
                    .then((response: any) => {
                        // setTimeout(() => {
                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                        // }, 5000);
                    }, (error) => {
                        console.log('não tem whats')
                    });
                console.log('Shared via Twitter');
            }, (error) => {
                console.log(error,'não tem whats')
            })
    }

    shareViaWhatsAppArquivo(conteudo) {
        this.SocialSharing.shareViaWhatsApp(conteudo.Titulo, conteudo.ArquivoUrl. conteudo.ImagemUrl)
            .then((data) => {
                console.log(data, 'sucesso')
                this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId,'whatsapp')
                    .then((response: any) => {
                        // setTimeout(() => {
                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                        // }, 5000);
                    }, (error) => {
                        console.log('não tem whats')
                    });
                console.log('Shared via Twitter');
            }, (error) => {
                console.log(error,'não tem whats')
            })
    }
}