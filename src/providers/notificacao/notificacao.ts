import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class NotificationProvider {

  constructor(public http: HttpClient, public auth: AuthProvider) {}

  Count() {
    var token = this.auth.getToken();
    let url = `${CONFIG_PROJECT.baseApi}/Mensagens`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          let count = 0;

          for(var i =0; i < result.length; i++)
          {
            if(!result[i].Lido)
            {
              count++;
            }
          }
          resolve(count);
        },
        (error) => {
          resolve(0);
        });
    });
  }

  Get()
  {
    var token = this.auth.getToken();
    let url = `${CONFIG_PROJECT.baseApi}/Mensagens`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
            reject(error);
        });
    });
  }

  GetDetail(id: any)
  {
    var token = this.auth.getToken();
    let url = `${CONFIG_PROJECT.baseApi}/Mensagens/` + id;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
            reject(error);
        });
    });
  }
}