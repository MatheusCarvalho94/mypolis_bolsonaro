import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import { AuthProvider } from '../auth/auth';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

@Injectable()
export class PersonProvider {

  constructor(public http: HttpClient, private auth : AuthProvider, public storage: Storage) { }


  post(person) {
    
    let url = `${CONFIG_PROJECT.baseApi}/pessoa/cadastrobasico`;
   
    person.ClienteId = CONFIG_PROJECT.clienteId;

    return new Promise((resolve, reject) => {
      this.http.post(url, person)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });

  }

  get() {
    let url = `${CONFIG_PROJECT.baseApi}/pessoa`;
    var token = this.auth.getToken();
    let header = { "headers": { "authorization": 'bearer ' + token } };
 
    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
          localStorage.setItem('profileName', result.Nome)

          this.storage.set('profileName', result.Nome);

        },
        (error) => {
          reject(error);
        });
    });
  }

  put(user)
  {
    let url = `${CONFIG_PROJECT.baseApi}/pessoa/editar`;
    var token = this.auth.getToken();
    let header = { "headers": { "authorization": 'bearer ' + token } };

    user.ClienteId = CONFIG_PROJECT.clienteId;

    return new Promise((resolve, reject) => {
      this.http.post(url, user,header)
        .map(res => res)
        .subscribe((result: any) => {
          console.log(result)
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });

  }

  putFoto(image)
  {
    let url = `${CONFIG_PROJECT.baseApi}/pessoa/UploadFotoBase64`;
    var token = this.auth.getToken();
    let header = { "headers": { "authorization": 'bearer ' + token } };

    return new Promise((resolve, reject) => {
      this.http.post(url,  {Base64 : image},header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });

    

  }

  
}