import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UtilsProvider } from '../../providers/utils/utils';
import { ParticipacaoProvider } from '../../providers/participacao/participacao';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@Component({
  selector: 'page-participacao',
  templateUrl: 'participacao.html',
})
export class ParticipacaoPage {
  

 lista:any;
 nadaEncontrado:boolean = false
//  erro: any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public utils: UtilsProvider,
  private participacao: ParticipacaoProvider,
private loadingCtrl: LoadingController) {
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      
      refresher.complete();
    }, 1000);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ParticipacaoPage');
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    
    this.participacao.getAll().then((result:any)=> {
      this.lista = result;

      if(this.lista.length==0){
        this.nadaEncontrado=true
      }
      loading.dismiss();
    }, error=> {
      // let erro = error;
      this.lista = error.status;
  console.log(this.lista);
      loading.dismiss();
    });
  }

  openDetail(page, id) {
    this.navCtrl.push(page, id)
  }

  
}
