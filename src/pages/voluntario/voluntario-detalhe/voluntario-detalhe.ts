import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { IndiqueAmigosPage } from '../indique-amigos';
import { SearchbarComponent } from '../../../components/searchbar/searchbar';
import { VoluntarioProvider } from '../../../providers/voluntario/voluntario';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { PersonProvider } from '../../../providers/person/person';
import { UtilsProvider } from '../../../providers/utils/utils';
import { SocialSharePage } from '../../social-share/social-share';
import { ConteudoProvider } from '../../../providers/conteudo/conteudo';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage({
  name: 'voluntario-detalhe',
  segment: 'voluntario-detalhe/:id'
})
@Component({
  selector: 'page-voluntario-detalhe',
  templateUrl: 'voluntario-detalhe.html',
})
export class VoluntarioDetalhePage {

  vol: any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController,
     public voluntario: VoluntarioProvider, 
     private iab: InAppBrowser,
     public loadingCtrl: LoadingController,
     public utils: UtilsProvider,
     public person: PersonProvider,
     private _conteudo: ConteudoProvider,
    ) {
    this.getData();
    this.verificaBotao()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoluntarioDetalhePage');
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getData()
      refresher.complete();
    }, 1000);

  }

  openModal() {
    let modal = this.modalCtrl.create(IndiqueAmigosPage);
    modal.present();
  }

  searchToggle() {

    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
    
  }
 
  getData() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.voluntario.findById(this.navParams.data).then((response: Object) => {
      loading.dismiss();
      this.vol = response;
      if(this.vol.Url==`sharing('Venha ser um Voluntário da Pátria. Baixe o aplicativo e faça seu cadastro.')`){
        this.buttonenviar=true;
      }else{
        this.buttonenviar=false;
      }
      console.log(response);
    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

  postLike(conteudo) {

    let idPost = conteudo.Id;

    this.person.get()
      .then((response: any) => {

        let person = (response as any);

        this._conteudo.postLike(idPost, person.Id)
          .then((response: any) => {

            if (response.Curtiu && !conteudo.JaCurtiu) {
              conteudo.Curtidas += 1;
            } else {
              conteudo.Curtidas -= 1;
            }

            conteudo.JaCurtiu = response.Curtiu;

            if (response.GanhouPonto) {
              this.utils.showModalSucesso(response.Mensagem,"Continue interagindo" ,"OK", null);
            }
          });
      });
  }
  buttonenviar=false;
  verificaBotao(){
   
    
  }

  openExternal(url){
    if(url==null){
      console.log('Não tem link')
    }else{
      window.open(url, '_system', 'location=no');
    }
  }

  // postShare(conteudo) {
  //   conteudo.Imagem = 'http://mypolis.com.br/conteudo/voluntarios/postcompartilhar.jpg'
    
  //   let modal = this.modalCtrl.create(SocialSharePage, conteudo);
  //   modal.present();
  // }

  sharing(conteudo: any) {

    let conteudoBody = {
      Titulo: conteudo + '#APPVoluntariosdaPatria17',

      Imagem: 'https://mypolis.com.br/conteudo/voluntarios/postcompartilhar.jpg'
    }

    let modal = this.modalCtrl.create(SocialSharePage, conteudoBody);
    modal.present();
  }


}
