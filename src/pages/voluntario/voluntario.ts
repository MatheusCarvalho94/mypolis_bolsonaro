import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { SearchbarComponent } from '../../components/searchbar/searchbar';
import { VoluntarioProvider } from '../../providers/voluntario/voluntario';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { SocialSharePage } from '../social-share/social-share';
import { UtilsProvider } from '../../providers/utils/utils';
import { PersonProvider } from '../../providers/person/person';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { DetailProgramaPage } from '../detail-programa/detail-programa';

@Component({
  selector: 'page-voluntario',
  templateUrl: 'voluntario.html',
})
export class VoluntarioPage {
  listSejaVoluntario: any;
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
      public voluntario: VoluntarioProvider, 
      public loadingCtrl: LoadingController,
      public utils: UtilsProvider,
      public person: PersonProvider,
      public modalCtrl: ModalController,
      private _conteudo: ConteudoProvider ) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoluntarioPage');
  }

  openDetail(page, id) {
    this.navCtrl.push(page, id)
  }

  searchToggle() {

    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
    
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getData()
      refresher.complete();
    }, 1000);

  }
  getData()
  {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.voluntario.findAll().then((result: any) => {
      loading.dismiss();
      this.listSejaVoluntario = result;
      console.log(result);
    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

  postLike(conteudo) {

    let idPost = conteudo.Id;

    this.person.get()
      .then((response: any) => {

        let person = (response as any);

        this._conteudo.postLike(idPost, person.Id)
          .then((response: any) => {

            if (response.Curtiu && !conteudo.JaCurtiu) {
              conteudo.Curtidas += 1;
            } else {
              conteudo.Curtidas -= 1;
            }

            conteudo.JaCurtiu = response.Curtiu;

            if (response.GanhouPonto) {
              this.utils.showModalSucesso(response.Mensagem,"Continue interagindo" ,"OK", null);
            }
          });
      });
  }

  postShare(conteudo) {
 
    let modal = this.modalCtrl.create(SocialSharePage, conteudo);
    modal.present();
  }

  openDetails(l){
    this.navCtrl.push(DetailProgramaPage, {
      interna: l
    })
  }

  openExternal(url){
    
  }
}
