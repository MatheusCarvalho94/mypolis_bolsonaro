import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginLoginEsqueciPage } from './login-login-esqueci';

@NgModule({
  declarations: [
    LoginLoginEsqueciPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginLoginEsqueciPage),
  ],
})
export class LoginLoginEsqueciPageModule {}
