import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams, LoadingController, AlertController, Events } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';
import { Facebook } from '@ionic-native/facebook';
import { NotificationProvider } from '../../providers/notificacao/notificacao';
import { PersonProvider } from '../../providers/person/person';
import { Http,  Headers, RequestOptions } from '@angular/http';

import { CONFIG_PROJECT } from '../../providers/app-config';

@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  HomePage=HomePage
  user = {
    username: '',
    password: ''
  }
  profile: any;
  count: 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private fb: Facebook,
    public events: Events,
    public http: Http,
    public notification: NotificationProvider,
    public person: PersonProvider,
  ) { }

  ionViewDidLoad() { }

  goPage(page) {
    this.navCtrl.push(page);
  }
  public form = {
    RoleId: 2,
    Nome: '',
    Email: '',
    Numero: '',
    Senha: '',
    ConfirmarSenha: '',
    clienteId: 20471
  }

  Logado
  onLogin(user) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    if(this.user.username==''){
      this.alertCtrl.create({
        title: '<img src="assets/icon/error.png"/> Erro',
        subTitle: 'Usuario ou senha inválidos',
        buttons: [{ text: 'Ok' }],
        cssClass: 'alertcss'
      }).present();
      loading.dismiss();

    }else{
    this.auth.postLogin(user)
      .then((response) => {
        setTimeout(() => {
          this.auth.saveSection(response);
          loading.dismiss();
          this.events.publish('profile', response);
          this.navCtrl.setRoot(HomePage);
        }, 2000);
      }, (error) => {
        setTimeout(() => {
          this.alertCtrl.create({
            title: '<img src="assets/icon/error.png"/> Erro',
            subTitle: error.error.error_description,
            buttons: [{ text: 'Ok' }],
            cssClass: 'alertcss'
          }).present();
          loading.dismiss();

        }, 2000);
      });
    }
  }


  onHome(user) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    loading.present();
    user = {
      username: '',
      password: ''
    }
    this.auth.postLogin(user)
      .then((response) => {
        setTimeout(() => {
          this.auth.saveSection(response);
          this.events.publish('profile', response);
          loading.dismiss();
          this.navCtrl.setRoot(HomePage);
        }, 2000);
      }, (error) => {
        setTimeout(() => {
          this.alertCtrl.create({
            title: '<img src="assets/icon/error.png"/> Erro',
            subTitle: error.error.error_description,
            buttons: [{ text: 'Ok' }],
            cssClass: 'alertcss'
          }).present();
          loading.dismiss();

        }, 2000);
      });

  }

  loginToFacebook() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then(res => {
        if (res.status === "connected") {
          this.getUserDetail(res.authResponse.userID, res.authResponse.accessToken);
        } else {
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }
  
  InfosFb: any;
  getUserDetail(userid, usertoken) {
    this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
      .then(res => {
        console.log(res);
        this.InfosFb = res;
        let headers = new Headers({
          'Content-Type': 'application/x-www-form-urlencoded'
        });
       
        this.http.get(`${CONFIG_PROJECT.baseApi}` + '/Pessoa/VerificaFacebook?FacebookId='+this.InfosFb.id+ '&ClienteCod=20471').map(res => res.json())
        

          .subscribe((data) => {
            console.log(data)
            if (data.Existe == true) {

              let headers = new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
              });
           
              let body = `grant_type=password&username=facebook:${res.id}&password=${usertoken}&appId=${CONFIG_PROJECT.appId}`;

              return this.http.post(`${CONFIG_PROJECT.baseApi}` + '/token', body, { headers: headers }).map(res => res.json())
                .subscribe((data) => {
                  localStorage.setItem('access_token', data.access_token);

                  localStorage.setItem("cacheUsuario", JSON.stringify(data));

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    loading.present();
                  localStorage.removeItem('profile')
                  this.events.publish('user:Login', data);
                  this.navCtrl.setRoot(HomePage, {
                    // AparecerAlert: 'Apareça'
                  }
        
                  )
                  loading.dismiss();
                })
            } else {
              let headers = new Headers({
                'Content-Type': 'application/json'
              });
            
              let body = {
                Nome: this.InfosFb.name,
                Email: this.InfosFb.email,
                FacebookId: this.InfosFb.id,
                Origem: 'aplicativo',
                RoleId: '2',
                clienteId: 20471,
              }
              return this.http.post(`${CONFIG_PROJECT.baseApi}` + '/Pessoa/CadastroFacebook', body, { headers: headers }).map(res => res.json())

                .subscribe((data) => {
                  console.log(data);
              let headers = new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
              });
              let options = new RequestOptions({ headers: headers });
             
              let body = `grant_type=password&username=facebook:${res.id}&password=${usertoken}&appId=${CONFIG_PROJECT.appId}`;

              return this.http.post(`${CONFIG_PROJECT.baseApi}` + '/token', body, { headers: headers }).map(res => res.json())
                .subscribe((data) => {
                  localStorage.setItem('access_token', data.access_token);
                  let loading = this.loadingCtrl.create({
                    content: 'Carregando...'
                  });
              
                  loading.present();
                  localStorage.setItem("cacheUsuario", JSON.stringify(data));
                  localStorage.removeItem('profile')
                  this.events.publish('user:Login', data);
                  this.navCtrl.setRoot(HomePage, {
                    // AparecerAlert: 'Apareça'
                  }
                  )
                  loading.dismiss();
                })
                }, err => {
                  console.log("ERROR!: ", err);
                })
            }
          })
      })
      .catch(e => {
        console.log(e);
      });
  }

}

