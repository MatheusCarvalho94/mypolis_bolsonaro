import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilsProvider } from '../../providers/utils/utils';
import { PersonProvider } from '../../providers/person/person';
import { SocialSharePage } from '../social-share/social-share';

@IonicPage({
  name: 'noticias',
  segment: 'noticias/:id'
})
@Component({
  selector: 'page-noticias-noticia-detalhe',
  templateUrl: 'noticias-noticia-detalhe.html',
})
export class NoticiasNoticiaDetalhePage {

  public id: number;
  public conteudo: any = {};
  public noticiasList = [];
  private tipoConteudo = 2;
  private IsAnonimo:any = localStorage.getItem('profileName')

   public page = 1;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _conteudo: ConteudoProvider,
    public auth: AuthProvider,
    public loading: LoadingController,
    public utils: UtilsProvider,
    public person: PersonProvider,
    public modalCtrl: ModalController
  ) { }


  ionViewDidLoad() {

    this.loadDetail();
    this.loadNews();
  }

  loadDetail() {

    let loading = this.loading.create();

    loading.present();

    this.id = this.navParams.data.id;

    return new Promise(resolve => {

      this._conteudo.getDetalhe(this.auth.getToken(), this.id)
        .then(data => {

          this.conteudo = data;
          resolve(true);
          loading.dismiss();

        });

    });
  }

  loadNews() {

    return new Promise(resolve => {

      this._conteudo.getConteudo(this.auth.getToken(),this.page, this.tipoConteudo)
        .then((data: any) => {
          let noticias = [];
          for (var i = 0; i < data.length; i++) {
            if (data[i].Id != this.conteudo.Id) {
              noticias.push(data[i]);
            }
          }

          this.noticiasList = noticias;
          resolve(true);
        });
    });
  }

  openDetail(page, id) {
    this.navCtrl.push(page, { id: id })
  }

  postLike(conteudo) {
    if(this.IsAnonimo=='Anônimo'){
      this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
    }else{
    let idPost = conteudo.Id;

    this.person.get()
      .then((response: any) => {

        let person = (response as any);

        this._conteudo.postLike(idPost, person.Id)
          .then((response: any) => {

            if (response.Curtiu && !conteudo.JaCurtiu) {
              conteudo.Curtidas += 1;
            } else {
              conteudo.Curtidas -= 1;
            }

            conteudo.JaCurtiu = response.Curtiu;

            if (response.GanhouPonto) {
              this.utils.showModalSucesso(response.Mensagem,"Continue interagindo" ,"OK", null);
            }
          });
      });
    }
  }

  postShare(conteudo) {
    if(this.IsAnonimo=='Anônimo'){
      this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
    }else{
 
      console.log(conteudo)
    let modal = this.modalCtrl.create(SocialSharePage, conteudo);
    modal.present();
    }
  }
}
