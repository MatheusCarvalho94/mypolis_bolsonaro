import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailTransmissaoPage } from './detail-transmissao';

@NgModule({
  declarations: [
    DetailTransmissaoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailTransmissaoPage),
  ],
})
export class DetailTransmissaoPageModule {}
