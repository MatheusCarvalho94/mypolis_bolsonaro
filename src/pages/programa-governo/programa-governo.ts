import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ConvideAmigosPage } from './convide-amigos';
import { GeralProvider } from '../../providers/geral/geral';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Conteudo } from '../../models/Conteudo';
import { SocialSharePage } from '../social-share/social-share';
import { UtilsProvider } from '../../providers/utils/utils';
import { PersonProvider } from '../../providers/person/person';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { DetailProgramaPage } from '../detail-programa/detail-programa';

@IonicPage()
@Component({
  selector: 'page-programa-governo',
  templateUrl: 'programa-governo.html',
})
export class ProgramaGovernoPage {

  programa: any = [];
  

  constructor(public navCtrl: NavController,
     public navParams: NavParams, 
     public modalCtrl: ModalController, 
     public geral: GeralProvider, 
     public loadingCtrl: LoadingController,
     public utils: UtilsProvider,
     public person: PersonProvider,
     private _conteudo: ConteudoProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProgramaGovernoPage');
    this.getPrograma();

  }
  
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getPrograma()
      refresher.complete();
    }, 1000);

  }

  inviteFriends() {
    let modal = this.modalCtrl.create(ConvideAmigosPage);
    modal.present();
  }

  getPrograma() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
      
    this.geral.findPrograma()
        .then(data => {
      loading.dismiss();
      this.programa = this.programa.concat(data);
      console.log(this.programa, 'Progrmaa');
      console.log(data, 'Programa ssss');

    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

  postLike(conteudo) {

    let idPost = conteudo.Id;

    this.person.get()
      .then((response: any) => {

        let person = (response as any);

        this._conteudo.postLike(idPost, person.Id)
          .then((response: any) => {

            if (response.Curtiu && !conteudo.JaCurtiu) {
              conteudo.Curtidas += 1;
            } else {
              conteudo.Curtidas -= 1;
            }

            conteudo.JaCurtiu = response.Curtiu;

            if (response.GanhouPonto) {
              this.utils.showModalSucesso(response.Mensagem,"Continue interagindo" ,"OK", null);
            }
            
          });
      });
  }

  postShare(conteudo) {

    let modal = this.modalCtrl.create(SocialSharePage, conteudo);
    modal.present();
  }

  openDetails(l){
    this.navCtrl.push(DetailProgramaPage, {
      interna: l
    })
  }
}
