import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificacoesPage } from './notificacoes';
import { PipesModule } from '../../pipes/pipes.module';


@NgModule({
  declarations: [
    NotificacoesPage,

  ],
  imports: [
    IonicPageModule.forChild(NotificacoesPage),
    PipesModule

  ],
})
export class NotificacoesPageModule {}
