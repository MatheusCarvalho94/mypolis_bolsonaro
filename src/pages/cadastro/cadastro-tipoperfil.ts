import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'cadastro-tipoperfil'
})
@Component({
  selector: 'page-cadastrotipoperfil',
  templateUrl: 'cadastro-tipoperfil.html',
})
export class CadastroTipoPerfilPage {
numero: any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroPage');
  }

  goPage(page) {
    this.navCtrl.push(page)
  }
  goEtapa(page, numero){
    this.navCtrl.push(page, numero)
    localStorage.setItem('numero-cadastro', numero);
  }

}
