import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController, Events } from 'ionic-angular';
import { PersonProvider } from '../../providers/person/person';
import { UtilsProvider } from '../../providers/utils/utils';
import { HomePage } from '../home/home';
import { Facebook } from '@ionic-native/facebook';

import { BrMaskerModule } from 'brmasker-ionic-3';


import { CONFIG_PROJECT } from '../../providers/app-config';



import { Http,  Headers, RequestOptions } from '@angular/http';

@IonicPage({
  name: 'cadastro'
})
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',


})
@NgModule({
  imports: [
    BrMaskerModule,
    
  ]
})

export class CadastroPage {
  
  Formulario = false;
  Sucesso = false;


  public form = {
    RoleId: 2,
    Nome: '',
    Email: '',
    Numero: '',
    Senha: '',
    ConfirmarSenha: '',
    clienteId: 20471,
    Origem: 'APP Voluntários da Pátria'
  }


  profileFB: any
  numeroPerfil: any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public person: PersonProvider,
    public http: Http,
    private fb: Facebook,

    public utils: UtilsProvider,
    public events: Events,
    public modalCtrl: ModalController) {

      }
    

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroPage');
  }

  goPage(page) {
    this.navCtrl.push(page)
  }

  onRegister() {

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    let numeroPerfil = parseInt(localStorage.getItem('numero-cadastro'));
    let headers = new Headers({})
    console.log(headers)
    let options = new RequestOptions({ headers: headers });
    loading.present();
   let body = {
      Nome: this.form.Nome,
      Email: this.form.Email,
      Celular: this.form.Numero,
      Senha: this.form.Senha,
      ConfirmarSenha: this.form.ConfirmarSenha,
      RoleId: numeroPerfil,
      clienteId: 20471,
      Origem: 'APP Voluntários da Pátria',
      // numeroPerfil: numeroPerfil
      
  }

  return this.http.post(`${CONFIG_PROJECT.baseApi}` + '/Pessoa/CadastroBasico', body, options).map(res => res.json())
      .subscribe((data) => {
        if (data.success == true) {
          loading.dismiss(),
          this.utils.showModalSucesso(data.Mensagem, null, "Iniciar", null);
          setTimeout(() => {
       this.VoltarHomeLogado();
          }, 3000);
          return false;
        } else {
          this.alertCtrl.create({
            title: '<img src="assets/icon/error.png"/> Erro',
            subTitle: data.Descricao,
            buttons: [{ text: 'Ok' }],
            cssClass: 'alertcss'
          }).present();

          loading.dismiss();
        }
      }
      , err => {
        console.log("ERROR!: ", err);
        //  this.loading.dismiss();ion
        var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
          if (mensagem.indexOf('encontra cadastrado') != -1) {
            this.alertCtrl.create({
              title: '<img src="assets/icon/error.png"/> ERRO',
              subTitle: "Esse e-mail já se encontra cadastrado. Informe outro e-mail ou <span (click)='EsqueciPage()'>clique aqui</span> caso tenha esquecido sua senha.",
              buttons: [{ text: 'Ok' }],
              cssClass: 'alertclasse'
            }).present();

            loading.dismiss()
          }
          else {
          this.alertCtrl.create({
            title: '<img src="assets/icon/error.png" /> Erro',
            subTitle: mensagem,
            buttons: [{ text: 'Ok' }],
            cssClass: 'alertcss'
          }).present();
            loading.dismiss()
          }
      }
    );
  }
Logado
  VoltarHomeLogado(){
    let url = `${CONFIG_PROJECT.baseApi}/token`;

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({ headers: headers });

    let body = `grant_type=password&username=${this.form.Email}&password=${this.form.Senha}&appId=${CONFIG_PROJECT.appId}`;


    return this.http.post(url, body, options).map(res => res.json())
      .subscribe((data) => {
        this.Logado = true;
        localStorage.setItem('access_token', data.access_token);

        localStorage.removeItem('profile')
        this.events.publish('user:Login', data, this.Logado);
        this.navCtrl.setRoot(HomePage);
        console.log('caiuno if')
      }, err => {
        console.log("ERROR!: ", err);

      }      
      )}



      loginToFacebook() {
        this.fb.login(['public_profile', 'user_friends', 'email'])
          .then(res => {
            if (res.status === "connected") {
              this.getUserDetail(res.authResponse.userID, res.authResponse.accessToken);
            } else {
            }
          })
          .catch(e => console.log('Error logging into Facebook', e));
      }
      InfosFb: any;
      getUserDetail(userid, usertoken) {
let numeroPerfil = parseInt(localStorage.getItem('numero-cadastro'))

        this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
          .then(res => {
            console.log(res);
            this.InfosFb = res;
            let headers = new Headers({
              'Content-Type': 'application/x-www-form-urlencoded'
            });
           
            this.http.get(`${CONFIG_PROJECT.baseApi}` + '/Pessoa/VerificaFacebook?FacebookId='+this.InfosFb.id+ '&ClienteCod=20471').map(res => res.json())
            
    
              .subscribe((data) => {
                console.log(data)
                if (data.Existe == true) {
    
                  let headers = new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded'
                  });
               
                  let body = `grant_type=password&username=facebook:${res.id}&password=${usertoken}&appId=${CONFIG_PROJECT.appId}`;
    
                  return this.http.post(`${CONFIG_PROJECT.baseApi}` + '/token', body, { headers: headers }).map(res => res.json())
                    .subscribe((data) => {
                      localStorage.setItem('access_token', data.access_token);
    
                      localStorage.setItem("cacheUsuario", JSON.stringify(data));
                      localStorage.removeItem('profile')
                      this.events.publish('user:Login', data);
                      this.navCtrl.setRoot(HomePage, {
                        // AparecerAlert: 'Apareça'
                      }
                      )
                    })
                } else {
                  let headers = new Headers({
                    'Content-Type': 'application/json'
                  });
                
                  let body = {
                    Nome: this.InfosFb.name,
                    Email: this.InfosFb.email,
                    FacebookId: this.InfosFb.id,
                    Origem: 'APP Voluntários da Pátria',
                    RoleId: numeroPerfil,                   
                    clienteId: 20471,
                  }
                  return this.http.post(`${CONFIG_PROJECT.baseApi}` + '/Pessoa/CadastroFacebook', body, { headers: headers }).map(res => res.json())
    
                    .subscribe((data) => {
                      console.log(data);
                  let headers = new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded'
                  });
                  let options = new RequestOptions({ headers: headers });
                 
                  let body = `grant_type=password&username=facebook:${res.id}&password=${usertoken}&appId=${CONFIG_PROJECT.appId}`;
    
                  return this.http.post(`${CONFIG_PROJECT.baseApi}` + '/token', body, { headers: headers }).map(res => res.json())
                    .subscribe((data) => {
                      localStorage.setItem('access_token', data.access_token);
    
                      localStorage.setItem("cacheUsuario", JSON.stringify(data));
                      localStorage.removeItem('profile')
                      this.events.publish('user:Login', data);
                      this.navCtrl.setRoot(HomePage, {
                        // AparecerAlert: 'Apareça'
                      }
                      )
                    })
                    }, err => {
                      console.log("ERROR!: ", err);
                    })
                }
              })
          })
          .catch(e => {
            console.log(e);
          });
      }

}
// @Component({
//   selector: 'modal-cadastro',
//   templateUrl: 'cadastro-modal.html'
// })
// export class CadastroModalPage {
//   // pesquisaDetail: any;
//   constructor(params: NavParams, public navCtrl: NavController, public loadingCtrl: LoadingController, public utils: UtilsProvider) {

//     }

//   close() {
//     this.navCtrl.pop();
//   }


// }

