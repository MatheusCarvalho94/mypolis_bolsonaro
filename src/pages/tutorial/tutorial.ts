import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, Events, LoadingController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {
  user = {
    username: '',
    password: ''
  }
  hiddenNext:boolean = false

  @ViewChild(Slides) slides: Slides;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private auth: AuthProvider,
    public events: Events,
  ) {}

  ionViewDidLoad() {}

  goState(user) {
    
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    loading.present();
    user = {
      username: '',
      password: ''
    }
    this.auth.postLogin(user)
      .then((response) => {
        setTimeout(() => {
          this.auth.saveSection(response);
          this.events.publish('profile', response);
          loading.dismiss();
          this.navCtrl.setRoot(HomePage);
        }, 2000);
      });

  }


  goToSlide() {

    this.slides.slideNext()
  }

  slideChanged() {

    this.slides.isEnd() == true ? this.hiddenNext = true : this.hiddenNext = false

  }
  
}
