import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, AlertController } from 'ionic-angular';
import { InstitucionalProvider } from '../../providers/institucional/institucional';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { UtilsProvider } from '../../providers/utils/utils';
import { SocialShareMaterialPage } from '../social-share-material/social-share-material';
import {File} from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { SocialSharePage } from '../social-share/social-share';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-material-institucional',
  templateUrl: 'material-institucional.html',
  providers: [PhotoLibrary]
})
export class MaterialInstitucionalPage {

  listMaterial: any;
  private IsAnonimo:any = localStorage.getItem('profileName')


  constructor(public navCtrl: NavController,
     public navParams: NavParams, 
     public loadingCtrl: LoadingController, 
     public material: InstitucionalProvider, 
     public platform: Platform,
     private photoLibrary: PhotoLibrary,
     public file: File,
     public transfer: FileTransfer,
     public alertCtrl: AlertController,
     public utils: UtilsProvider,
     public modalCtrl: ModalController) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterialInstitucionalPage');
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
     
      infiniteScroll.complete();
    }, 500);
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getData()
      refresher.complete();
    }, 1000);

  }
  getData()
  {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.material.findAcervo().then((result: any) => {
      loading.dismiss();
      this.listMaterial = result;
      console.log(result);
    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

  doDownload(imagem)
  {

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present()
    if(this.IsAnonimo=='Anônimo'){
      this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
      loading.dismiss()
    }else{
      const fileTransfer: FileTransferObject = this.transfer.create();
      let directory = '';
      let fileName = imagem.substring(imagem.lastIndexOf('/') + 1);
      if (this.platform.is('ios')) {
        directory = this.file.documentsDirectory;
      }
      else if (this.platform.is('android')) {
        directory = this.file.externalRootDirectory + 'Download/';
      }
      this.photoLibrary.requestAuthorization().then(() => {
      this.photoLibrary.saveImage(imagem, 'Downloads').then((data) => {
          console.log(data, 'data')
          loading.dismiss()
          const alertFailure = this.alertCtrl.create({
            title: `Download Concluído!`,
            buttons: ['Ok']
          });
          alertFailure.present();
          }, (err) => {
          loading.dismiss()
            console.log('Failure', err)
            const alertFailure = this.alertCtrl.create({
              title: `Falha no Download!`,
              buttons: ['Ok']
            });
            alertFailure.present();
        });
      });
  }
}
  getFile(arquivo:any)
{
    this.utils.downloadFile(arquivo);
    
  }

   postShare(conteudo) {
    if(this.IsAnonimo=='Anônimo'){
      this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
    }else{
      conteudo.AcervoId = conteudo.Id;
      let modal = this.modalCtrl.create(SocialSharePage, conteudo);
      modal.present();
    }
  }

}
