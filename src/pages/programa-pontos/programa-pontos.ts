import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { PontoProvider } from '../../providers/ponto/ponto';
import { PersonProvider } from '../../providers/person/person';
import { Events } from 'ionic-angular';
import { PesquisaPage } from '../pesquisa/pesquisa';
import { SocialSharing } from '@ionic-native/social-sharing';
import { UtilsProvider } from '../../providers/utils/utils';

import { SharedService } from '../../providers/shared-service/shared-service';
import { SocialSharePage } from '../social-share/social-share';

@IonicPage()
@Component({
  selector: 'page-programa-pontos',
  templateUrl: 'programa-pontos.html',
  providers: [SocialSharing, SharedService]
})
export class ProgramaPontosPage {

  tab: string = 'como-funciona';
  extrato: any;
  medalhas: any;
  TotalPontos: any;
  MedalhaAtual: any;
  private IsAnonimo:any = localStorage.getItem('profileName')

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public pontos: PontoProvider,
    public loadingCtrl: LoadingController,
    public utils: UtilsProvider,
    public pessoa: PersonProvider,
    public events: Events) {
    if (navParams.data != 0) {
      this.tab = navParams.data;
      this.events.publish('menu:closed', '');
    }
    this.loadAll();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProgramaPontosPage');
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.loadAll()
      refresher.complete();
    }, 1000);

  }
  loadExtrato() {
    this.pontos.getExtrato().then((result: any) => {
      this.extrato = result;
      console.log(result);
    }, (error) => {
    });
  }

  loadMedalhas() {
    this.pontos.getMedalhas().then((result: any) => {
      this.medalhas = result;
      console.log(result);
    }, (error) => {

    });
  }

  loadAll() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.getPersonDetail();
    this.loadExtrato();
    this.loadMedalhas();

    loading.dismiss();
  }

  getPersonDetail() {
    this.pessoa.get().then((response: any) => {

      this.TotalPontos = response.TotalPontos;

      this.MedalhaAtual = response.Medalha ? response.Medalha.Nome : "";

    }, (error) => {
      console.log(error);
    });
  }

  openDetail() {
    if(this.IsAnonimo=='Anônimo'){
      this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
    }else{
    this.navCtrl.push('perfil');
    }
  }

  openPesquisa() {
    if(this.IsAnonimo=='Anônimo'){
      this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
    }else{
    this.navCtrl.push(PesquisaPage);
    }
  }

  sharing(conteudo) {
    if(this.IsAnonimo=='Anônimo'){
      this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
    }else{
    let conteudoBody = {
      Titulo: conteudo + '#APPVoluntariosdaPatria17',
      Id :10067,
      Imagem: 'http://mypolis.com.br/conteudo/voluntarios/postcompartilhar.jpg'
    }

    let modal = this.modalCtrl.create(SocialSharePage, conteudoBody);
    modal.present();
    }
  }

}
