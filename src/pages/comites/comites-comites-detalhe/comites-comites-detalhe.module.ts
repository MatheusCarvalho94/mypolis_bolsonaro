import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComitesComitesDetalhePage } from './comites-comites-detalhe';

@NgModule({
  declarations: [
    ComitesComitesDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(ComitesComitesDetalhePage),
  ],
})
export class ComitesComitesDetalhePageModule {}
