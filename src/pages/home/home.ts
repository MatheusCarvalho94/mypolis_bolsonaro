import { Component } from '@angular/core';
import { NavController, ActionSheetController, Platform, ModalController, NavParams, LoadingController } from 'ionic-angular';
import { SearchbarComponent } from '../../components/searchbar/searchbar';
import { AgendaProvider } from '../../providers/agenda/agenda';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { PersonProvider } from '../../providers/person/person';
import { PushProvider } from '../../providers/push/push';

import { SocialSharing } from '@ionic-native/social-sharing';
import { SharedService } from '../../providers/shared-service/shared-service';
import { SocialSharePage } from '../social-share/social-share';
import { ComitesPage } from '../../pages/comites/comites';
import { QueroContribuirPage } from "../../pages/quero-contribuir/quero-contribuir";
import { VoluntarioPage } from '../../pages/voluntario/voluntario';
import { NotificacoesPage } from '../../pages/notificacoes/notificacoes';
import { ParticipacaoPage } from '../../pages/participacao/participacao';
import { PesquisaPage } from '../../pages/pesquisa/pesquisa';
import { ProgramaGovernoPage } from '../../pages/programa-governo/programa-governo';
import { MaterialInstitucionalPage } from '../../pages/material-institucional/material-institucional';
import { ProgramaPontosPage } from '../../pages/programa-pontos/programa-pontos';
import { NotificationProvider } from '../../providers/notificacao/notificacao';
import { Events } from 'ionic-angular';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { DetailTransmissaoPage } from '../detail-transmissao/detail-transmissao';
import { Storage } from '@ionic/storage';
import { NotificacoesNotificacoesDetalhePage } from '../notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe';
declare var cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [SocialSharing, SharedService]
})
export class HomePage {
  cordova: any;
  tab = "noticias";
  agendaList: any = [];
  noticiasList: any = [];
  private pagina1: number = 1;
  private paginaBanner1: number = 1;
  private pagina2: number = 1;
  private start: number = 3;
  private skip: number = 0;
  profile: any;
  count: 0;
  public IsAnonimo:any = this.storage.get('profileName')

  FiltroEventos = [
    { name: 'Todos', id: 0, active: true },
    { name: 'Hoje', id: 1, active: false },
    { name: 'Essa Semana', id: 2, active: false },
    { name: 'Esse mês', id: 3, active: false },
  ];


  pages = [
    { title: 'Quero contribuir', component: QueroContribuirPage, icon: 'logo-usd', description: null, cssClass: 'quero-contribuir', parameter: 0 },
    { title: 'Início', component: HomePage, icon: 'home', description: 'Notícias, eventos, agenda...', cssClass: 'iniciocss', parameter: 0 },
    { title: 'Notificações', component: NotificacoesPage, icon: 'notifications', description: null, notificationsNumber: this.count, parameter: 0 },
    { title: 'Programa de Governo', component: ProgramaGovernoPage, icon: 'list', description: null, parameter: 0 },
    { title: 'Seja Voluntário', component: VoluntarioPage, icon: 'hand', description: null, parameter: 0 },
    { title: 'Minha Participação', component: ParticipacaoPage, icon: 'person', description: null, parameter: 0 },
    { title: 'Pesquisa de Opinião', component: PesquisaPage, icon: 'text', description: null, parameter: 0 },
    { title: 'Programa de Pontos', component: ProgramaPontosPage, icon: 'star', description: 'Como funciona, histórico de pontos...', cssClass: 'programacss', parameter: 0 },
    { title: 'Material de Campanha', component: MaterialInstitucionalPage, icon: 'images', description: 'Banners, post...', cssClass: 'materialcss', parameter: 0 },
    { title: 'Comitês', component: ComitesPage, icon: 'people', description: null, parameter: 0 }
  ];
  // Tipo de Conteudo Noticias
  private tipoConteudo = 2;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionsheetCtrl: ActionSheetController,
    public platform: Platform,
    public modalCtrl: ModalController,
    public agenda: AgendaProvider,
    public auth: AuthProvider,
    public utils: UtilsProvider,
    public conteudo: ConteudoProvider,
    public person: PersonProvider,
    public notification: NotificationProvider,
    public pushProvider: PushProvider,
    private localNotifications: LocalNotifications,
    public push: Push,
    public events: Events,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public androidPermissions: AndroidPermissions,
  ) {
    this.loadDataUser();
    this.storage.get('profileName').then((val) => {
      console.log('Your age is', val);
      this.IsAnonimo=val
      
    });

    events.subscribe('profile', (profile) => {
      this.storage.get('profileName').then((val) => {
        console.log('Your age is', val);
        this.IsAnonimo=val
        if(this.IsAnonimo!='Anônimo'){
          this.pushProvider.initPush()
          const options: PushOptions = {
            android: {
              senderID: '318490138469'
            },
            ios: {
              alert: 'true',
              badge: false,
              sound: 'true'
            },
            windows: {}
          };
      
          const pushObject: PushObject = this.push.init(options);
          pushObject.on('notification').subscribe((notificacao: any) => {
            console.log('message -> ' + notificacao);  [];
  
          if (notificacao.additionalData.foreground==true) {
            this.localNotifications.schedule({
              title: 'Voluntários da Pátria',
              text: notificacao.message,
              icon: 'ic_notifications',
              smallIcon: 'ic_notification_small',
              id: notificacao.additionalData.notificacaoId, 
            });
            // this.localNotifications.on("click", (notification, state) => {
            //   this.navCtrl.setRoot(NotificacoesNotificacoesDetalhePage, notification.id);
            // })

          }else if(notificacao.additionalData.foreground==false){
            console.log(notificacao, 'LoginNotificacaoCOMPONENTS')
            this.navCtrl.setRoot(NotificacoesNotificacoesDetalhePage, notificacao.additionalData.notificacaoId);
          }
        });
        pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
        }
      });
    });
  }


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.loadAgenda()
      this.loadNews()
      refresher.complete();
    }, 1000);

  }


  openDetail(page, id) {
    this.navCtrl.push(page, { id: id });
  }

  optionsContribuition() {
    let actionSheet = this.actionsheetCtrl.create({
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Colabore para mudanças',
          icon: 'ios-thumbs-up-outline',
          handler: () => {
            this.navCtrl.setRoot(VoluntarioPage);
          }
        },
        {
          text: 'Responda uma pesquisa',
          icon: 'ios-paper-outline',
          handler: () => {
            this.navCtrl.push(PesquisaPage);

          }
        },
        {
          text: 'Participe enviando uma sugestão',
          icon: 'ios-text-outline',
          handler: () => {
            this.navCtrl.push(ParticipacaoPage);
          }
        },
        {
          text: 'Conheça a proposta de governo',
          icon: 'ios-search-outline',
          handler: () => {
            this.navCtrl.push(ProgramaGovernoPage);


            ProgramaGovernoPage
          }
        },
        {
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  searchToggle() {

    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();

  }


  bannerConteudo=32
  bannerLiveConteudo=33
  banners: any
  bannerslive:any = []
  bannersAll:any = []
  
  loadBanner() {
    var auxList = []
    return new Promise(resolve => {
      this.conteudo.getConteudo(this.auth.getToken(), this.paginaBanner1, this.bannerConteudo)
        .then(data => {
          this.banners=data
          this.conteudo.getConteudo(this.auth.getToken(), this.paginaBanner1, this.bannerLiveConteudo)
            .then(data2 => {
              // console.log('banner live', data2)
              this.bannerslive=data2;
              for (var x = 0; x < this.bannerslive.length; x++) {
                auxList.push(this.bannerslive[x]);
              }
              for (var z = 0; z < this.banners.length; z++) {
                auxList.push(this.banners[z]);
              }
              this.bannersAll=auxList;

              // this.bannerslive.push(this.banners)

              // this.bannersAll=this.bannerslive
              console.log('banners all', this.bannersAll)
          })
        });
    });
  }

  


  loadNews() {

    return new Promise(resolve => {

      this.conteudo.getConteudo(this.auth.getToken(), this.pagina1, this.tipoConteudo)
        .then(data => {

          this.noticiasList = this.noticiasList.concat(data);
          console.log(data);
          resolve(true);

        });

    });

  }

  loadAgenda() {

    return new Promise(resolve => {

      this.agenda.getLista(this.auth.getToken(), this.pagina2,3)
        .then(data => {

          this.agendaList = this.agendaList.concat(data);

          resolve(true);

        });

    });
  }

  doInfinite(infiniteScroll: any, tab) {



    setTimeout(() => {

      if (this.tab == 'agenda') {

        this.pagina2 += 1;
        this.loadAgenda().then(() => {
          infiniteScroll.complete();
        });
      }
      else {
        this.pagina1 += 1;
        this.loadNews().then(() => {
          infiniteScroll.complete();
        });
      }

    }, 600);


  }

  estados = new Array();
  cidades: any;

  filter = {
    cidade: '',
    estado: ''
  }

  selectOptEstados = {
    title: 'Selecione o estado',
    subTitle: 'Selecione o estado',
    checked: true
  }

  selectOptCidades = {
    title: 'Selecione a cidade',
    subTitle: 'Selecione a cidade',
    checked: true
  }

  onSelectCidade(uf) {
    this.utils.getCidades(uf).then((result: any) => { this.cidades = result; }, (error) => { });
  }

  getEstados() {
    this.utils.getEstados().then((result: any) => { this.estados = result; }, (error) => { });
  }

  filterByCity(filter) {

    return new Promise(resolve => {

      this.agenda.filterCity(this.auth.getToken(), filter, this.start, this.skip)
        .then(data => {


          if (data != null) {

            this.agendaList = data;
          }
          else {

            this.utils.showAlert("Nenhum resultado encontrado!");
            this.start = 2;
          }

          resolve(true);
        });
    });
  }

  postLike(conteudo) {
    if(this.IsAnonimo=='Anônimo'){
      this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
    }else{
      let idPost = conteudo.Id;
      if(this.auth.getToken()){
        
      this.person.get()
      .then((response: any) => {

        let person = (response as any);

        this.conteudo.postLike(idPost, person.Id)
          .then((response: any) => {

            if (response.Curtiu && !conteudo.JaCurtiu) {
              conteudo.Curtidas += 1;
            } else {
              conteudo.Curtidas -= 1;
            }

            conteudo.JaCurtiu = response.Curtiu;

            if (response.GanhouPonto) {
              this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
            }
          });
      });
      }else{
        console.log('nao ta logado')
      }
    }
  }

  ionViewDidLoad() {
    this.loadBanner();
    this.loadNews();
    this.loadAgenda()
    this.getEstados();
  }

  toggleClass(element, type: any) {
    let floorElements = document.getElementsByClassName("filtroeventos") as HTMLCollectionOf<HTMLElement>;
    for (var i = 0; i < floorElements.length; i++) {
      if (floorElements[i].innerText != element.currentTarget.innerText) {
        floorElements[i].classList.remove('active');
      }
    }
    for (var j = 0; j < floorElements.length; j++) {
      if (floorElements[j].innerText == element.currentTarget.innerText) {
        floorElements[j].classList.add('active');
      }
    }

    this.filterByData(type);
  }

  filterByData(type: any) {
    return new Promise(resolve => {

      this.agenda.filterData(this.auth.getToken(), type)
        .then(data => {
          this.agendaList = data;
          resolve(true);
        });
    });
  }

  showShare(conteudo) {
    if(this.IsAnonimo=='Anônimo'){
      this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
    }else{

      console.log(conteudo);
      let modal = this.modalCtrl.create(SocialSharePage, conteudo);
      modal.present();
    }
  }

  loadDataUser() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();

    this.getPersonDetail().then((result) => {
      this.getNotificationsCount();
      this.events.publish('profile', this.profile);
      loading.dismiss();
    });
  }

  getPersonDetail() {
    return new Promise(resolve => {
      this.person.get()
        .then((response: any) => {

          this.profile = response;

          console.log(response);

          resolve(true);
        }, (error) => {
          console.log(error);
        });
    });
  }

  getNotificationsCount() {
    this.notification.Count().then((response: any) => {
      this.count = response;
      this.events.publish('count', this.count);
      console.log("sync notification: " + response)
      this.notificationLoop();
    });
  }

  notificationLoop() {
    setTimeout(() => {
      this.notification.Count().then((response: any) => {
        this.count = response;
        this.events.publish('count', this.count);
        console.log("sync notification: " + response)
        this.getNotificationsCount();
      })
    }, 30000);
  }

  openExternal(url, opcao){
    if (url.indexOf("http://") == 0 || url.indexOf("https://") == 0) {
      window.open(url, '_system', 'location=no');
      console.log('Não tem link')
    }else if (url=='DetailTransmissaoPage'){
      this.navCtrl.push(url, {
        interna: opcao
      })
    }else{
      this.navCtrl.push(url)
    }
  }

}
