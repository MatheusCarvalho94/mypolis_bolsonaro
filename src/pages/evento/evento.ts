import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { EventosAmigosPage } from './modal-amigos';
import { SearchbarComponent } from '../../components/searchbar/searchbar';
import { AuthProvider } from '../../providers/auth/auth';
import { AgendaProvider } from '../../providers/agenda/agenda';
import { PersonProvider } from '../../providers/person/person';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { SocialSharePage } from '../social-share/social-share';

@IonicPage({
  name: 'evento',
  segment: 'evento/:id'
})
@Component({
  selector: 'page-evento',
  templateUrl: 'evento.html',
})
export class EventoPage {
  evento: any;
  outros: any= [];
  private start: number = 3;
  private skip: number = 0;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public agenda: AgendaProvider,
    public auth: AuthProvider,
    public utils: UtilsProvider,
    public person: PersonProvider,
    private _conteudo: ConteudoProvider) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventoPage');
  }

  openDetail(page, id) {
    this.navCtrl.push(page, { id: id })
  }

  showAllFriends() {
    let modal = this.modalCtrl.create(EventosAmigosPage);
    modal.present();
  }

  searchToggle() {

    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();

  }

  confirmEvent() {
    let confirm = this.alertCtrl.create({
      title: 'Confirmar presença no evento?',
      buttons: [
        {
          text: 'Sim eu irei!',
          handler: () => {

          }
        },
        {
          text: 'Cancelar',
          handler: () => {

          }
        }
      ]
    });
    confirm.present();
  }

  getData() {
    return new Promise(resolve => {
      this.agenda.getDetalhe(this.auth.getToken(), this.navParams.data.id)
        .then((data: any) => {
          this.evento = data;
          console.log(data);
          this.getOutros(data.Id);
          resolve(true);
        });
    });
  }

  private pagina2: number = 1;
  getOutros(id: any) {

      return new Promise(resolve => {
  
        this.agenda.getLista(this.auth.getToken(), this.pagina2,3)
          .then(data => {
  
            this.outros = this.outros.concat(data);
  
            resolve(true);
  
          });
  
      });
  }
  filterByData(type: any) {
    return new Promise(resolve => {

      this.agenda.filterData(this.auth.getToken(), type)
        .then(data => {
          this.outros = data;
          resolve(true);
        });
    });
  }

  postLike(conteudo) {

    let idPost = conteudo.Id;

    this.person.get()
      .then((response: any) => {

        let person = (response as any);

        this._conteudo.postLike(idPost, person.Id)
          .then((response: any) => {

            if (response.Curtiu && !conteudo.JaCurtiu) {
              conteudo.Curtidas += 1;
            } else {
              conteudo.Curtidas -= 1;
            }

            conteudo.JaCurtiu = response.Curtiu;

            if (response.GanhouPonto) {
              this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
            }
          });
      });
  }

  postShare(conteudo) {

    let modal = this.modalCtrl.create(SocialSharePage, conteudo);
    modal.present();
  }
}