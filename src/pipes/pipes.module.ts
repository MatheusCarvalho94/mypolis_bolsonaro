import { NgModule } from '@angular/core';
import { ExcerptPipe } from './excerpt/excerpt';
import { NotificationsPipe } from './notifications/notifications';
@NgModule({
	declarations: [ExcerptPipe,
    NotificationsPipe],
	imports: [],
	exports: [ExcerptPipe,
    NotificationsPipe]
})
export class PipesModule {}
