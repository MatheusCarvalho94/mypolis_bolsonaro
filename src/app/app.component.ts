import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, AlertController, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { ComitesPage } from '../pages/comites/comites';
import { VoluntarioPage } from '../pages/voluntario/voluntario';
import { NotificacoesPage } from '../pages/notificacoes/notificacoes';
import { ParticipacaoPage } from '../pages/participacao/participacao';
import { PesquisaPage } from '../pages/pesquisa/pesquisa';
import { ProgramaGovernoPage } from '../pages/programa-governo/programa-governo';
import { HomePage } from '../pages/home/home';
import { MaterialInstitucionalPage } from '../pages/material-institucional/material-institucional';
import { NotificacoesNotificacoesDetalhePage } from '../pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe';
import { ProgramaPontosPage } from '../pages/programa-pontos/programa-pontos';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { CONFIG_PROJECT } from '../providers/app-config';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

import { LoginPage } from '../pages/login/login';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { AuthProvider } from '../providers/auth/auth';
import { PersonProvider } from '../providers/person/person';
import { PushProvider } from '../providers/push/push';
import { NotificationProvider } from '../providers/notificacao/notificacao';
import { QueroContribuirPage } from "../pages/quero-contribuir/quero-contribuir";
import { Events } from 'ionic-angular';
import { UtilsProvider } from '../providers/utils/utils';
import { AppAvailability } from '@ionic-native/app-availability';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Storage } from '@ionic/storage';

declare var cordova: any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public disconnect: boolean = false;
  NotificacoesNotificacoesDetalhePage=NotificacoesNotificacoesDetalhePage
  rootPage: any;
  profile: any;
  count: 0;
  public IsAnonimo:any = this.storage.get('profileName')

  cordova: any;
  pages = [
    { title: 'Início', component: HomePage, icon: 'home', description: 'Notícias, Eventos, Agenda...',  cssClass: 'iniciocss', parameter : 0 },
    { title: 'Notificações', component: NotificacoesPage, icon: 'notifications', description: null, notificationsNumber: true, parameter : 0 },
    { title: 'Programa de Governo', component: ProgramaGovernoPage, icon: 'list', description: null , parameter : 0},
    { title: 'Seja Voluntário', component: VoluntarioPage, icon: 'hand', description: null , parameter : 0},
    { title: 'Minha Participação', component: ParticipacaoPage, icon: 'person', description: null, parameter : 0 },
    { title: 'Pesquisa de Opinião', component: PesquisaPage, icon: 'text', description: null, parameter : 0 },
    { title: 'Programa de Pontos', component: ProgramaPontosPage, icon: 'star', description: 'Como funciona, histórico de pontos...', cssClass: 'programacss', parameter : 0 },
    { title: 'Material de Campanha', component: MaterialInstitucionalPage, icon: 'images', description: 'Posts, santinhos, banners…', cssClass: 'materialcss', parameter : 0},
    { title: 'Comitês', component: ComitesPage, icon: 'people', description: null, parameter : 0 }
  ];
  Logado: any

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public auth: AuthProvider,
    public person: PersonProvider,
    private localNotifications: LocalNotifications,
    public pushProvider: PushProvider,
    public utils: UtilsProvider,
    public push: Push,
    public notification: NotificationProvider,
    public loadingCtrl: LoadingController,
    private appAvailability: AppAvailability,
    private iab: InAppBrowser,
    public alertCtrl: AlertController,
    public events: Events,
    public http: Http,
    public storage: Storage,
    private network: Network,
) {
    console.log(this.IsAnonimo, 'nome do individuo')
    this.storage.get('profileName').then((val) => {
      console.log('Your age is', val);
      this.IsAnonimo=val
    });
    events.subscribe('profile:count', (profile, count) => {
      this.storage.get('profileName').then((val) => {
        console.log('Your age is', val);
        this.IsAnonimo=val
        if(this.IsAnonimo=='Anônimo'){
          this.NoLogin=true;
        }else{
          this.NoLogin=false
          this.profile = profile;
        }
      });
     
    });

    events.subscribe('profile', (profile) => {
      this.storage.get('profileName').then((val) => {
        console.log('Your age is', val);
        this.IsAnonimo=val
        if(this.IsAnonimo=='Anônimo'){
          this.NoLogin=true;
        }else{
          this.NoLogin=false
          this.profile = profile;
        }
      });
    });

    events.subscribe('count', (count) => {
      this.storage.get('profileName').then((val) => {
        console.log('Your age is', val);
        this.IsAnonimo=val
        if(this.IsAnonimo=='Anônimo'){
          this.NoLogin=true;
        }else{
          this.NoLogin=false
          this.count = count;
        }
      });
    });

  
    events.subscribe('menu:closed', () => {
      // your action here
  });

    events.subscribe('clickLogin', () => {
      this.nav.setRoot(LoginPage);
    })

    events.subscribe('NotificacoesLocal', (notificacaoId) => {
      this.nav.setRoot(NotificacoesNotificacoesDetalhePage, notificacaoId);
    })
    this.initializeApp();

  }

  onDisconnect() {
    console.log('Sem conexão com internet. O conteúdo mais recente não poderá ser exibido. :-(');
    this.alertCtrl.create({
    
      title: '',
      subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
      buttons: ['OK'],
      cssClass: 'alertcss'
    }).present();
  }

  NoLogin
  initializeApp() {
    this.network.onDisconnect().subscribe(() => {
      this.disconnect = true;
      console.log(this.disconnect + 'caralho não vai passar aqui não') ;
      this.onDisconnect                                                                                                                                                 ();
    });
    console.log('iniciando app');
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (this.auth.isLogged()) {
        console.log('iniciando app logado');
        if(this.IsAnonimo=='Anônimo'){
          this.NoLogin=true;
        }else{
          this.NoLogin=false
          this.rootPage = HomePage;
          this.pushProvider.initPush()
          const options: PushOptions = {
            android: {
              senderID: '318490138469'
            },
            ios: {
              alert: 'true',
              badge: false,
              sound: 'true'
            },
            windows: {}
          };
      
          const pushObject: PushObject = this.push.init(options);
          pushObject.on('notification').subscribe((notificacao: any) => {
            console.log('message -> ' + notificacao);  [];
  
          if (notificacao.additionalData.foreground==true) {
            this.localNotifications.schedule({
              title: 'Voluntários da Pátria',
              text: notificacao.message,
              icon: 'ic_notifications',
              smallIcon: 'ic_notification_small',
              id: notificacao.additionalData.notificacaoId, 
            });
            // this.localNotifications.on("click", (notification, state) => {
            //   this.nav.setRoot(NotificacoesNotificacoesDetalhePage, notification.id);
            // })

          }else if(notificacao.additionalData.foreground==false){
            console.log(notificacao, 'LoginNotificacaoCOMPONENTS')
            this.nav.setRoot(NotificacoesNotificacoesDetalhePage, notificacao.additionalData.notificacaoId);
          }
        });
        pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
        }
        
      }else {
        this.rootPage = TutorialPage;
      }
    });
  }


  openPage(page) {
    switch (page.component.name) {
      case "NotificacoesPage":
        if(this.IsAnonimo=='Anônimo'){
          this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
        }else{
          this.nav.setRoot(page.component,page.parameter);
        }
        break;
      case "VoluntarioPage":
        if(this.IsAnonimo=='Anônimo'){
          this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
        }else{
          this.nav.setRoot(page.component,page.parameter);
        }
        break;
      case "ParticipacaoPage":
        if(this.IsAnonimo=='Anônimo'){
          this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
        }else{
          this.nav.setRoot(page.component,page.parameter);
        }
        break;
      case "PesquisaPage":
        if(this.IsAnonimo=='Anônimo'){
          this.utils.showModalLogin("Você não está logado! :(","Você precisa estar logado para continuar.", 'Faça seu login');
        }else{
          this.nav.setRoot(page.component,page.parameter);
        }
        break;
        
      default:
        this.nav.setRoot(page.component,page.parameter);
      break;
    }
  }

  
  openPageHistorico(){
    this.nav.setRoot(ProgramaPontosPage,'historico');
  }

  
  openinnap(){
    window.open('https://maisquevoto.com.br/psl', '_blank', 'location=no');
  }


  openBrowser(){
    window.open('https://maisquevoto.com.br/psl', '_blank', 'location=no');
  }

  
  openPagePush(page, params) {
    this.nav.setRoot(page,params);
  }


  logoutApp() {
    this.auth.logoutUser();
    this.nav.setRoot(LoginPage);
  }

  
  getNotificationsCount(){
    setTimeout(() => { this.notification.Count().then((response: any) => { 
      this.count = response; 
      this.events.publish('count', this.count);
      console.log("sync notification: " + response)
      this.getNotificationsCount();
    })}, 30000);
  }


  ///CLICK LOGIN

  appExistStatus:any
  app:any
  CheckForApp(app){
    // let app;
    console.log(app)
      if (this.platform.is('ios')) {
        if(app=='facebook'){
          this.fbCheck('fb://', 'fb://profile/280501375845086');
        }else if(app=='instagram'){
          this.instaCheck('instagram://');
        }else if(app=='twitter'){
          this.twitterCheck('twitter://');
        }else if(app=='youtube'){
          this.youtubeCheck('youtube://');
        }
      } else if(this.platform.is('android')){
        if(app=='facebook'){
          this.fbCheck('com.facebook.katana', 'fb://page/280501375845086');
        }else if(app=='instagram'){
          this.instaCheck('com.instagram.android');
        }else if(app=='twitter'){
          this.twitterCheck('com.twitter.android');
        }else if(app=='youtube'){
          this.youtubeCheck('com.youtube');
        }
      }
    }

  fbCheck(facebook, urlscheme){
    this.appAvailability.check(facebook)
    .then(
      (yes) => {
        console.log(facebook + ' is available')
        window.open(urlscheme, '_system', 'location=no');
      },
      (no) => {
        console.log(facebook + ' is NOT available')
        window.open('https://www.facebook.com/PartidoSocialLiberalBR', '_system', 'location=no');
        console.log("náo tem instalado");
      }
    );
   }
   
  instaCheck(instagram){
    this.appAvailability.check(instagram)
    .then(
      (yes) => {
        console.log(instagram + ' is available')
        this.appExistStatus = instagram + 'is available'
        window.open('instagram://user?username=PSL_Nacional', '_system', 'location=no');
      },
      (no) => {
        console.log(instagram + ' is NOT available')
        window.open('https://www.instagram.com/PSL_Nacional', '_system', 'location=no');
      }
    );
  }
  
  youtubeCheck(youtube){
    this.appAvailability.check(youtube)
    .then(
      (yes) => {
        console.log(youtube + ' is available')
        this.appExistStatus = youtube + 'is available'
        window.open('youtube://UCj6-AU8CsA4VBR4_sQn3Hug', '_system', 'location=no');
      },
      (no) => {
        console.log(youtube + ' is NOT available')
        window.open('https://www.youtube.com/channel/UCj6-AU8CsA4VBR4_sQn3Hug', '_system', 'location=no');
      }
    );
  }

  twitterCheck(twitter){
    this.appAvailability.check(this.app)
    .then(
      (yes) => {
        console.log(twitter + ' is available')
        this.appExistStatus = this.app + 'is available'
        {window.open('twitter://user?screen_name=PSL_Nacional', '_system', 'location=no');}

      },
      (no) => {
        console.log("veio no else do availabity");
        window.open('https://twitter.com/PSL_Nacional', '_system', 'location=no');
        console.log("náo tem instalado");
      }
    );
  }

}

