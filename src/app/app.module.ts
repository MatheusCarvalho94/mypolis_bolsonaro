import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { TextMaskModule } from 'angular2-text-mask';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { Facebook } from '@ionic-native/facebook';
import { LoginPageModule } from '../pages/login/login.module';
import { VoluntarioPageModule } from '../pages/voluntario/voluntario.module';
import { NotificacoesPageModule } from '../pages/notificacoes/notificacoes.module';
import { ComitesPageModule } from '../pages/comites/comites.module';
import { ParticipacaoPageModule } from '../pages/participacao/participacao.module';
import { TutorialPageModule } from '../pages/tutorial/tutorial.module';
import { PesquisaPageModule } from '../pages/pesquisa/pesquisa.module';
import { ProgramaGovernoPageModule } from '../pages/programa-governo/programa-governo.module';
import { ConvitePageModule } from '../pages/convite/convite.module';
import { NoticiasNoticiaDetalhePageModule } from '../pages/noticias-noticia-detalhe/noticias-noticia-detalhe.module';
import { PerfilPageModule } from '../pages/perfil/perfil.module';
import { MaterialInstitucionalPageModule } from '../pages/material-institucional/material-institucional.module';
import { ProgramaPontosPageModule } from '../pages/programa-pontos/programa-pontos.module';
import { DetailProgramaPageModule } from '../pages/detail-programa/detail-programa.module';
import { DetailTransmissaoPageModule } from '../pages/detail-transmissao/detail-transmissao.module';
import { SearchbarComponent } from '../components/searchbar/searchbar';
import { AuthProvider } from '../providers/auth/auth';
import { PersonProvider } from '../providers/person/person';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Push} from '@ionic-native/push';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import {QueroContribuirPageModule} from "../pages/quero-contribuir/quero-contribuir.module";

//Modulos
import { HttpModule } from '@angular/http';

import { UtilsProvider } from '../providers/utils/utils';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { AgendaProvider } from '../providers/agenda/agenda';

import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { ConteudoProvider } from '../providers/conteudo/conteudo';
import { PipesModule } from '../pipes/pipes.module';
import { ComiteProvider } from '../providers/comite/comite';
import { NotificationProvider } from '../providers/notificacao/notificacao';
import { GeralProvider } from '../providers/geral/geral';
import { VoluntarioProvider } from '../providers/voluntario/voluntario';
import { InstitucionalProvider } from '../providers/institucional/institucional';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { PesquisaProvider } from '../providers/pesquisa/pesquisa';
import { PontoProvider } from '../providers/ponto/ponto';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Camera } from '@ionic-native/camera';
import { Network } from '@ionic-native/network';

import { SocialSharing } from '@ionic-native/social-sharing';
import { SocialSharePage } from '../pages/social-share/social-share';
import { SocialShareMaterialPage } from '../pages/social-share-material/social-share-material';
// import { ModalSucessoPage1 } from '../pages/cadastro/modal-sucesso/modal-sucesso';
import { AppAvailability } from '@ionic-native/app-availability';

import { ModalSucessoPage } from '../pages/modal-sucesso/modal-sucesso';

import { Geolocation } from '@ionic-native/geolocation';


import { DoacaoProvider } from '../providers/doacao/doacao';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { ParticipacaoProvider } from '../providers/participacao/participacao';
import { PushProvider } from '../providers/push/push';
import { NotificacoesNotificacoesDetalhePageModule } from '../pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe.module';

registerLocaleData(localePt, 'pt');


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SearchbarComponent,
    SocialSharePage,
    ModalSucessoPage,
    SocialShareMaterialPage,

  ],
  imports: [
    BrowserModule,
    TextMaskModule,
    HttpClientModule,
    LoginPageModule,
    VoluntarioPageModule,
    ComitesPageModule,
    NotificacoesPageModule,
    NotificacoesNotificacoesDetalhePageModule,
    ParticipacaoPageModule,
    TutorialPageModule,
    PesquisaPageModule,
    ConvitePageModule,
    ProgramaGovernoPageModule,
    PerfilPageModule,
    NoticiasNoticiaDetalhePageModule,
    MaterialInstitucionalPageModule,
    ProgramaPontosPageModule,
    DetailProgramaPageModule,
    DetailTransmissaoPageModule,
    HttpModule,
    PipesModule,
    BrMaskerModule,
    FormsModule,
    BrMaskerModule,
    IonicModule.forRoot(MyApp, {
      menuType: 'overlay',
      backButtonText: ' '
    }),
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    TextMaskModule,
    QueroContribuirPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SearchbarComponent,
    SocialSharePage,
    ModalSucessoPage,
    SocialShareMaterialPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    InAppBrowser,
    PersonProvider,
    AgendaProvider,
    UtilsProvider,
    ConteudoProvider,
    ComiteProvider,
    NotificationProvider,
    GeralProvider,
    VoluntarioProvider,
    InstitucionalProvider,
    FileTransfer,
    Network,
    Push,
    File,
    PesquisaProvider,
    PontoProvider,
    AndroidPermissions,
    AppAvailability,
    Camera ,
    SocialSharing,
    DoacaoProvider,
    LocalNotifications,
    ParticipacaoProvider,
    
    Geolocation,
    [{ provide: LOCALE_ID, useValue: 'pt' }],
    PushProvider,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
