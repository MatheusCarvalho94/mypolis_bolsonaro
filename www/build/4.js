webpackJsonp([4],{

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginLoginEsqueciPageModule", function() { return LoginLoginEsqueciPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_esqueci__ = __webpack_require__(386);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginLoginEsqueciPageModule = /** @class */ (function () {
    function LoginLoginEsqueciPageModule() {
    }
    LoginLoginEsqueciPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login_login_esqueci__["a" /* LoginLoginEsqueciPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login_login_esqueci__["a" /* LoginLoginEsqueciPage */]),
            ],
        })
    ], LoginLoginEsqueciPageModule);
    return LoginLoginEsqueciPageModule;
}());

//# sourceMappingURL=login-login-esqueci.module.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginLoginEsqueciPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(143);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginLoginEsqueciPage = /** @class */ (function () {
    function LoginLoginEsqueciPage(navCtrl, navParams, alertCtrl, auth, utils, loadingCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.utils = utils;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.form = {
            Email: ''
        };
    }
    LoginLoginEsqueciPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginLoginEsqueciPage');
    };
    LoginLoginEsqueciPage.prototype.EsqueciSubmit = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.auth.postResetPassWord(this.form.Email)
            .then(function (response) {
            setTimeout(function () {
                loading.dismiss();
                _this.utils.showModalSucesso(response.toString(), null, "Ok", null);
                // alert.setTitle(response.toString());
                // alert.present();
            }, 1000);
            setTimeout(function () {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
            }, 2000);
        }, function (error) {
            console.log(error);
            setTimeout(function () {
                if (error.status == 400) {
                    console.log('aaaaa');
                    _this.alertCtrl.create({
                        title: '<img src="assets/icon/error.png"/> Erro',
                        subTitle: error.error.Message,
                        buttons: [{ text: 'Ok' }],
                        cssClass: 'alertcss'
                    }).present();
                }
            }, 1000);
            loading.dismiss();
        });
        ;
    };
    LoginLoginEsqueciPage.prototype.goLogin = function () {
        this.navCtrl.setRoot('login');
    };
    LoginLoginEsqueciPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login-login-esqueci',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/login/login-login-esqueci/login-login-esqueci.html"*/'<ion-content padding class="padding-fixed" color="light">\n  <header class="login-header" text-center style="    margin-top: 15%;">\n      <img src="assets/imgs/logo.png" class="brand">\n      <h3 class="title-blue">Esqueci minha senha</h3>\n      <div class="block-text">\n        <p style="    color: #333333;">Digite abaixo seu e-mail cadastrado, para que você possa alterar sua senha. Iremos te enviar um e-mail para redefinição de senha.</p>\n      </div>\n  </header>\n  <ion-list style="\n  margin-bottom: -7%;">\n\n    <ion-item>\n\n\n      <ion-input type="text" color="light" placeholder="E-mail" [(ngModel)]="form.Email"></ion-input>\n    </ion-item>\n\n  </ion-list>\n  <footer margin-top padding-top>\n    <button ion-button full text-uppercase margin-top (click)="EsqueciSubmit(form)" >Enviar E-mail</button>\n    <button ion-button color="white" full outline text-uppercase margin-top (click)="goLogin()" style="    border: 2px solid #0276a9;\n    color: #0276a9;">Cancelar</button>\n  </footer>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/login/login-login-esqueci/login-login-esqueci.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], LoginLoginEsqueciPage);
    return LoginLoginEsqueciPage;
}());

//# sourceMappingURL=login-login-esqueci.js.map

/***/ })

});
//# sourceMappingURL=4.js.map