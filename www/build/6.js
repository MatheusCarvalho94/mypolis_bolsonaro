webpackJsonp([6],{

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComitesComitesDetalhePageModule", function() { return ComitesComitesDetalhePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__comites_comites_detalhe__ = __webpack_require__(383);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ComitesComitesDetalhePageModule = /** @class */ (function () {
    function ComitesComitesDetalhePageModule() {
    }
    ComitesComitesDetalhePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__comites_comites_detalhe__["a" /* ComitesComitesDetalhePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__comites_comites_detalhe__["a" /* ComitesComitesDetalhePage */]),
            ],
        })
    ], ComitesComitesDetalhePageModule);
    return ComitesComitesDetalhePageModule;
}());

//# sourceMappingURL=comites-comites-detalhe.module.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComitesComitesDetalhePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_comite_comite__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_searchbar_searchbar__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ComitesComitesDetalhePage = /** @class */ (function () {
    function ComitesComitesDetalhePage(navParams, modalCtrl, comiteProvider, auth, sanitizer) {
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.comiteProvider = comiteProvider;
        this.auth = auth;
        this.sanitizer = sanitizer;
        this.result = null;
        this.hasLatLng = false;
        this.googleMapsSrc = null;
    }
    ComitesComitesDetalhePage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    ComitesComitesDetalhePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var comiteId = parseInt(this.navParams.get('id'));
        this.comiteProvider.findById(this.auth.getToken(), comiteId).then(function (result) {
            _this.result = result;
            if (result.Longitude && result.Latitude) {
                _this.hasLatLng = true;
                _this.googleMapsSrc = _this.sanitizer.bypassSecurityTrustResourceUrl('http://maps.google.com/maps?q=' + result.Latitude + ',' + result.Longitude + '&z=15&output=embed');
            }
        }, function (err) {
            console.log('error: ', err);
        });
    };
    ComitesComitesDetalhePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-comites-comites-detalhe',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/comites/comites-comites-detalhe/comites-comites-detalhe.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Comites</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n        <ion-icon name="search"></ion-icon>\n      </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content *ngIf="result">\n  <iframe *ngIf="hasLatLng" frameborder="0" style="border:0;margin-top: 11%;" allowfullscreen class="map"\n          [src]="googleMapsSrc"></iframe>\n  <div padding>\n    <ion-list class="list">\n      <div ion-item *ngIf="result?.Endereco">\n        <ion-icon name="pin" item-start></ion-icon>\n        {{ result?.Endereco }}\n      </div>\n      <div ion-item *ngIf="result?.HorarioFuncionamento">\n        <ion-icon name="time" item-start></ion-icon>\n        {{ result?.HorarioFuncionamento }}\n      </div>\n      <div ion-item *ngIf="result?.Telefone ">\n        <ion-icon name="phone-portrait" item-start></ion-icon>\n        {{ result?.Telefone }}\n      </div>\n      <div ion-item *ngIf="result?.Email ">\n        <ion-icon name="mail" item-start></ion-icon>\n        {{ result?.Email }}\n      </div>\n    </ion-list>\n  </div>\n</ion-content>\n<ion-content *ngIf="!result">\n  <p text-center>Carregando...</p>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/comites/comites-comites-detalhe/comites-comites-detalhe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_comite_comite__["a" /* ComiteProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ComitesComitesDetalhePage);
    return ComitesComitesDetalhePage;
}());

//# sourceMappingURL=comites-comites-detalhe.js.map

/***/ })

});
//# sourceMappingURL=6.js.map