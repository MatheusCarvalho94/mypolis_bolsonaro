webpackJsonp([8],{

/***/ 371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroTipoPerfilPageModule", function() { return CadastroTipoPerfilPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cadastro_tipoperfil__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CadastroTipoPerfilPageModule = /** @class */ (function () {
    function CadastroTipoPerfilPageModule() {
    }
    CadastroTipoPerfilPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cadastro_tipoperfil__["a" /* CadastroTipoPerfilPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cadastro_tipoperfil__["a" /* CadastroTipoPerfilPage */]),
                __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ],
        })
    ], CadastroTipoPerfilPageModule);
    return CadastroTipoPerfilPageModule;
}());

//# sourceMappingURL=cadastro-tipoperfil.module.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastroTipoPerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CadastroTipoPerfilPage = /** @class */ (function () {
    function CadastroTipoPerfilPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CadastroTipoPerfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CadastroPage');
    };
    CadastroTipoPerfilPage.prototype.goPage = function (page) {
        this.navCtrl.push(page);
    };
    CadastroTipoPerfilPage.prototype.goEtapa = function (page, numero) {
        this.navCtrl.push(page, numero);
        localStorage.setItem('numero-cadastro', numero);
    };
    CadastroTipoPerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cadastrotipoperfil',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/cadastro/cadastro-tipoperfil.html"*/'<ion-content>\n  <style>\n    .card-options li ion-icon{\n      color: #cdcdcd;\n    }\n    .vp{\n      margin-top: 6%!important;\n    width: 100%!important;\n    }\n    strong{\n      color:#333333;\n    }\n    .card-options{\n      margin-top: 20px!important;\n\n    }\n    .card-options li.select{\n      background: #fff;\n    }\n    .card-options li{\n      border:none;\n      background: #efefef;\n      padding-top: 5px!important;\n      padding-bottom: 5px!important;                            \n      margin-bottom: 7px!important;\n      font-size: 1.5rem!important\n    }\n  .scroll-content{\n    /* background: url(../assets/imgs/bg-login.png)!important; */\n  background-size: cover!important;\n  background-repeat: no-repeat;\n  }\n  .imgvolu{\n    width: 60%;\n    margin-top: 4%;\n  }\n  .arrowteste{\n    margin-right: 3%;\n  }\n  .container-stage .number{\n    background:#333333;\n}\n  \n  </style>\n  <div padding style=" background: url(./assets/imgs/bg2.png) left top !important;\n  background-size: cover!important;\n  background-repeat: no-repeat;">\n    <header class="login-header" text-center>\n        <img src="assets/imgs/logo-black.png" class="brand imgvolu">\n        <div class="vp">\n          <img src="assets/imgs/voluntario_patria.png" class="brand imgvp" style="    width: 65%!important;">\n\n        </div>\n\n        <h3 class="title-blue" style="color:#0276a9;">Cadastre-se!</h3>\n    </header>\n    <div class="container-stage">\n        <!-- <div class="number">1</div> -->\n        <div class="text">\n          <strong>Esta é sua primeira tarefa, recruta! Selecione seu tipo de perfil:\n          </strong>\n          <p>De acordo com sua escolha, saberemos qual será seu posto em nossa batalha.\n          </p>\n        </div>\n      </div>\n\n\n    <ul class="card-options">\n        <li (click)="goEtapa(\'cadastro\', 2)">\n        <img src="assets/imgs/arrowteste.png" class="arrowteste">\n          <strong>Quero apenas receber informações sobre a campanha de Jair Bolsonaro.\n          </strong>\n        </li>\n        <li (click)="goEtapa(\'cadastro\', 2)">\n        <img src="assets/imgs/arrowteste.png" class="arrowteste">\n          <strong>Quero participar. Conte comigo para compartilhar informações!</strong>\n        </li>\n        <li (click)="goEtapa(\'cadastro\', 3)">\n        <img src="assets/imgs/arrowteste.png" class="arrowteste">\n          <strong>Quero contribuir como voluntário e financeiramente!\n          </strong>\n        </li>\n        <li (click)="goEtapa(\'cadastro\', 4)">\n        <img src="assets/imgs/arrowteste.png" class="arrowteste">\n          <strong>Quero me filiar ao partido de Jair Bolsonaro.\n          </strong>\n        </li>\n    </ul>\n    <!-- <button ion-button full text-uppercase (click)="goPage(\'cadastro\')">Etapa seguinte</button> -->\n  </div>\n  \n</ion-content>\n<ion-footer>\n  <ion-toolbar (click)="goPage(\'login\')" text-center style="color:#fff;background:#0276a9;">\n    Ou, volte para <strong style="color:#fff">o login</strong>\n  </ion-toolbar>\n</ion-footer>\n\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/cadastro/cadastro-tipoperfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], CadastroTipoPerfilPage);
    return CadastroTipoPerfilPage;
}());

//# sourceMappingURL=cadastro-tipoperfil.js.map

/***/ })

});
//# sourceMappingURL=8.js.map