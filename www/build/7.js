webpackJsonp([7],{

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroPageModule", function() { return CadastroPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cadastro__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { ModalSucessoPage } from '../modal-sucesso/modal-sucesso';

var CadastroPageModule = /** @class */ (function () {
    function CadastroPageModule() {
    }
    CadastroPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cadastro__["a" /* CadastroPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cadastro__["a" /* CadastroPage */]),
                // IonicPageModule.forChild(CadastroModalPage),
                __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ],
        })
    ], CadastroPageModule);
    return CadastroPageModule;
}());

//# sourceMappingURL=cadastro.module.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_facebook__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_brmasker_ionic_3__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var CadastroPage = /** @class */ (function () {
    function CadastroPage(navCtrl, navParams, loadingCtrl, alertCtrl, person, http, fb, utils, events, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.person = person;
        this.http = http;
        this.fb = fb;
        this.utils = utils;
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.Formulario = false;
        this.Sucesso = false;
        this.form = {
            RoleId: 2,
            Nome: '',
            Email: '',
            Numero: '',
            Senha: '',
            ConfirmarSenha: '',
            clienteId: 20471,
            Origem: 'APP Voluntários da Pátria'
        };
    }
    CadastroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CadastroPage');
    };
    CadastroPage.prototype.goPage = function (page) {
        this.navCtrl.push(page);
    };
    CadastroPage.prototype.onRegister = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        var numeroPerfil = parseInt(localStorage.getItem('numero-cadastro'));
        var headers = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Headers */]({});
        console.log(headers);
        var options = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["d" /* RequestOptions */]({ headers: headers });
        loading.present();
        var body = {
            Nome: this.form.Nome,
            Email: this.form.Email,
            Celular: this.form.Numero,
            Senha: this.form.Senha,
            ConfirmarSenha: this.form.ConfirmarSenha,
            RoleId: numeroPerfil,
            clienteId: 20471,
            Origem: 'APP Voluntários da Pátria',
        };
        return this.http.post("" + __WEBPACK_IMPORTED_MODULE_7__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/Pessoa/CadastroBasico', body, options).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.success == true) {
                loading.dismiss(),
                    _this.utils.showModalSucesso(data.Mensagem, null, "Iniciar", null);
                setTimeout(function () {
                    _this.VoltarHomeLogado();
                }, 3000);
                return false;
            }
            else {
                _this.alertCtrl.create({
                    title: '<img src="assets/icon/error.png"/> Erro',
                    subTitle: data.Descricao,
                    buttons: [{ text: 'Ok' }],
                    cssClass: 'alertcss'
                }).present();
                loading.dismiss();
            }
        }, function (err) {
            console.log("ERROR!: ", err);
            //  this.loading.dismiss();ion
            var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
            if (mensagem.indexOf('encontra cadastrado') != -1) {
                _this.alertCtrl.create({
                    title: '<img src="assets/icon/error.png"/> ERRO',
                    subTitle: "Esse e-mail já se encontra cadastrado. Informe outro e-mail ou <span (click)='EsqueciPage()'>clique aqui</span> caso tenha esquecido sua senha.",
                    buttons: [{ text: 'Ok' }],
                    cssClass: 'alertclasse'
                }).present();
                loading.dismiss();
            }
            else {
                _this.alertCtrl.create({
                    title: '<img src="assets/icon/error.png" /> Erro',
                    subTitle: mensagem,
                    buttons: [{ text: 'Ok' }],
                    cssClass: 'alertcss'
                }).present();
                loading.dismiss();
            }
        });
    };
    CadastroPage.prototype.VoltarHomeLogado = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_7__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + "/token";
        var headers = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var body = "grant_type=password&username=" + this.form.Email + "&password=" + this.form.Senha + "&appId=" + __WEBPACK_IMPORTED_MODULE_7__providers_app_config__["a" /* CONFIG_PROJECT */].appId;
        return this.http.post(url, body, options).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Logado = true;
            localStorage.setItem('access_token', data.access_token);
            localStorage.removeItem('profile');
            _this.events.publish('user:Login', data, _this.Logado);
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
            console.log('caiuno if');
        }, function (err) {
            console.log("ERROR!: ", err);
        });
    };
    CadastroPage.prototype.loginToFacebook = function () {
        var _this = this;
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                _this.getUserDetail(res.authResponse.userID, res.authResponse.accessToken);
            }
            else {
            }
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    CadastroPage.prototype.getUserDetail = function (userid, usertoken) {
        var _this = this;
        var numeroPerfil = parseInt(localStorage.getItem('numero-cadastro'));
        this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            _this.InfosFb = res;
            var headers = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            _this.http.get("" + __WEBPACK_IMPORTED_MODULE_7__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/Pessoa/VerificaFacebook?FacebookId=' + _this.InfosFb.id + '&ClienteCod=20471').map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                if (data.Existe == true) {
                    var headers_1 = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Headers */]({
                        'Content-Type': 'application/x-www-form-urlencoded'
                    });
                    var body = "grant_type=password&username=facebook:" + res.id + "&password=" + usertoken + "&appId=" + __WEBPACK_IMPORTED_MODULE_7__providers_app_config__["a" /* CONFIG_PROJECT */].appId;
                    return _this.http.post("" + __WEBPACK_IMPORTED_MODULE_7__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/token', body, { headers: headers_1 }).map(function (res) { return res.json(); })
                        .subscribe(function (data) {
                        localStorage.setItem('access_token', data.access_token);
                        localStorage.setItem("cacheUsuario", JSON.stringify(data));
                        localStorage.removeItem('profile');
                        _this.events.publish('user:Login', data);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {});
                    });
                }
                else {
                    var headers_2 = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Headers */]({
                        'Content-Type': 'application/json'
                    });
                    var body = {
                        Nome: _this.InfosFb.name,
                        Email: _this.InfosFb.email,
                        FacebookId: _this.InfosFb.id,
                        Origem: 'APP Voluntários da Pátria',
                        RoleId: numeroPerfil,
                        clienteId: 20471,
                    };
                    return _this.http.post("" + __WEBPACK_IMPORTED_MODULE_7__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/Pessoa/CadastroFacebook', body, { headers: headers_2 }).map(function (res) { return res.json(); })
                        .subscribe(function (data) {
                        console.log(data);
                        var headers = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Headers */]({
                            'Content-Type': 'application/x-www-form-urlencoded'
                        });
                        var options = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["d" /* RequestOptions */]({ headers: headers });
                        var body = "grant_type=password&username=facebook:" + res.id + "&password=" + usertoken + "&appId=" + __WEBPACK_IMPORTED_MODULE_7__providers_app_config__["a" /* CONFIG_PROJECT */].appId;
                        return _this.http.post("" + __WEBPACK_IMPORTED_MODULE_7__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/token', body, { headers: headers }).map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            localStorage.setItem('access_token', data.access_token);
                            localStorage.setItem("cacheUsuario", JSON.stringify(data));
                            localStorage.removeItem('profile');
                            _this.events.publish('user:Login', data);
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {});
                        });
                    }, function (err) {
                        console.log("ERROR!: ", err);
                    });
                }
            });
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    CadastroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cadastro',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/cadastro/cadastro.html"*/'<ion-content>\n    <style>\n      .alert-head h2 > img{\n      position: absolute;\n      width: 25%;\n      top: 13%;\n      width: 10%;\n      left: 78px;\n      right: 0;\n  }\n  .scroll-content{\n    padding-bottom: 0!important;\n    background: url(../assets/imgs/bg-login.png) no-repeat left top!important;\n  }\n  \n  page-cadastro .login-header.background{\n    background: none!important;\n  }\n    .container-stage .number{\n      background:#333333;\n  \n    }\n    .button-ios{\n      background-color:#0276a9!important;\n    }\n    .button-md{\n      background-color:#0276a9!important;\n    }\n   \n      </style>\n    <header class="login-header background" text-center>\n      <img src="assets/imgs/logo.png" class="brand" style="    width: 45%;">\n        <h3 class="title-blue" style="color:#0276a9!important;">Cadastro</h3>\n \n    <div padding>\n      <div class="block-text" text-center>\n        <p style="color: #333333">Preencha os dados abaixo. <br/> É bem simples e rápido!</p>\n      </div>\n      <!-- <div class="container-stage" style="    padding-bottom: 0;">\n          <div class="number" style="background:#cdcdcd">1</div>\n          <div class="text" style="color:#cdcdcd;">\n            <strong >Selecione seu tipo de perfil:</strong>\n            <p>De acordo com o perfil iremos selecionar a melhor maneira de passar informações.</p>\n          </div>\n        </div> -->\n      <!-- <div class="container-stage" style="    padding-bottom: 10%;">\n          <div class="number" > 2\n       \n          </div>\n   \n        <div class="text">\n          <strong>Preencha seus dados iniciais:</strong>\n          <p style="color:#333333">*Todos os dados devem ser preenchidos</p>\n        </div>\n      </div> -->\n      <form autocomplete="off" #loginform="ngForm">\n      <ion-list>\n        <ion-item class="input-block-color">\n          <ion-input type="text" placeholder="Nome" [(ngModel)]="form.Nome" name="Nome"></ion-input>\n        </ion-item>\n  \n        <ion-item class="input-block-color">\n          <ion-input type="email" placeholder="E-mail" [(ngModel)]="form.Email" name="Email"></ion-input>\n        </ion-item>\n  \n        <ion-item class="input-block-color">\n  \n          <!-- <ion-input placeholder="Telefone"  [textMask]="{mask: maskcel}" type="tel" [(ngModel)]="form.Numero" name="Numero"></ion-input>  -->\n          <ion-input placeholder="Telefone"  [brmasker]="{phone: true}" type="tel" [(ngModel)]="form.Numero" name="Numero"></ion-input> \n  \n        </ion-item>\n  \n        <ion-item class="input-block-color" style="   border-radius: 10px 10px 0px 0px;">\n          <ion-input type="password" placeholder="Senha" [(ngModel)]="form.Senha" name="Senha"></ion-input>\n        </ion-item>\n  \n        <ion-item class="input-block-color" style="    border-radius: 10px 10px 0px 0px;">\n          <ion-input type="password" placeholder="Confirmar senha" [(ngModel)]="form.ConfirmarSenha" name="ConfirmarSenha"> </ion-input>\n        </ion-item>\n  \n        <!-- <ion-item margin-top class="checkbox" style="background-color: transparent;border-bottom:none">\n          <ion-label><strong>Li</strong> e <strong>aceito</strong> os termos de uso</ion-label>\n          <ion-checkbox checked="true" [(ngModel)]="model.CheckTermo" name="CheckTermo"></ion-checkbox>\n        </ion-item> -->\n  \n      </ion-list>\n      <button ion-button full text-uppercase (click)="onRegister()" >Enviar</button>\n    </form>\n\n      <button ion-button outline full text-uppercase margin-top (click)="loginToFacebook()">\n        <img class="ico-general-button" src="assets/imgs/ico-facebook.png" width="11" height="19"> Cadastrar pelo Facebook\n      </button>\n    </div>\n  </header>\n    <div>\n        <ion-toolbar (click)="goPage(\'cadastro-tipoperfil\')" text-center style="color:#fff">\n            Voltar a tela <strong> anterior</strong>\n          </ion-toolbar>\n    </div>\n    \n\n  </ion-content>\n  \n  '/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/cadastro/cadastro.html"*/,
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_6_brmasker_ionic_3__["a" /* BrMaskerModule */],
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_8__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], CadastroPage);
    return CadastroPage;
}());

// @Component({
//   selector: 'modal-cadastro',
//template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/cadastro/cadastro-modal.html"*/'<div class="container-round">\n        <div class="box">\n            <button class="btnClose" (click)="close()">\n                <ion-icon name="close"></ion-icon>\n            </button>\n            <ion-icon name="checkmark"  class="check"></ion-icon>\n            <ion-icon name="close-circle" class="check"></ion-icon>\n            <div class="text">teste</div>\n            <div class="text">testte</div>\n            <button class="btnOk" (click)="close()">tete</button>\n           \n        </div>\n    </div>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/cadastro/cadastro-modal.html"*/
// })
// export class CadastroModalPage {
//   // pesquisaDetail: any;
//   constructor(params: NavParams, public navCtrl: NavController, public loadingCtrl: LoadingController, public utils: UtilsProvider) {
//     }
//   close() {
//     this.navCtrl.pop();
//   }
// }
//# sourceMappingURL=cadastro.js.map

/***/ })

});
//# sourceMappingURL=7.js.map