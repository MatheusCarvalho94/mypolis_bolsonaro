webpackJsonp([0],{

/***/ 375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventoPageModule", function() { return EventoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__evento__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_amigos__ = __webpack_require__(380);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var EventoPageModule = /** @class */ (function () {
    function EventoPageModule() {
    }
    EventoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__evento__["a" /* EventoPage */],
                __WEBPACK_IMPORTED_MODULE_3__modal_amigos__["a" /* EventosAmigosPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__evento__["a" /* EventoPage */])
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__evento__["a" /* EventoPage */],
                __WEBPACK_IMPORTED_MODULE_3__modal_amigos__["a" /* EventosAmigosPage */]
            ]
        })
    ], EventoPageModule);
    return EventoPageModule;
}());

//# sourceMappingURL=evento.module.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventosAmigosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EventosAmigosPage = /** @class */ (function () {
    function EventosAmigosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EventosAmigosPage.prototype.ionViewDidLoad = function () {
    };
    EventosAmigosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'amigos-evento',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/evento/modal-amigos.html"*/'<div class="container-round" text-center>\n  <header>\n    <h3 class="title-blue">Amigos que comparecerão</h3>\n    <p>Seus amigos do Facebook</p>\n  </header>\n  <ion-list>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n  </ion-list>\n</div>\n  \n  '/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/evento/modal-amigos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], EventosAmigosPage);
    return EventosAmigosPage;
}());

//# sourceMappingURL=modal-amigos.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_amigos__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_searchbar_searchbar__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_agenda_agenda__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__social_share_social_share__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var EventoPage = /** @class */ (function () {
    function EventoPage(navCtrl, navParams, modalCtrl, alertCtrl, agenda, auth, utils, person, _conteudo) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.agenda = agenda;
        this.auth = auth;
        this.utils = utils;
        this.person = person;
        this._conteudo = _conteudo;
        this.outros = [];
        this.start = 3;
        this.skip = 0;
        this.pagina2 = 1;
        this.getData();
    }
    EventoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventoPage');
    };
    EventoPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, { id: id });
    };
    EventoPage.prototype.showAllFriends = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__modal_amigos__["a" /* EventosAmigosPage */]);
        modal.present();
    };
    EventoPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    EventoPage.prototype.confirmEvent = function () {
        var confirm = this.alertCtrl.create({
            title: 'Confirmar presença no evento?',
            buttons: [
                {
                    text: 'Sim eu irei!',
                    handler: function () {
                    }
                },
                {
                    text: 'Cancelar',
                    handler: function () {
                    }
                }
            ]
        });
        confirm.present();
    };
    EventoPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.getDetalhe(_this.auth.getToken(), _this.navParams.data.id)
                .then(function (data) {
                _this.evento = data;
                console.log(data);
                _this.getOutros(data.Id);
                resolve(true);
            });
        });
    };
    EventoPage.prototype.getOutros = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.getLista(_this.auth.getToken(), _this.pagina2, 3)
                .then(function (data) {
                _this.outros = _this.outros.concat(data);
                resolve(true);
            });
        });
    };
    EventoPage.prototype.filterByData = function (type) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.filterData(_this.auth.getToken(), type)
                .then(function (data) {
                _this.outros = data;
                resolve(true);
            });
        });
    };
    EventoPage.prototype.postLike = function (conteudo) {
        var _this = this;
        var idPost = conteudo.Id;
        this.person.get()
            .then(function (response) {
            var person = response;
            _this._conteudo.postLike(idPost, person.Id)
                .then(function (response) {
                if (response.Curtiu && !conteudo.JaCurtiu) {
                    conteudo.Curtidas += 1;
                }
                else {
                    conteudo.Curtidas -= 1;
                }
                conteudo.JaCurtiu = response.Curtiu;
                if (response.GanhouPonto) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                }
            });
        });
    };
    EventoPage.prototype.postShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    EventoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-evento',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/evento/evento.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Evento</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content *ngIf="evento" style=" background: #fff!important;">\n  \n        <style>\n          .scroll-content{\n            background: #fafafa!important;\n          }\n         section, .scroll-content, .fixed-content, div {\n      background: #fff!important;\n      margin-top: -1%;\n    }\n    .vejaoutras{\n            background:#fafafa!important;\n        }\n    .button-clear-ios {\n        border-color: transparent;\n        color: #bfbfbf!important;\n        background-color: transparent;\n    }\n          \nsection{\n  background:#fff!important;\n}\n.content-md, .content-ios{\n  background: #fafafa!important;\n}\n\n    \n            </style>\n  <img [src]="evento.Imagem"  class="img-featured">\n  <div>\n    <header class="buttons-event" style="padding-left: 16px;\n    padding-right: 16px;    padding-bottom: 39px;\n    padding-top: 5px;">\n      <ion-row>\n        <ion-col text-left col-7 col-md-2>\n          <div style="margin-top: 8px;">\n     \n          </div>\n        </ion-col>\n        <ion-col text-right col-5 col-md-2>\n          <button ion-button icon-left clear small (click)="postLike(evento)" [ngClass]="{\'liked\': evento.JaCurtiu}" style="height: 15px;\n          border-right: 1px solid #bfbfbf;\n          border-radius: 0!important;">\n          <ion-icon name="heart" [ngClass]="{\'liked\': evento.JaCurtiu}"></ion-icon>\n          <!-- <ion-icon name="heart" *ngIf="evento.Curtidas > 0" style="color:#004f99"></ion-icon> -->\n          <div [ngClass]="{\'liked\': evento.JaCurtiu}">{{evento.Curtidas}}</div>\n          <!-- <div *ngIf="evento.Curtidas > 0" style="color:#004f99">{{evento.Curtidas}}</div> -->\n          </button>\n          <button class="share" ion-button icon-right text-left clear small (click)="postShare(evento)">\n              <img src="assets/imgs/sharenot.png" class="imgshare" style="    width: 80%;\n              margin-left: 16%;">\n\n          </button>\n        </ion-col>\n      </ion-row>\n    </header>\n    <section style="padding:16px;    padding-top: 0;">\n        <h4 class="bold">{{evento.Titulo}}</h4>\n\n      <p class="date-event"><ion-icon name="calendar"></ion-icon> {{evento.DataBR}}<span *ngIf="evento.Hora">, às {{evento.Hora}}</span></p>\n      <div class="block-text" margin-top >\n        <p [innerHtml]="evento.Texto"></p>\n        <p style="color: #797979"><ion-icon name="pin"></ion-icon> {{evento.Local}} </p>\n\n      </div>\n    </section>\n    <section class="padding-section vejaoutras"  style="padding:16px;">\n      <h5 class="bold">Veja outros eventos</h5>\n      <ion-slides pager spaceBetween="-40" class="list-cards">\n        <ion-slide *ngFor="let outro of outros">\n          <ion-card (click)="openDetail(\'evento\',outro.Id)">\n            <div class="row">\n              <div class="hour"> {{outro.Data | date: \'dd\'}} <strong>{{outro.Data | date: \'MMM\'}}</strong> <small *ngIf="outro.Hora">{{outro.Hora}}h</small> </div>\n              <div class="col" text-left>\n                <ion-card-content>\n                  <ion-card-title>{{outro.Titulo}}</ion-card-title>\n                  {{outro.Local}}\n\n                  \n                </ion-card-content>\n              </div>\n            </div>\n          </ion-card>\n        </ion-slide>\n      </ion-slides>\n\n    </section>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/evento/evento.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_agenda_agenda__["a" /* AgendaProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__["a" /* ConteudoProvider */]])
    ], EventoPage);
    return EventoPage;
}());

//# sourceMappingURL=evento.js.map

/***/ })

});
//# sourceMappingURL=0.js.map