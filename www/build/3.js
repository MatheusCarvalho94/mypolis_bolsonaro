webpackJsonp([3],{

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParticipacaoParticipacaoDetalhePageModule", function() { return ParticipacaoParticipacaoDetalhePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__participacao_participacao_detalhe__ = __webpack_require__(387);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ParticipacaoParticipacaoDetalhePageModule = /** @class */ (function () {
    function ParticipacaoParticipacaoDetalhePageModule() {
    }
    ParticipacaoParticipacaoDetalhePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__participacao_participacao_detalhe__["a" /* ParticipacaoParticipacaoDetalhePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__participacao_participacao_detalhe__["a" /* ParticipacaoParticipacaoDetalhePage */]),
            ],
        })
    ], ParticipacaoParticipacaoDetalhePageModule);
    return ParticipacaoParticipacaoDetalhePageModule;
}());

//# sourceMappingURL=participacao-participacao-detalhe.module.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParticipacaoParticipacaoDetalhePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_participacao_participacao__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ParticipacaoParticipacaoDetalhePage = /** @class */ (function () {
    function ParticipacaoParticipacaoDetalhePage(navCtrl, navParams, participacao, loadingCtrl, util) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.participacao = participacao;
        this.loadingCtrl = loadingCtrl;
        this.util = util;
        this.exibirInteracao = false;
        this.TextoBotao = "Interagir";
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.participacao.get(this.navParams.data).then(function (result) {
            loading.dismiss();
            _this.part = result;
            console.log(result);
        }, function (error) { loading.dismiss(); });
    }
    ParticipacaoParticipacaoDetalhePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ParticipacaoParticipacaoDetalhePage');
    };
    ParticipacaoParticipacaoDetalhePage.prototype.interagir = function () {
        var _this = this;
        if (this.TextoBotao == "Interagir") {
            this.exibirInteracao = true;
            this.TextoBotao = "Enviar";
        }
        else if (this.TextoBotao == "Enviar") {
            this.exibirInteracao = false;
            var loading_1 = this.loadingCtrl.create({
                content: 'Enviando...'
            });
            loading_1.present();
            this.participacao.put(this.Texto, this.part.Id).then(function (result) {
                loading_1.dismiss();
                _this.util.showModalSucesso(result.Titulo, result.Mensagem, "OK", null);
                _this.TextoBotao = "Interagir";
                _this.Texto = '';
                _this.participacao.get(result.SolicitacaoId).then(function (result2) {
                    _this.part = result2;
                });
            }, function (error) {
                _this.Texto = '';
                loading_1.dismiss();
                _this.TextoBotao = "Interagir";
            });
        }
    };
    ParticipacaoParticipacaoDetalhePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-participacao-participacao-detalhe',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/participacao/participacao-participacao-detalhe/participacao-participacao-detalhe.html"*/'<ion-header color="header">\n  <style>\n  .item-input ion-textarea{\n  border: 1px solid #dedede;\n  margin-bottom: 10%;\n  padding: 5%;\n}\n  </style>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Minha Participação</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-card *ngIf="part">\n    <ion-card-content>\n      <ion-note>\n        <ion-icon name="calendar"></ion-icon> {{part.DataCompleta}}</ion-note>\n      <div class="block-text" margin-top>\n        <div>\n          <strong>Categoria:</strong>\n          <p>{{part.CategoriaNome}}</p>\n        </div>\n        <div margin-top>\n          <strong>Tipo:</strong>\n          <p>{{part.SolicitacaoCategoria.Nome}}</p>\n        </div>\n        <div margin-top>\n          <strong>Local:</strong>\n          <p>{{part.Local}}</p>\n        </div>\n        <div margin-top>\n          <strong>Descrição:</strong>\n          <p>{{part.Texto}}</p>\n        </div>\n        <div margin-top *ngIf="part && part.Imagens && part.Imagens.length != 0">\n          <strong>Fotos:</strong>\n          <ul class="list-pictures">\n            <li *ngFor="let imagem of part.Imagens">\n              <img [src]="imagem.ImagemURL">\n            </li>\n          </ul>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card *ngIf="part && part.Mensagens && part.Mensagens.length != 0">\n    <ion-card-content>\n      <ion-note>Histórico</ion-note>\n      <ion-list>\n        <ion-item *ngFor="let m of part.Mensagens">\n          <ion-avatar item-start>\n            <img [src]="m.Pessoa.CaminhoFoto">\n          </ion-avatar>\n          <h2>{{m.Pessoa.Nome}}</h2>\n          <h3>{{m.TempoApartirData}}</h3>\n          <p [innerHtml]="m.Mensagem"></p>\n        </ion-item>\n      </ion-list>\n      <ion-item *ngIf="exibirInteracao" class="item-textarea">\n        <ion-label stacked text-uppercase>Descrição</ion-label>\n        <ion-textarea class="textarea-detalhe" [(ngModel)]="Texto"></ion-textarea>\n      </ion-item>\n      <ion-label class="error" *ngIf="ErroTexto">Campo Descrição obrigatório</ion-label>\n      <button ion-button round full (click)="interagir()" *ngIf="part.Status != 3 && part.Status != 4">{{TextoBotao}}</button>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/participacao/participacao-participacao-detalhe/participacao-participacao-detalhe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_participacao_participacao__["a" /* ParticipacaoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */]])
    ], ParticipacaoParticipacaoDetalhePage);
    return ParticipacaoParticipacaoDetalhePage;
}());

//# sourceMappingURL=participacao-participacao-detalhe.js.map

/***/ })

});
//# sourceMappingURL=3.js.map