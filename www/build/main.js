webpackJsonp([9],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_push__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_local_notifications__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_config__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PushProvider = /** @class */ (function () {
    function PushProvider(http, auth, app, platform, push, localNotifications) {
        this.http = http;
        this.auth = auth;
        this.app = app;
        this.platform = platform;
        this.push = push;
        this.localNotifications = localNotifications;
        console.log('Hello PushProvider Provider');
    }
    PushProvider.prototype.initPush = function () {
        var _this = this;
        if (!this.platform.is('cordova')) {
            console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
            return;
        }
        var options = {
            android: {
                senderID: '318490138469'
            },
            ios: {
                alert: 'true',
                badge: false,
                sound: 'true'
            },
            windows: {}
        };
        var pushObject = this.push.init(options);
        pushObject.on('registration').subscribe(function (data) {
            console.log('device token -> ' + data.registrationId);
            localStorage.setItem('TokenPush', JSON.stringify(data.registrationId));
            var plataforma = '';
            if (navigator.userAgent.match(/Android/i)) {
                plataforma = 'android';
            }
            else {
                plataforma = 'ios';
            }
            var body = {
                AppId: __WEBPACK_IMPORTED_MODULE_6__app_config__["a" /* CONFIG_PROJECT */].appId,
                Token: data.registrationId,
                Plataforma: plataforma
            };
            var url = __WEBPACK_IMPORTED_MODULE_6__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Push/Register/";
            var token = _this.auth.getToken();
            var header = { "headers": { "authorization": 'bearer ' + token } };
            return new Promise(function (resolve, reject) {
                _this.http.post(url, body, header).map(function (res) { return res; })
                    .subscribe(function (data) {
                    console.log(data, "retorno do token");
                }, function (err) {
                    console.log('deuruim', err);
                });
            });
        });
        // pushObject.on('notification').subscribe((notificacao: any) => {
        //     console.log('message -> ' + notificacao);  [];
        //   if (notificacao.additionalData.foreground==true) {
        //     this.localNotifications.schedule({
        //       title: 'Voluntários da Pátria',
        //       text: notificacao.message,
        //       icon: 'ic_notifications',
        //       smallIcon: 'ic_notification_small',
        //       id: notificacao.additionalData.MensagemCod
        //     });
        //   }else if(notificacao.additionalData.foreground==false){
        //     console.log(notificacao, 'LoginNotificacao')
        //   this.nav.push(NotificacoesNotificacoesDetalhePage, notificacao.additionalData.notificacaoId);
        //     // .push()
        //   }
        // });
        // pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* Nav */])
    ], PushProvider.prototype, "nav", void 0);
    PushProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_local_notifications__["a" /* LocalNotifications */]])
    ], PushProvider);
    return PushProvider;
}());

//# sourceMappingURL=push.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComitesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_comite_comite__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ComitesPage = /** @class */ (function () {
    function ComitesPage(navCtrl, 
        //private navParams: NavParams,
        modalCtrl, comiteProvider, utils, auth) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.comiteProvider = comiteProvider;
        this.utils = utils;
        this.auth = auth;
        this.itemsPerPage = 4;
        this.currentPage = 1;
        this.totalPages = 1;
        this.items = [];
        this._itemsResult = [];
        this.itemsEnded = false;
        this.estados = [];
        this.cidades = [];
    }
    ComitesPage.prototype.ionViewDidLoad = function () {
        this.getData();
    };
    ComitesPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present().then().catch();
    };
    ComitesPage.prototype.getData = function () {
        var _this = this;
        this.comiteProvider.findAll(this.auth.getToken()).then(function (result) {
            // Filter
            if (_this.modelCidade && _this.modelEstado) {
                _this._itemsResult = _this.filterResult(result);
            }
            else {
                _this._itemsResult = result;
            }
            _this.totalPages = Math.ceil(_this._itemsResult.length / _this.itemsPerPage);
            _this.items = _this._itemsResult.slice(0, _this.itemsPerPage);
        }, function (err) {
            console.log('Erro:', err);
        });
    };
    ComitesPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getData();
            refresher.complete();
        }, 1000);
    };
    ComitesPage.prototype.doInfinite = function (infiniteScroll) {
        if (!this.itemsEnded && this.currentPage < this.totalPages) {
            var sliceItems = this._itemsResult.slice(this.currentPage * this.itemsPerPage, ++this.currentPage * this.itemsPerPage);
            this.items = this.items.concat(sliceItems);
            this.currentPage++;
        }
        else {
            this.itemsEnded = true;
        }
        setTimeout(function () {
            infiniteScroll.complete();
        }, 500);
    };
    ComitesPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, { id: id });
    };
    ComitesPage.prototype.getNameFromUf = function (uf) {
        return this.utils.getNameFromUf(uf);
    };
    ComitesPage.prototype.changeEstado = function () {
        if (this.modelEstado) {
            var estado = this.utils.getFromUf(this.modelEstado);
            this.cidades = estado.cidades;
        }
    };
    ComitesPage.prototype.changeCidade = function () {
        this.getData();
    };
    ComitesPage.prototype.filterResult = function (result) {
        var _this = this;
        if (result.length) {
            var filtered = result.filter(function (elem) {
                return elem.Cidade == _this.modelCidade && elem.Estado == _this.modelEstado;
            });
            return filtered;
        }
        return result;
    };
    ComitesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-comites',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/comites/comites.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Comites</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n        <ion-icon name="search"></ion-icon>\n      </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n      <div class="wrapError" *ngIf="(items).length === 0">\n        <i class="fa fa-smile-o" aria-hidden="true"></i>\n        <p>Aguarde. Em breve estará disponível a lista de comitês.</p>\n      </div>\n  <ion-list margin-top *ngIf="items.length">\n    <ion-card *ngFor="let item of items" (click)="openDetail(\'comites-detalhe\', item.Id)">\n      <ion-card-content class="flex-card">\n        <div>\n          <ion-card-title [innerHtml]="item.Cidade"></ion-card-title>\n          {{item.Cidade}} / <span [innerHtml]="getNameFromUf(item.Estado)"></span>\n  \n          <ion-note>\n            <ion-icon name="pin"></ion-icon>\n            {{ item.Endereco }}\n          </ion-note>\n        </div>\n        <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n<!-- \n  <ion-list margin-top *ngIf="items.length == 0">\n    <ion-card>\n      <ion-card-content class="flex-card">\n        <div>\n          <p>Nenhum comitê encontrado.</p>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </ion-list> -->\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="!itemsEnded">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/comites/comites.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_comite_comite__["a" /* ComiteProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */]])
    ], ComitesPage);
    return ComitesPage;
}());

//# sourceMappingURL=comites.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoluntarioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_voluntario_voluntario__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__social_share_social_share__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detail_programa_detail_programa__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var VoluntarioPage = /** @class */ (function () {
    function VoluntarioPage(navCtrl, navParams, voluntario, loadingCtrl, utils, person, modalCtrl, _conteudo) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.voluntario = voluntario;
        this.loadingCtrl = loadingCtrl;
        this.utils = utils;
        this.person = person;
        this.modalCtrl = modalCtrl;
        this._conteudo = _conteudo;
        this.getData();
    }
    VoluntarioPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VoluntarioPage');
    };
    VoluntarioPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, id);
    };
    VoluntarioPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    VoluntarioPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getData();
            refresher.complete();
        }, 1000);
    };
    VoluntarioPage.prototype.getData = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.voluntario.findAll().then(function (result) {
            loading.dismiss();
            _this.listSejaVoluntario = result;
            console.log(result);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    VoluntarioPage.prototype.postLike = function (conteudo) {
        var _this = this;
        var idPost = conteudo.Id;
        this.person.get()
            .then(function (response) {
            var person = response;
            _this._conteudo.postLike(idPost, person.Id)
                .then(function (response) {
                if (response.Curtiu && !conteudo.JaCurtiu) {
                    conteudo.Curtidas += 1;
                }
                else {
                    conteudo.Curtidas -= 1;
                }
                conteudo.JaCurtiu = response.Curtiu;
                if (response.GanhouPonto) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                }
            });
        });
    };
    VoluntarioPage.prototype.postShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    VoluntarioPage.prototype.openDetails = function (l) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__detail_programa_detail_programa__["a" /* DetailProgramaPage */], {
            interna: l
        });
    };
    VoluntarioPage.prototype.openExternal = function (url) {
    };
    VoluntarioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-voluntario',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/voluntario/voluntario.html"*/'<ion-header color="header">\n  <style>\n    .has-refresher > .scroll-content{\nbackground-color: #fff!important\n    }\n    </style>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Seja um Voluntário</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding style="background:#fff;    padding-top: 2%;\nmargin-top: -1%;"  >\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  \n  <ion-list>\n    <ion-item *ngFor="let l of listSejaVoluntario" (click)="openDetail(\'voluntario-detalhe\', l.Id)">\n      <ion-avatar item-start *ngIf="l.Arquivo!=null">\n        <img src="{{l.Arquivo.Url}}">\n      </ion-avatar>\n      <h2>{{l.Titulo}}</h2>\n      <p>{{l.Chamada}}</p>\n      <!-- <ion-icon name="ios-arrow-forward"></ion-icon> -->\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/voluntario/voluntario.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_voluntario_voluntario__["a" /* VoluntarioProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__["a" /* ConteudoProvider */]])
    ], VoluntarioPage);
    return VoluntarioPage;
}());

//# sourceMappingURL=voluntario.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailProgramaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DetailProgramaPage = /** @class */ (function () {
    function DetailProgramaPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.interna = this.navParams.get('interna');
        console.log(this.interna);
    }
    DetailProgramaPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    DetailProgramaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailProgramaPage');
    };
    DetailProgramaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-detail-programa',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/detail-programa/detail-programa.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <ion-title text-center>{{interna.Titulo}}</ion-title>\n    <ion-buttons end>\n      <button  class="search-header">\n      <i class="fa fa-search" aria-hidden="true"></i>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <div [innerHTML]="interna.Texto"></div>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/detail-programa/detail-programa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], DetailProgramaPage);
    return DetailProgramaPage;
}());

//# sourceMappingURL=detail-programa.js.map

/***/ }),

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export User */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());

var AuthProvider = /** @class */ (function () {
    function AuthProvider(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    AuthProvider.prototype.postLogin = function (credentials) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/token";
        var body = "grant_type=password&username=" + credentials.username + "&password=" + credentials.password + "&appId=" + __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].appId;
        var header = { "headers": { "Content-Type": 'application/x-www-form-urlencoded' } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AuthProvider.prototype.postResetPassWord = function (email) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa/RecuperarSenha";
            var data = {
                Email: email,
                ClienteId: __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].clienteId
            };
            _this.http.post(url, data)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AuthProvider.prototype.saveSection = function (data) {
        localStorage.setItem('access_token', data.access_token);
        this.storage.set('access_token', data.access_token);
    };
    AuthProvider.prototype.isLogged = function () {
        if (this.getToken()) {
            return true;
        }
        return false;
    };
    AuthProvider.prototype.logoutUser = function () {
        localStorage.removeItem('profileName');
        localStorage.removeItem('access_token');
        this.storage.remove('profileName');
        this.storage.remove('access_token');
    };
    AuthProvider.prototype.getToken = function () {
        return localStorage.getItem('access_token');
        // return this.storage.get('access_token')
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificacoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NotificacoesPage = /** @class */ (function () {
    function NotificacoesPage(navCtrl, navParams, notification, loadingCtrl, _sanitizer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.notification = notification;
        this.loadingCtrl = loadingCtrl;
        this._sanitizer = _sanitizer;
        this.getNotifications();
        this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/zlNuRWWDo7w?autoplay=1');
    }
    NotificacoesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificacoesPage');
    };
    NotificacoesPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getNotifications();
            refresher.complete();
        }, 1000);
    };
    NotificacoesPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, id);
    };
    NotificacoesPage.prototype.getNotifications = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.notification.Get().then(function (response) {
            loading.dismiss();
            _this.notificationsList = response;
            console.log(response);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    NotificacoesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notificacoes',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/notificacoes/notificacoes.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Notificações</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n\n    \n\n  <ion-list margin-top>\n    <ion-card (click)="openDetail(\'notificacoes\', notification.Id)" *ngFor="let notification of notificationsList" [ngClass]="{\'naolidanot\': notification.Lido == false}">\n      <ion-card-content>\n        <div>\n          <ion-card-title>{{notification.Titulo}}</ion-card-title>\n          <p [innerHTML]="notification.Mensagem | excerpt:100"></p>\n          <ion-note>  <ion-icon name="calendar"></ion-icon> {{notification.DataCriacao}}</ion-note>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/notificacoes/notificacoes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_notificacao_notificacao__["a" /* NotificationProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* DomSanitizer */]])
    ], NotificacoesPage);
    return NotificacoesPage;
}());

//# sourceMappingURL=notificacoes.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgramaGovernoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__convide_amigos__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_geral_geral__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__social_share_social_share__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detail_programa_detail_programa__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ProgramaGovernoPage = /** @class */ (function () {
    function ProgramaGovernoPage(navCtrl, navParams, modalCtrl, geral, loadingCtrl, utils, person, _conteudo) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.geral = geral;
        this.loadingCtrl = loadingCtrl;
        this.utils = utils;
        this.person = person;
        this._conteudo = _conteudo;
        this.programa = [];
    }
    ProgramaGovernoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProgramaGovernoPage');
        this.getPrograma();
    };
    ProgramaGovernoPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getPrograma();
            refresher.complete();
        }, 1000);
    };
    ProgramaGovernoPage.prototype.inviteFriends = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__convide_amigos__["a" /* ConvideAmigosPage */]);
        modal.present();
    };
    ProgramaGovernoPage.prototype.getPrograma = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.geral.findPrograma()
            .then(function (data) {
            loading.dismiss();
            _this.programa = _this.programa.concat(data);
            console.log(_this.programa, 'Progrmaa');
            console.log(data, 'Programa ssss');
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    ProgramaGovernoPage.prototype.postLike = function (conteudo) {
        var _this = this;
        var idPost = conteudo.Id;
        this.person.get()
            .then(function (response) {
            var person = response;
            _this._conteudo.postLike(idPost, person.Id)
                .then(function (response) {
                if (response.Curtiu && !conteudo.JaCurtiu) {
                    conteudo.Curtidas += 1;
                }
                else {
                    conteudo.Curtidas -= 1;
                }
                conteudo.JaCurtiu = response.Curtiu;
                if (response.GanhouPonto) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                }
            });
        });
    };
    ProgramaGovernoPage.prototype.postShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    ProgramaGovernoPage.prototype.openDetails = function (l) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__detail_programa_detail_programa__["a" /* DetailProgramaPage */], {
            interna: l
        });
    };
    ProgramaGovernoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-programa-governo',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/programa-governo/programa-governo.html"*/'<ion-header color="header">\n  <style>\n    .has-refresher > .scroll-content{\n      background-color: #fff!important\n    }\n    .button-clear-ios{\n      color:#bfbfbf;\n    }\n    .button-clear-md{\n      color:#bfbfbf;\n\n    }\n    .button-clear-md:hover:not(.disable-hover) {\n    background-color: transparent!important;\n}\n    </style>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Programa de Governo</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding >\n  \n  <div class="wrapError" *ngIf="(programa).length === 0">\n    <i class="fa fa-smile-o" aria-hidden="true"></i>\n    <p>Aguarde. Em breve esse conteúdo estará disponível.</p>\n  </div>\n\n  <ion-list>\n    <ion-item *ngFor="let l of programa" (click)="openDetails(l)">\n      <ion-avatar item-start>\n        <img src="{{l.Imagem}}">\n      </ion-avatar>\n      <h2>{{l.Titulo}}</h2>\n      <ion-icon name="ios-arrow-forward"></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/programa-governo/programa-governo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_geral_geral__["a" /* GeralProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__["a" /* ConteudoProvider */]])
    ], ProgramaGovernoPage);
    return ProgramaGovernoPage;
}());

//# sourceMappingURL=programa-governo.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaterialInstitucionalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_institucional_institucional__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_photo_library__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__social_share_social_share__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MaterialInstitucionalPage = /** @class */ (function () {
    function MaterialInstitucionalPage(navCtrl, navParams, loadingCtrl, material, platform, photoLibrary, file, transfer, alertCtrl, utils, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.material = material;
        this.platform = platform;
        this.photoLibrary = photoLibrary;
        this.file = file;
        this.transfer = transfer;
        this.alertCtrl = alertCtrl;
        this.utils = utils;
        this.modalCtrl = modalCtrl;
        this.IsAnonimo = localStorage.getItem('profileName');
        this.getData();
    }
    MaterialInstitucionalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MaterialInstitucionalPage');
    };
    MaterialInstitucionalPage.prototype.doInfinite = function (infiniteScroll) {
        console.log('Begin async operation');
        setTimeout(function () {
            infiniteScroll.complete();
        }, 500);
    };
    MaterialInstitucionalPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getData();
            refresher.complete();
        }, 1000);
    };
    MaterialInstitucionalPage.prototype.getData = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.material.findAcervo().then(function (result) {
            loading.dismiss();
            _this.listMaterial = result;
            console.log(result);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    MaterialInstitucionalPage.prototype.doDownload = function (imagem) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        if (this.IsAnonimo == 'Anônimo') {
            this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
            loading.dismiss();
        }
        else {
            var fileTransfer = this.transfer.create();
            var directory = '';
            var fileName = imagem.substring(imagem.lastIndexOf('/') + 1);
            if (this.platform.is('ios')) {
                directory = this.file.documentsDirectory;
            }
            else if (this.platform.is('android')) {
                directory = this.file.externalRootDirectory + 'Download/';
            }
            this.photoLibrary.requestAuthorization().then(function () {
                _this.photoLibrary.saveImage(imagem, 'Downloads').then(function (data) {
                    console.log(data, 'data');
                    loading.dismiss();
                    var alertFailure = _this.alertCtrl.create({
                        title: "Download Conclu\u00EDdo!",
                        buttons: ['Ok']
                    });
                    alertFailure.present();
                }, function (err) {
                    loading.dismiss();
                    console.log('Failure', err);
                    var alertFailure = _this.alertCtrl.create({
                        title: "Falha no Download!",
                        buttons: ['Ok']
                    });
                    alertFailure.present();
                });
            });
        }
    };
    MaterialInstitucionalPage.prototype.getFile = function (arquivo) {
        this.utils.downloadFile(arquivo);
    };
    MaterialInstitucionalPage.prototype.postShare = function (conteudo) {
        if (this.IsAnonimo == 'Anônimo') {
            this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
        }
        else {
            conteudo.AcervoId = conteudo.Id;
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__social_share_social_share__["a" /* SocialSharePage */], conteudo);
            modal.present();
        }
    };
    MaterialInstitucionalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-material-institucional',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/material-institucional/material-institucional.html"*/'<ion-header color="header">\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title text-center>Material de Campanha</ion-title>\n      <ion-buttons end>\n        <!-- <button (click)="searchToggle()" class="search-header">\n        <ion-icon name="search"></ion-icon>\n      </button> -->\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n      <ion-refresher (ionRefresh)="doRefresh($event)">\n          <ion-refresher-content></ion-refresher-content>\n        </ion-refresher>\n    <section>\n      <!-- <ion-scroll scrollX="true" class="scrolling-links tabs-links">\n        <button class="item active">Todos</button>\n        <button class="item">Santinhos</button>\n        <button class="item">Cartazes</button>\n        <button class="item">Flipetas</button>\n      </ion-scroll> -->\n      \n      <div>\n        <div class="wrapError" *ngIf="(listMaterial) == \'\'">\n            <i class="fa fa-smile-o" aria-hidden="true"></i>\n            <p>Aguarde. Em breve estará disponível a agenda de eventos e encontros.</p>\n        </div>\n        <ion-card *ngFor="let material of listMaterial">\n          <img src="{{material.Imagem}}"/>\n          <ion-card-content>\n            <ion-card-title>{{material.Titulo}}</ion-card-title>\n            <p [innerHtml]= "material.SubTitulo"></p>\n          </ion-card-content>\n          <ion-row class="footer-card">\n            <ion-col style="padding-right: 15%;">\n              <button ion-button (click)="doDownload(material.Imagem)">Baixar</button>\n            </ion-col>\n            <ion-col>\n              <button ion-button icon-right text-left clear small (click)="postShare(material)">\n                <div text-right>Compartilhe e <br/> ganhe <strong>{{material.PontuacaoCompartilhamento}} pontos</strong></div>\n                <!-- <ion-icon name="share"></ion-icon> -->\n                <img src="assets/imgs/sharenot.png" style="width: 14%;\n                margin-left: 6%;">\n              </button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </div>\n      <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n      </ion-infinite-scroll>\n    </section>\n  </ion-content>\n  '/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/material-institucional/material-institucional.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_7__ionic_native_photo_library__["a" /* PhotoLibrary */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_institucional_institucional__["a" /* InstitucionalProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_photo_library__["a" /* PhotoLibrary */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], MaterialInstitucionalPage);
    return MaterialInstitucionalPage;
}());

//# sourceMappingURL=material-institucional.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgramaPontosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_ponto_ponto__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pesquisa_pesquisa__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_shared_service_shared_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__social_share_social_share__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ProgramaPontosPage = /** @class */ (function () {
    function ProgramaPontosPage(navCtrl, navParams, modalCtrl, pontos, loadingCtrl, utils, pessoa, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.pontos = pontos;
        this.loadingCtrl = loadingCtrl;
        this.utils = utils;
        this.pessoa = pessoa;
        this.events = events;
        this.tab = 'como-funciona';
        this.IsAnonimo = localStorage.getItem('profileName');
        if (navParams.data != 0) {
            this.tab = navParams.data;
            this.events.publish('menu:closed', '');
        }
        this.loadAll();
    }
    ProgramaPontosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProgramaPontosPage');
    };
    ProgramaPontosPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.loadAll();
            refresher.complete();
        }, 1000);
    };
    ProgramaPontosPage.prototype.loadExtrato = function () {
        var _this = this;
        this.pontos.getExtrato().then(function (result) {
            _this.extrato = result;
            console.log(result);
        }, function (error) {
        });
    };
    ProgramaPontosPage.prototype.loadMedalhas = function () {
        var _this = this;
        this.pontos.getMedalhas().then(function (result) {
            _this.medalhas = result;
            console.log(result);
        }, function (error) {
        });
    };
    ProgramaPontosPage.prototype.loadAll = function () {
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.getPersonDetail();
        this.loadExtrato();
        this.loadMedalhas();
        loading.dismiss();
    };
    ProgramaPontosPage.prototype.getPersonDetail = function () {
        var _this = this;
        this.pessoa.get().then(function (response) {
            _this.TotalPontos = response.TotalPontos;
            _this.MedalhaAtual = response.Medalha ? response.Medalha.Nome : "";
        }, function (error) {
            console.log(error);
        });
    };
    ProgramaPontosPage.prototype.openDetail = function () {
        if (this.IsAnonimo == 'Anônimo') {
            this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
        }
        else {
            this.navCtrl.push('perfil');
        }
    };
    ProgramaPontosPage.prototype.openPesquisa = function () {
        if (this.IsAnonimo == 'Anônimo') {
            this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pesquisa_pesquisa__["b" /* PesquisaPage */]);
        }
    };
    ProgramaPontosPage.prototype.sharing = function (conteudo) {
        if (this.IsAnonimo == 'Anônimo') {
            this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
        }
        else {
            var conteudoBody = {
                Titulo: conteudo + '#APPVoluntariosdaPatria17',
                Id: 10067,
                Imagem: 'http://mypolis.com.br/conteudo/voluntarios/postcompartilhar.jpg'
            };
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__social_share_social_share__["a" /* SocialSharePage */], conteudoBody);
            modal.present();
        }
    };
    ProgramaPontosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-programa-pontos',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/programa-pontos/programa-pontos.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Programa de Pontos</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <header class="information-user">\n    <div>\n        <small>Você possui</small>\n        <p><strong>{{TotalPontos}}</strong> pontos</p>\n    </div>\n    <div>\n        <small>Você é um</small>\n        <p><strong>{{MedalhaAtual}}</strong></p>\n    </div>\n  </header>\n  <ion-segment [(ngModel)]="tab" color="primary" class="wrap">\n    <ion-segment-button value="como-funciona">Como Funciona</ion-segment-button>\n    <ion-segment-button value="historico">Histórico</ion-segment-button>\n    <ion-segment-button value="medalhas">Medalhas</ion-segment-button>\n  </ion-segment>\n  <div padding>\n    <div [ngSwitch]="tab">\n      <div *ngSwitchCase="\'como-funciona\'">\n        <div class="block-text">\n          <p>Quanto mais você usa o aplicativo e executa determinadas ações, mais pontos você ganha. Usuários engajados têm pontuação mais alta.</p>\n        </div>\n        <ion-list class="padding-section">\n          <ion-card>\n            <ion-card-content class="flex-card">\n              <ion-icon class="set" name="phone-portrait"></ion-icon>\n              <div>\n                <ion-card-title>Interagindo com o aplicativo</ion-card-title>\n                <p>Compartilhe os conteúdos do aplicativo, dê curtidas, crie participações. Cada ação te dá mais pontos.</p>\n              </div>\n            </ion-card-content>\n          </ion-card>\n          <ion-card>\n            <ion-card-content class="flex-card">\n              <ion-icon class="set" name="clipboard"></ion-icon>\n              <div>\n                <ion-card-title>Complete seu cadastro</ion-card-title>\n                <p>Informe seus dados completando seu cadastro e você ganhará ainda mais pontos.É simples e rápido.</p>\n                <button ion-button outline (click)="openDetail()">Completar Agora</button>\n              </div>\n            </ion-card-content>\n          </ion-card>\n          <ion-card>\n            <ion-card-content class="flex-card">\n              <ion-icon class="set" name="text"></ion-icon>\n              <div>\n                <ion-card-title>Responda Pesquisas</ion-card-title>\n                <p>A cada pesquisa que você participa, você ganha novos pontos. Veja as pesquisas em aberto e participe.</p>\n                <button ion-button outline (click)="openPesquisa()">Ver Pesquisas</button>\n              </div>\n            </ion-card-content>\n          </ion-card>\n          <ion-card>\n            <ion-card-content class="flex-card">\n              <ion-icon class="set" name="body"></ion-icon>\n              <div>\n                <ion-card-title>Convide Amigos</ion-card-title>\n                <p>Juntos somos mais fortes. Quanto mais amigos engajados nessa causa, mais força teremos na mudança do país.</p>\n                <button ion-button outline (click)="sharing(\'Venha ser um Voluntário da Pátria. Baixe o aplicativo e faça seu cadastro.\')">Enviar Convite</button>\n              </div>\n            </ion-card-content>\n          </ion-card>\n        </ion-list>\n      </div>\n      <div *ngSwitchCase="\'historico\'">\n        <div class="block-text">\n          <p>Confira seu histórico de ações e os pontos ganhos em cada uma através da lista abaixo.</p>\n        </div>\n        <div class="box-round">\n          <header>Histórico de Pontos</header>\n          <ion-list>\n            <ion-item *ngFor="let  ext of extrato">\n              <ion-icon name="add" item-start></ion-icon>\n              <div>\n                <small>{{ext.DataFormatada}}</small>\n                <h3>{{ext.RegraPonto.NomeTipo}}</h3>\n              </div>\n              <div item-end class="plus">+{{ext.Pontos}}pts</div>\n            </ion-item>\n          </ion-list>\n        </div>\n      </div>\n      <div *ngSwitchCase="\'medalhas\'">\n        <div class="block-text">\n          <p>Quanto mais pontos você fizer, mais medalhas você conquista. Confira as medalhas disponveis e a pontuação que você precisa obter para cada uma.            </p>\n        </div>\n        <div class="box-round">\n          <header>Categoria</header>\n          <ion-list>\n            <ion-item *ngFor="let medalha of medalhas">\n              <div class="flexTitle">\n                <img src="{{medalha.ImagemUrl}}" alt=""> \n                <h3>{{medalha.Nome}}</h3>\n              </div>\n              <div item-end class="plus">{{medalha.Pontos}}pts</div>\n            </ion-item>\n          </ion-list>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/programa-pontos/programa-pontos.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_7__providers_shared_service_shared_service__["a" /* SharedService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_ponto_ponto__["a" /* PontoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], ProgramaPontosPage);
    return ProgramaPontosPage;
}());

//# sourceMappingURL=programa-pontos.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificacoesNotificacoesDetalhePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificacoesNotificacoesDetalhePage = /** @class */ (function () {
    function NotificacoesNotificacoesDetalhePage(navCtrl, navParams, notification, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.notification = notification;
        this.loadingCtrl = loadingCtrl;
        this.notif = {
            DataCriacao: '',
            Id: '',
            Imagem: '',
            Lido: '',
            Mensagem: '',
            Titulo: ''
        };
        this.getNotification();
    }
    NotificacoesNotificacoesDetalhePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificacoesNotificacoesDetalhePage');
    };
    NotificacoesNotificacoesDetalhePage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getNotification();
            refresher.complete();
        }, 1000);
    };
    NotificacoesNotificacoesDetalhePage.prototype.getNotification = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        console.log(this.navParams.data);
        this.notification.GetDetail(this.navParams.data).then(function (response) {
            loading.dismiss();
            _this.notif = response;
            console.log(response);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    NotificacoesNotificacoesDetalhePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notificacoes-notificacoes-detalhe',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Notificações</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n    <ion-card>\n        <img *ngIf="notif.Imagem" [src]="notif.Imagem" class="img-featured">\n      <ion-card-content>\n        <div>\n          <ion-card-title>{{notif.Titulo}}</ion-card-title>\n          <ion-note>  <ion-icon name="calendar"></ion-icon> {{notif.DataCriacao}}</ion-note>\n        </div>\n        <div class="padding-border-top">\n            <p [innerHTML]="notif.Mensagem"></p>\n        </div>\n      </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_notificacao_notificacao__["a" /* NotificationProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */]])
    ], NotificacoesNotificacoesDetalhePage);
    return NotificacoesNotificacoesDetalhePage;
}());

//# sourceMappingURL=notificacoes-notificacoes-detalhe.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__excerpt_excerpt__ = __webpack_require__(339);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notifications_notifications__ = __webpack_require__(340);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__excerpt_excerpt__["a" /* ExcerptPipe */],
                __WEBPACK_IMPORTED_MODULE_2__notifications_notifications__["a" /* NotificationsPipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__excerpt_excerpt__["a" /* ExcerptPipe */],
                __WEBPACK_IMPORTED_MODULE_2__notifications_notifications__["a" /* NotificationsPipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CONFIG_PROJECT; });
var CONFIG_PROJECT = {
    name: "Voluntários da Pátria",
    appId: 'voluntarios',
    baseApi: 'https://mypolis.com.br/api',
    // baseApi: 'http://localhost:57438/api',
    // clienteId: 4,
    clienteId: 20471,
    ApiKeyGoogleMaps: 'AIzaSyCO-yjQ862Y2DRPc7S0NdG8AYaSbNLEZFk'
};
// voluntarios
// 20471 
//# sourceMappingURL=app-config.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgendaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AgendaProvider = /** @class */ (function () {
    function AgendaProvider(http) {
        this.http = http;
    }
    AgendaProvider.prototype.getLista = function (token, pag, size) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Agenda?Page=" + pag + "&Size=" + size;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AgendaProvider.prototype.getListaSemPaginar = function (token) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Agenda?$Paginar=" + false;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AgendaProvider.prototype.filterCity = function (token, filter, pag, skip) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Agenda?Paginar=" + false + ("$skip=" + skip + "&$top=" + pag + "&$filter=Local eq '" + (filter.cidade + ', ' + filter.estado) + "'");
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AgendaProvider.prototype.getDetalhe = function (token, id) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Agenda/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AgendaProvider.prototype.filterData = function (token, filtro) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Agenda/Filtro/" + filtro;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AgendaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], AgendaProvider);
    return AgendaProvider;
}());

//# sourceMappingURL=agenda.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComiteProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ComiteProvider = /** @class */ (function () {
    function ComiteProvider(http) {
        this.http = http;
    }
    ComiteProvider.prototype.getHeader = function (token) {
        return {
            "headers": {
                "Content-Type": 'application/json',
                "Authorization": "Bearer " + token
            }
        };
    };
    ComiteProvider.prototype.getPromise = function (url, header) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ComiteProvider.prototype.findAll = function (token) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Comites", header = this.getHeader(token);
        return this.getPromise(url, header);
    };
    ComiteProvider.prototype.findById = function (token, comiteId) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Comites/" + comiteId, header = this.getHeader(token);
        return this.getPromise(url, header);
    };
    ComiteProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ComiteProvider);
    return ComiteProvider;
}());

//# sourceMappingURL=comite.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoluntarioProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VoluntarioProvider = /** @class */ (function () {
    function VoluntarioProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    VoluntarioProvider.prototype.findAll = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos?TipoConteudoId=15&Size=10";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    VoluntarioProvider.prototype.findById = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    VoluntarioProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], VoluntarioProvider);
    return VoluntarioProvider;
}());

//# sourceMappingURL=voluntario.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PesquisaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PesquisaProvider = /** @class */ (function () {
    function PesquisaProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    PesquisaProvider.prototype.findAll = function () {
        var _this = this;
        console.log('buscando');
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pesquisas";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                var lista = [];
                console.log(result);
                for (var z = 0; z < result.length; z++) {
                    for (var j = 0; j < result[z].Pesquisas.length; j++) {
                        result[z].Pesquisas[j].Pontuacao = result[z].Pontuacao;
                        result[z].Pesquisas[j].Tema = result[z].Tema;
                        result[z].Pesquisas[j].ExibirGrafico = result[z].Status == 4;
                        result[z].Pesquisas[j].Finalizada = result[z].Status == 4 || result[z].Status == 3;
                        for (var i = 0; i < result[z].Pesquisas[j].PesquisasRespostas.length; i++) {
                            var value = result[z].Pesquisas[j].PesquisasRespostas[i];
                            if (value.Resposta == 'Sim' || value.Resposta == 'A Favor') {
                                result[z].Pesquisas[j].agree = value;
                            }
                            else if (value.Resposta == 'Não' || value.Resposta == 'Contra') {
                                result[z].Pesquisas[j].negative = value;
                            }
                        }
                        ;
                        lista.push({ Tema: result.Tema, Pesquisa: result[z].Pesquisas[j] });
                    }
                }
                resolve(lista);
            }, function (error) {
                reject(error);
            });
        });
    };
    PesquisaProvider.prototype.post = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pesquisas/Participar";
        var header = { "headers": { "Authorization": "Bearer " + token } };
        var body = {
            RespostaId: id
        };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PesquisaProvider.prototype.findById = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pesquisa/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                for (var i = 0; i < result.Grafico[0].DadosGrafico.length; i++) {
                    var pergunta = result.Grafico[0].DadosGrafico[i];
                    if (pergunta.Texto == 'Sim' || pergunta.Texto == 'A Favor') {
                        result.agreePercent = pergunta.Porcentagem;
                        result.agreeText = pergunta.Texto;
                    }
                    else if (pergunta.Texto == 'Não' || pergunta.Texto == 'Contra') {
                        result.negativePercent = pergunta.Porcentagem;
                        result.negativeText = pergunta.Texto;
                    }
                }
                result.mensagem = result.Mensagem;
                result.ExibirGrafico = result.ExibeResultados;
                result.Pontuacao = result.Pesquisa.Pontuacao;
                //resultado.ExibirGrafico = true;
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PesquisaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], PesquisaProvider);
    return PesquisaProvider;
}());

//# sourceMappingURL=pesquisa.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_facebook__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_app_config__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, auth, loadingCtrl, alertCtrl, fb, events, http, notification, person) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this.events = events;
        this.http = http;
        this.notification = notification;
        this.person = person;
        this.HomePage = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.user = {
            username: '',
            password: ''
        };
        this.form = {
            RoleId: 2,
            Nome: '',
            Email: '',
            Numero: '',
            Senha: '',
            ConfirmarSenha: '',
            clienteId: 20471
        };
    }
    LoginPage.prototype.ionViewDidLoad = function () { };
    LoginPage.prototype.goPage = function (page) {
        this.navCtrl.push(page);
    };
    LoginPage.prototype.onLogin = function (user) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        if (this.user.username == '') {
            this.alertCtrl.create({
                title: '<img src="assets/icon/error.png"/> Erro',
                subTitle: 'Usuario ou senha inválidos',
                buttons: [{ text: 'Ok' }],
                cssClass: 'alertcss'
            }).present();
            loading.dismiss();
        }
        else {
            this.auth.postLogin(user)
                .then(function (response) {
                setTimeout(function () {
                    _this.auth.saveSection(response);
                    loading.dismiss();
                    _this.events.publish('profile', response);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
                }, 2000);
            }, function (error) {
                setTimeout(function () {
                    _this.alertCtrl.create({
                        title: '<img src="assets/icon/error.png"/> Erro',
                        subTitle: error.error.error_description,
                        buttons: [{ text: 'Ok' }],
                        cssClass: 'alertcss'
                    }).present();
                    loading.dismiss();
                }, 2000);
            });
        }
    };
    LoginPage.prototype.onHome = function (user) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        user = {
            username: '',
            password: ''
        };
        this.auth.postLogin(user)
            .then(function (response) {
            setTimeout(function () {
                _this.auth.saveSection(response);
                _this.events.publish('profile', response);
                loading.dismiss();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
            }, 2000);
        }, function (error) {
            setTimeout(function () {
                _this.alertCtrl.create({
                    title: '<img src="assets/icon/error.png"/> Erro',
                    subTitle: error.error.error_description,
                    buttons: [{ text: 'Ok' }],
                    cssClass: 'alertcss'
                }).present();
                loading.dismiss();
            }, 2000);
        });
    };
    LoginPage.prototype.loginToFacebook = function () {
        var _this = this;
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                _this.getUserDetail(res.authResponse.userID, res.authResponse.accessToken);
            }
            else {
            }
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    LoginPage.prototype.getUserDetail = function (userid, usertoken) {
        var _this = this;
        this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            _this.InfosFb = res;
            var headers = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            _this.http.get("" + __WEBPACK_IMPORTED_MODULE_8__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/Pessoa/VerificaFacebook?FacebookId=' + _this.InfosFb.id + '&ClienteCod=20471').map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                if (data.Existe == true) {
                    var headers_1 = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]({
                        'Content-Type': 'application/x-www-form-urlencoded'
                    });
                    var body = "grant_type=password&username=facebook:" + res.id + "&password=" + usertoken + "&appId=" + __WEBPACK_IMPORTED_MODULE_8__providers_app_config__["a" /* CONFIG_PROJECT */].appId;
                    return _this.http.post("" + __WEBPACK_IMPORTED_MODULE_8__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/token', body, { headers: headers_1 }).map(function (res) { return res.json(); })
                        .subscribe(function (data) {
                        localStorage.setItem('access_token', data.access_token);
                        localStorage.setItem("cacheUsuario", JSON.stringify(data));
                        var loading = _this.loadingCtrl.create({
                            content: 'Carregando...'
                        });
                        loading.present();
                        localStorage.removeItem('profile');
                        _this.events.publish('user:Login', data);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */], {});
                        loading.dismiss();
                    });
                }
                else {
                    var headers_2 = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]({
                        'Content-Type': 'application/json'
                    });
                    var body = {
                        Nome: _this.InfosFb.name,
                        Email: _this.InfosFb.email,
                        FacebookId: _this.InfosFb.id,
                        Origem: 'aplicativo',
                        RoleId: '2',
                        clienteId: 20471,
                    };
                    return _this.http.post("" + __WEBPACK_IMPORTED_MODULE_8__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/Pessoa/CadastroFacebook', body, { headers: headers_2 }).map(function (res) { return res.json(); })
                        .subscribe(function (data) {
                        console.log(data);
                        var headers = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]({
                            'Content-Type': 'application/x-www-form-urlencoded'
                        });
                        var options = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["d" /* RequestOptions */]({ headers: headers });
                        var body = "grant_type=password&username=facebook:" + res.id + "&password=" + usertoken + "&appId=" + __WEBPACK_IMPORTED_MODULE_8__providers_app_config__["a" /* CONFIG_PROJECT */].appId;
                        return _this.http.post("" + __WEBPACK_IMPORTED_MODULE_8__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/token', body, { headers: headers }).map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            localStorage.setItem('access_token', data.access_token);
                            var loading = _this.loadingCtrl.create({
                                content: 'Carregando...'
                            });
                            loading.present();
                            localStorage.setItem("cacheUsuario", JSON.stringify(data));
                            localStorage.removeItem('profile');
                            _this.events.publish('user:Login', data);
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */], {});
                            loading.dismiss();
                        });
                    }, function (err) {
                        console.log("ERROR!: ", err);
                    });
                }
            });
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/login/login.html"*/'<ion-content padding class="padding-fixed" color="light">\n  <style>\n    .imgvp {\n\n    width: 65% !important;\n\n}\n    </style>\n    <header class="login-header" text-center>\n        <img src="assets/imgs/logo.png" class="brand">\n        <div class="vp" style="    width: 70%;\n        margin: 0 auto;\n        margin-top: -1%;">\n          <img src="assets/imgs/voluntario_patria.png" class="brand imgvp" style="width:100%!important;">\n\n        </div>\n\n        <h3 class="title-white">Seja Bem-Vindo!</h3>\n        <div class="block-text">\n          <p class="subtitleblue">Antes de seguir, você precisa se identificar</p>\n        </div>\n    </header>\n    <form autocomplete="off" #loginform="ngForm" (ngSubmit)="onLogin(user)">\n      <ion-list>\n        <ion-item class="input-block-color">\n          <ion-input type="email" required color="light" placeholder="Insira seu e-mail" [(ngModel)]="user.username" name="username"></ion-input>\n        </ion-item>\n      \n        <ion-item class="input-block-color">\n          <ion-input type="password" required placeholder="Insira sua Senha" [(ngModel)]="user.password"  name="password"></ion-input>\n        </ion-item>\n      </ion-list>\n      <button ion-button full color="light" text-uppercase class="butaoentrar" style="    background: #0276a9;\n      color: #fff;\n      font-weight: bold;">Entrar</button>\n    </form>\n    \n    <div (click)="goPage(\'esqueci-senha\')" text-center class="link">Esqueci minha senha</div>\n\n    <button ion-button outline full text-uppercase color="light" class="botaologfb" icon-left (click)="loginToFacebook()" style="    color: #0276a9!important;\n    font-weight: bold!important;width:100%;"> \n      <img class="ico-general-button" src="assets/imgs/ico-facebook.png" width="9" height="16"> Logar pelo Facebook\n    </button>\n    <div (click)="goPage(\'cadastro-tipoperfil\')" text-center class="link" style="    color: #509f6a;\n    font-weight: 600;\n    font-size: 1.1em;\n    text-transform: uppercase;">Cadastre-se!</div>\n\n  \n</ion-content>\n\n<ion-footer>\n  <ion-toolbar (click)="onHome()" text-center style="color:#fff;background:#0276a9;">\n    Ir para <strong style="color:#fff">Home</strong>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_7__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__providers_notificacao_notificacao__["a" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_person_person__["a" /* PersonProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 156:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 156;

/***/ }),

/***/ 16:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_modal_sucesso_modal_sucesso__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_util_events__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_local_notifications__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_config__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var UtilsProvider = /** @class */ (function () {
    function UtilsProvider(http, alertCtrl, transfer, app, platform, androidPermissions, modalCtrl, events, localNotifications) {
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.transfer = transfer;
        this.app = app;
        this.platform = platform;
        this.androidPermissions = androidPermissions;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.localNotifications = localNotifications;
        this.estados = null;
        this.initialize();
    }
    Object.defineProperty(UtilsProvider.prototype, "navCtrl", {
        get: function () {
            return this.app.getActiveNav();
        },
        enumerable: true,
        configurable: true
    });
    UtilsProvider.prototype.initialize = function () {
        var _this = this;
        this.http.get('assets/estados-cidades.json')
            .subscribe(function (result) {
            _this.estados = result.estados;
        }, function (error) {
            console.error('Error: ', error);
        });
    };
    UtilsProvider.prototype.getEstados = function () {
        var _this = this;
        if (this.estados) {
            return new Promise(function (resolve, reject) {
                resolve(_this.estados);
            });
        }
        return new Promise(function (resolve, reject) {
            var url = 'assets/estados-cidades.json';
            _this.http.get(url)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result.estados);
            }, function (error) {
                reject(error);
            });
        });
    };
    UtilsProvider.prototype.getCidades = function (uf) {
        var _this = this;
        if (this.estados) {
            return new Promise(function (resolve, reject) {
                var cidades = [];
                for (var i = 0; i < _this.estados.length; i++) {
                    if (_this.estados[i].sigla == uf) {
                        cidades = _this.estados[i].cidades;
                        break;
                    }
                }
                resolve(cidades);
            });
        }
        return new Promise(function (resolve, reject) {
            var url = 'assets/estados-cidades.json';
            _this.http.get(url)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                var cidades = [];
                for (var i = 0; i < result.estados.length; i++) {
                    if (result.estados[i].sigla == uf) {
                        cidades = result.estados[i].cidades;
                        break;
                    }
                }
                resolve(cidades);
            }, function (error) {
                reject(error);
            });
        });
    };
    UtilsProvider.prototype.getNameFromUf = function (uf) {
        var nome = uf;
        if (this.estados) {
            for (var i = 0; i < this.estados.length; i++) {
                if (this.estados[i].sigla == uf) {
                    nome = this.estados[i].nome;
                    break;
                }
            }
        }
        return nome;
    };
    UtilsProvider.prototype.getFromUf = function (uf) {
        var estado = null;
        if (this.estados) {
            for (var i = 0; i < this.estados.estados.length; i++) {
                if (this.estados.estados[i].sigla == uf) {
                    estado = this.estados.estados[i];
                    break;
                }
            }
        }
        return estado;
    };
    UtilsProvider.prototype.showAlert = function (text, message) {
        if (message === void 0) { message = undefined; }
        var alert = this.alertCtrl.create({
            title: "OK",
            buttons: [
                {
                    text: 'Fechar',
                    role: 'cancel'
                }
            ]
        });
        alert.setTitle(text);
        if (message != undefined && message != '' && message != null) {
            alert.setMessage(message);
        }
        alert.present();
    };
    UtilsProvider.prototype.showModalSucesso = function (titulo, mensagem, botao, clbk) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_modal_sucesso_modal_sucesso__["a" /* ModalSucessoPage */], { Titulo: titulo, Mensagem: mensagem, Btn: botao, error: false, callBack: clbk });
        modal.present();
    };
    UtilsProvider.prototype.showModalError = function (titulo, mensagem, botao) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_modal_sucesso_modal_sucesso__["a" /* ModalSucessoPage */], { Titulo: titulo, Mensagem: mensagem, Btn: botao, error: true });
        modal.present();
    };
    UtilsProvider.prototype.showModalLogin = function (titulo, mensagem, botao) {
        var _this = this;
        this.alertCtrl.create({
            // title: this.service.config.name,
            // subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
            // buttons: [{ text: 'Ok' }]
            title: titulo,
            subTitle: mensagem,
            buttons: [
                {
                    text: 'Faça seu Login',
                    role: 'login',
                    handler: function () {
                        // this.nav.setRoot();
                        _this.events.publish('clickLogin', true);
                    }
                },
                {
                    text: 'X',
                    // text: '<i class="fa fa-times" aria-hidden="true"></i>',
                    role: 'cancelar'
                }
            ],
            cssClass: 'alertLoginClass'
        }).present();
    };
    UtilsProvider.prototype.downloadFile = function (arquivo) {
        var _this = this;
        this.platform.ready().then(function () {
            if (!_this.platform.is('cordova')) {
                return false;
            }
            var fileTransfer = _this.transfer.create();
            var fileName = arquivo;
            var directory = '';
            if (_this.platform.is('ios')) {
                directory = cordova.file.documentsDirectory;
            }
            else if (_this.platform.is('android')) {
                directory = cordova.file.externalRootDirectory + 'Dowload/';
            }
            _this.androidPermissions.checkPermission(_this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
                .then(function (result) {
                console.log(result.hasPermission);
                if (!result.hasPermission) {
                    _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
                }
                else {
                    var downloadProgress_1 = 0;
                    fileTransfer.onProgress(function (progressEvent) {
                        var number = (progressEvent.loaded / progressEvent.total) * 100;
                        downloadProgress_1 = parseInt(parseFloat(number.toString()).toFixed(0));
                        _this.localNotifications.update({
                            id: 1,
                            text: downloadProgress_1 + '%',
                        });
                    });
                    _this.localNotifications.schedule({
                        id: 1,
                        title: 'Baixando Material..',
                        text: 0 + '%',
                        smallIcon: 'res://iconedown',
                        icon: 'res://icon',
                        // vibrate: true,
                        sound: "file://img/ok.mp3",
                    });
                    fileTransfer.download(arquivo.Url, directory + fileName).then(function (entry) {
                        cordova.plugins.notification.local.clearAll();
                        cordova.plugins.notification.local.schedule({
                            vibrate: true,
                            id: 1,
                            title: 'Download Concluído',
                            text: '100%',
                            smallIcon: 'res://iconedown',
                            icon: 'res://icon',
                            lockscreen: true,
                            attachments: [entry.nativeURL],
                        });
                        cordova.plugins.notification.local.on('click', function (notification) {
                            window.open(encodeURI(entry.nativeURL), "_blank", "location=no,enableViewportScale=yes");
                        });
                        // this.showModalSucesso('Download concluído', null, "OK", null);
                    }, function (error) {
                        _this.showModalError('Erro no download', null, "OK");
                        console.log(error);
                        // handle error
                    });
                }
            }, function (err) { return _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE); });
        });
    };
    UtilsProvider.prototype.cpf = function (cpf) {
        if (cpf == null) {
            return false;
        }
        if (cpf.length != 11) {
            return false;
        }
        if ((cpf == '00000000000') || (cpf == '11111111111') || (cpf == '22222222222') || (cpf == '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') || (cpf == '66666666666') || (cpf == '77777777777') || (cpf == '88888888888') || (cpf == '99999999999')) {
            return false;
        }
        var numero = 0;
        var caracter = '';
        var numeros = '0123456789';
        var j = 10;
        var somatorio = 0;
        var resto = 0;
        var digito1 = 0;
        var digito2 = 0;
        var cpfAux = '';
        cpfAux = cpf.substring(0, 9);
        for (var i = 0; i < 9; i++) {
            caracter = cpfAux.charAt(i);
            if (numeros.search(caracter) == -1) {
                return false;
            }
            numero = Number(caracter);
            somatorio = somatorio + (numero * j);
            j--;
        }
        resto = somatorio % 11;
        digito1 = 11 - resto;
        if (digito1 > 9) {
            digito1 = 0;
        }
        j = 11;
        somatorio = 0;
        cpfAux = cpfAux + digito1;
        for (var i = 0; i < 10; i++) {
            caracter = cpfAux.charAt(i);
            numero = Number(caracter);
            somatorio = somatorio + (numero * j);
            j--;
        }
        resto = somatorio % 11;
        digito2 = 11 - resto;
        if (digito2 > 9) {
            digito2 = 0;
        }
        cpfAux = cpfAux + digito2;
        if (cpf != cpfAux) {
            return false;
        }
        else {
            return true;
        }
    };
    UtilsProvider.prototype.getCep = function (cep) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get('https://viacep.com.br/ws/' + cep + '/json/')
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    UtilsProvider.prototype.getGeoLocation = function (lat, lng) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + __WEBPACK_IMPORTED_MODULE_8__app_config__["a" /* CONFIG_PROJECT */].ApiKeyGoogleMaps + "&sensor=true";
            _this.http.get(url)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* Nav */])
    ], UtilsProvider.prototype, "nav", void 0);
    UtilsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular_util_events__["a" /* Events */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_local_notifications__["a" /* LocalNotifications */]])
    ], UtilsProvider);
    return UtilsProvider;
}());

//# sourceMappingURL=utils.js.map

/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PersonProvider = /** @class */ (function () {
    function PersonProvider(http, auth, storage) {
        this.http = http;
        this.auth = auth;
        this.storage = storage;
    }
    PersonProvider.prototype.post = function (person) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa/cadastrobasico";
        person.ClienteId = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].clienteId;
        return new Promise(function (resolve, reject) {
            _this.http.post(url, person)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PersonProvider.prototype.get = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
                localStorage.setItem('profileName', result.Nome);
                _this.storage.set('profileName', result.Nome);
            }, function (error) {
                reject(error);
            });
        });
    };
    PersonProvider.prototype.put = function (user) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa/editar";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        user.ClienteId = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].clienteId;
        return new Promise(function (resolve, reject) {
            _this.http.post(url, user, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                console.log(result);
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PersonProvider.prototype.putFoto = function (image) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa/UploadFotoBase64";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, { Base64: image }, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PersonProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */]])
    ], PersonProvider);
    return PersonProvider;
}());

//# sourceMappingURL=person.js.map

/***/ }),

/***/ 196:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/cadastro/cadastro-tipoperfil.module": [
		371,
		8
	],
	"../pages/cadastro/cadastro.module": [
		372,
		7
	],
	"../pages/comites/comites-comites-detalhe/comites-comites-detalhe.module": [
		373,
		6
	],
	"../pages/comites/comites.module": [
		211
	],
	"../pages/convite/convite.module": [
		212
	],
	"../pages/dados-complementares/dados-complementares.module": [
		374,
		5
	],
	"../pages/detail-programa/detail-programa.module": [
		213
	],
	"../pages/detail-transmissao/detail-transmissao.module": [
		214
	],
	"../pages/evento/evento.module": [
		375,
		0
	],
	"../pages/login/login-login-esqueci/login-login-esqueci.module": [
		376,
		4
	],
	"../pages/login/login.module": [
		215
	],
	"../pages/material-institucional/material-institucional.module": [
		216
	],
	"../pages/noticias-noticia-detalhe/noticias-noticia-detalhe.module": [
		217
	],
	"../pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe.module": [
		218
	],
	"../pages/notificacoes/notificacoes.module": [
		219
	],
	"../pages/participacao/participacao-enviar-participacao/participacao-enviar-participacao.module": [
		220
	],
	"../pages/participacao/participacao-participacao-detalhe/participacao-participacao-detalhe.module": [
		377,
		3
	],
	"../pages/perfil/perfil.module": [
		222
	],
	"../pages/pesquisa/pesquisa-pesquisa-resposta/pesquisa-pesquisa-resposta.module": [
		378,
		2
	],
	"../pages/pesquisa/pesquisa.module": [
		223
	],
	"../pages/programa-governo/programa-governo.module": [
		224
	],
	"../pages/programa-pontos/programa-pontos.module": [
		225
	],
	"../pages/quero-contribuir/quero-contribuir.module": [
		226
	],
	"../pages/voluntario/voluntario-detalhe/voluntario-detalhe.module": [
		379,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 196;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalSucessoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalSucessoPage = /** @class */ (function () {
    function ModalSucessoPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.data = navParams.data;
        // this.cadastro;
        console.log(this.data);
    }
    ModalSucessoPage.prototype.ionViewDidLoad = function () {
    };
    ModalSucessoPage.prototype.close = function () {
        this.viewCtrl.dismiss();
        if (this.data.callBack) {
            this.data.callBack();
        }
    };
    ModalSucessoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'modal-sucesso',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/modal-sucesso/modal-sucesso.html"*/'<div class="container-round">\n    <div class="box">\n        <button class="btnClose" (click)="close()">\n            <ion-icon name="close"></ion-icon>\n        </button>\n        <ion-icon name="checkmark" *ngIf="!data.error" class="check"></ion-icon>\n        <ion-icon name="close-circle" *ngIf="data.error" class="check" [ngClass]="{\'error\': data.error}"></ion-icon>\n        <div class="text">{{data.Titulo}}</div>\n        <div class="text">{{data.Mensagem}}</div>\n        <button class="btnOk" (click)="close()">{{data.Btn}}</button>\n       \n    </div>\n</div>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/modal-sucesso/modal-sucesso.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], ModalSucessoPage);
    return ModalSucessoPage;
}());

//# sourceMappingURL=modal-sucesso.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QueroContribuirPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return TseModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_doacao_doacao__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_stripe__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var QueroContribuirPage = /** @class */ (function () {
    function QueroContribuirPage(navCtrl, navParams, modalCtrl, formBuilder, BrMaskerModule, doacao, loadingCtrl, stripe, utils, currencyPipe) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.BrMaskerModule = BrMaskerModule;
        this.doacao = doacao;
        this.loadingCtrl = loadingCtrl;
        this.stripe = stripe;
        this.utils = utils;
        this.currencyPipe = currencyPipe;
        this.step = 1;
        this.formaPgto = 'cartao';
        this.bandeira = '';
        this.configuracao = {
            DoacaoValor1: '',
            DoacaoValor2: '',
            DoacaoValor3: '',
            DoacaoValor4: '',
            DoacaoValorMaximo: 0,
            DoacaoValorMinimo: 0,
            Nome: '',
            Logo: '',
            TextoValorMinimo: '',
            TextoValorMaximo: ''
        };
        this.model = {
            ClienteId: 0,
            CPFCNPJ: '',
            Nome: '',
            Email: '',
            Celular: '',
            Sexo: 0,
            Valor2: '',
            CEP: '',
            Complemento: '',
            Endereco: '',
            Cidade: '',
            Estado: '',
            Bairro: '',
            EnderecoNumero: '',
            EventoId: null,
            BemServico: '',
            EspecieRecurso: 4,
            StatusSistema: 1,
            NumeroDocumento: '',
            NumeroAutorizacao: '',
            ValidadeDocumento: '',
            NomeDocumento: '',
            DataNascimentoDocumento: '',
            BandeiraDocumento: 0,
            MesDocumento: '',
            AnoDocumento: '',
            check1: false,
            check2: false
        };
        this.years = new Array();
        this.erroParte1 = 0.7;
        this.erroParte2 = 0.7;
        this.erroParte3 = 0.7;
        this.erroParte4 = 0.7;
        this.errorNome = false;
        this.errorEmail = false;
        this.errorEmail2 = false;
        this.errorCelular = false;
        this.errorCPF = false;
        this.errorCPF2 = false;
        this.errorSexo = false;
        this.errorValor = false;
        this.errorValor2 = false;
        this.errorValor3 = false;
        this.errorCheck1 = false;
        this.errorCheck2 = false;
        this.errorCEP = false;
        this.errorEndereco = false;
        this.errorCidade = false;
        this.errorEstado = false;
        this.errorBairro = false;
        this.errorNumeroDocumento = false;
        this.errorNumeroAutorizacao = false;
        this.errorNomeDocumento = false;
        this.errorDataNascimentoDocumento = false;
        this.errorDataNascimentoDocumento2 = false;
        this.errorDataNascimentoDocumento3 = false;
        this.errorMesDocumento = false;
        this.errorAnoDocumento = false;
        this.errorDataVencimento = false;
        this.valor1 = false;
        this.valor2 = false;
        this.valor3 = false;
        this.valor4 = false;
        this.loadAll();
    }
    QueroContribuirPage_1 = QueroContribuirPage;
    QueroContribuirPage.prototype.ngOnInit = function () {
        //formularios
        this.contribuirForm = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormGroup"]({
            nome: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"](this.model.Nome, {
                validators: [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]
            }),
            email: this.formBuilder.control(this.model.Email, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].email]),
            cpf: this.formBuilder.control(this.model.CPFCNPJ, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            celular: this.formBuilder.control(this.model.Celular, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            sexo: this.formBuilder.control(this.model.Sexo, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required])
        });
        this.contribuirStep2Form = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormGroup"]({
            valor: this.formBuilder.control(this.model.Valor2, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            check1: this.formBuilder.control(false, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            check2: this.formBuilder.control(false, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
        });
        this.contribuirStep3Form = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormGroup"]({
            cep: this.formBuilder.control(this.model.CEP, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            endereco: this.formBuilder.control(this.model.Endereco, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            cidade: this.formBuilder.control(this.model.Cidade, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            estado: this.formBuilder.control(this.model.Estado, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            bairro: this.formBuilder.control(this.model.Bairro, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            complemento: this.formBuilder.control(this.model.Complemento),
            numero: this.formBuilder.control(this.model.EnderecoNumero),
        });
        this.contribuirStep4Form = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormGroup"]({
            recurso: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            documento: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            cvv: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            mes: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            ano: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            nomeimpresso: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            data: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
        });
    };
    QueroContribuirPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    QueroContribuirPage.prototype.ionViewDidLoad = function () {
        var step = parseInt(this.navParams.data.step);
        if (step > 1) {
            this.step = step;
            if (step == 5) {
                var model = this.navParams.data.model;
                if (model.Sucesso == 1) {
                    this.utils.showModalSucesso(model.Titulo, model.Mensagem, "OK", null);
                }
            }
        }
        else {
            this.getDoacaoFromStorage();
            this.step = 1;
        }
    };
    QueroContribuirPage.prototype.goBack = function () {
        var _this = this;
        this.navCtrl.pop().catch(function () {
            _this.navCtrl.setRoot('HomePage');
        });
    };
    QueroContribuirPage.prototype.goToStep = function (step, model, back) {
        var error = false;
        if (step == 2) {
            error = this.validaCampos1(model);
        }
        else if (step == 3) {
            error = this.validaCampos2(model, back);
        }
        else if (step == 4) {
            error = this.validaCampos3(model, false);
        }
        if (!error) {
            //seta a parte em que a pessoa parou a doação
            if (this.model.StatusSistema < step) {
                this.model.StatusSistema = step;
                this.putDoacao();
            }
            //atualizando so dados atuais da doação
            if (model != undefined) {
                localStorage.setItem('doacao', JSON.stringify(model));
            }
            this.navCtrl.setRoot(QueroContribuirPage_1, { step: step, model: step == 5 ? model : null });
        }
    };
    QueroContribuirPage.prototype.loadAll = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.carregarConfig().then(function () {
            _this.getDoacaoFromStorage();
            _this.loadYears();
            loading.dismiss();
        });
    };
    //carrega configuração de valores do cliente
    QueroContribuirPage.prototype.carregarConfig = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.doacao.get().then(function (result) {
                _this.configuracao = result;
                _this.configuracao.TextoValorMinimo = "Valor mínimo é de R$ " + _this.configuracao.DoacaoValorMinimo.toString().replace(".", ",");
                _this.configuracao.TextoValorMaximo = "Valor mínimo é de R$ " + _this.configuracao.DoacaoValorMaximo.toString().replace(".", ",");
                console.log(result);
                resolve(true);
            });
        });
    };
    //enviar a doação
    QueroContribuirPage.prototype.enviarDoacao = function (model) {
        var _this = this;
        //this.erroParte4 = true;
        if (!this.validaCampos4(model)) {
            var loading_1 = this.loadingCtrl.create({
                content: 'Aguarde enquanto essa operação é processada…'
            });
            loading_1.present();
            if (this.model.EspecieRecurso == 4) {
                this.stripe.getCardType(model.NumeroDocumento)
                    .then(function (response) {
                    console.log(response);
                    if (response == "Visa") {
                        _this.model.BandeiraDocumento = 1;
                    }
                    else if (response == "MasterCard") {
                        _this.model.BandeiraDocumento = 2;
                    }
                    if (response == "Visa" || response == "MasterCard") {
                        // this.model.BandeiraDocumento = 1;
                        _this.postDoacao(_this.model).then(function (result) {
                            // this.erroParte4 = false;
                            loading_1.dismiss();
                            if (result.Sucesso == 1) {
                                localStorage.removeItem("doacao");
                                _this.goToStep(5, null, false);
                            }
                            else {
                                _this.utils.showModalError('Problema encontrado', result.Mensagem, 'Voltar');
                            }
                        });
                    }
                    else {
                        // this.erroParte4 = false;
                        loading_1.dismiss();
                        _this.utils.showModalError('Problema encontrado', "Cartão não permitido ou inválido, verifique seus dados.", 'Voltar');
                    }
                });
            }
            else {
                this.postDoacao(this.model).then(function (result) {
                    // this.erroParte4 = false;
                    loading_1.dismiss();
                    if (result.Sucesso == 1) {
                        localStorage.removeItem("doacao");
                        _this.goToStep(5, result, false);
                    }
                    else {
                        // this.erroParte4 = false;
                        _this.utils.showModalError('Problema encontrado', result.Mensagem, 'Voltar');
                    }
                });
            }
        }
    };
    QueroContribuirPage.prototype.postDoacao = function (model) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.model.ValidadeDocumento = _this.model.MesDocumento + '/' + _this.model.AnoDocumento;
            _this.model.ClienteId = __WEBPACK_IMPORTED_MODULE_6__providers_app_config__["a" /* CONFIG_PROJECT */].clienteId;
            _this.model.Valor2 = _this.model.Valor2.replace('R$ ', '');
            _this.doacao.post(model).then(function (result) {
                resolve(result);
            }, function (erro) {
                console.log(erro);
                resolve({ Mensagem: erro.error.Message });
            });
        });
    };
    //marcar cada passo feito na doação
    QueroContribuirPage.prototype.putDoacao = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.doacao.put(_this.model).then(function (result) {
                resolve(result);
            }, function (erro) {
                console.log(erro);
                resolve({ Mensagem: erro.error.Message });
            });
        });
    };
    QueroContribuirPage.prototype.verificaMensagemSucesso = function () {
    };
    QueroContribuirPage.prototype.setEspecieRecurso = function (recurso) {
        this.model.EspecieRecurso = recurso;
    };
    //lista de anos do cartão de crédito
    QueroContribuirPage.prototype.loadYears = function () {
        var y = (new Date()).getFullYear();
        for (var i = 0; i <= 12; i++) {
            this.years.push(y + i);
        }
    };
    //validação (keyup) dos campos do primeiro formulário
    QueroContribuirPage.prototype.validaPart1 = function () {
        if (this.model.Nome != '' && this.model.Email != '' && this.model.Celular != '' && this.model.CPFCNPJ != '' && this.model.Sexo > 0) {
            this.erroParte1 = 1;
            return true;
        }
        else {
            return false;
        }
    };
    //validação (keyup) dos campos do segundo formulário
    QueroContribuirPage.prototype.validaPart2 = function () {
        if (this.model.Valor2 != '' &&
            this.model.check1 &&
            this.model.check2) {
            this.erroParte2 = 1;
            return true;
        }
        else {
            return false;
        }
    };
    //validação (keyup) dos campos do terceiro formulário
    QueroContribuirPage.prototype.validaPart3 = function () {
        if (this.model.CEP != '' &&
            this.model.Endereco != '' &&
            this.model.Cidade != '' &&
            this.model.Estado != '' &&
            this.model.Bairro != '') {
            this.erroParte3 = 1;
            return true;
        }
        else {
            return false;
        }
    };
    QueroContribuirPage.prototype.validaPart4 = function () {
        //cartão
        if (this.model.EspecieRecurso == 4) {
            if (this.model.NumeroDocumento != '' &&
                this.model.NumeroAutorizacao != '' &&
                this.model.NomeDocumento != '' &&
                (this.model.DataNascimentoDocumento != '' && this.ValidaData(this.model.DataNascimentoDocumento)) &&
                (this.model.MesDocumento != '' && this.model.AnoDocumento != '' && this.validarValidadeCartao())) {
                this.erroParte4 = 1;
                return true;
            }
            else {
                return false;
            }
        }
        else if (this.model.EspecieRecurso == 6) {
        }
    };
    //checa a data de vencimento do cartão
    QueroContribuirPage.prototype.validarValidadeCartao = function () {
        var a = this.model.AnoDocumento + "-" + this.model.MesDocumento + "-01";
        var datacartao = new Date(a);
        var datetimenow = new Date();
        if (datetimenow > datacartao) {
            return false;
        }
        else {
            return true;
        }
    };
    //checa se a data de nascimento é válida
    QueroContribuirPage.prototype.ValidaData = function (text) {
        var comp = text.split('/');
        var d = parseInt(comp[0], 10);
        var m = parseInt(comp[1], 10);
        var y = parseInt(comp[2], 10);
        var date = new Date(y, m - 1, d);
        if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {
            return true;
        }
        else {
            return false;
        }
    };
    //validação ao clicar dos campos do primeiro formulário
    QueroContribuirPage.prototype.validaCampos1 = function (model) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var erro = false;
        if (model.Nome == '') {
            this.errorNome = true;
            erro = true;
        }
        else {
            this.errorNome = false;
        }
        if (model.Celular == '') {
            this.errorCelular = true;
            erro = true;
        }
        else {
            this.errorCelular = false;
        }
        if (model.Sexo == 0) {
            this.errorSexo = true;
            erro = true;
        }
        else {
            this.errorSexo = false;
        }
        if (model.Email == '') {
            this.errorEmail = true;
            erro = true;
        }
        else {
            this.errorEmail = false;
        }
        if (model.Email != '' && !re.test(model.Email)) {
            this.errorEmail2 = true;
            erro = true;
        }
        else {
            this.errorEmail2 = false;
        }
        if (model.CPFCNPJ == '') {
            this.errorCPF = true;
            erro = true;
        }
        else {
            this.errorCPF = false;
        }
        var validacpf = this.utils.cpf(model.CPFCNPJ.replace(".", "").replace(".", "").replace("-", ""));
        if (model.CPFCNPJ != '' && validacpf == false) {
            this.errorCPF2 = true;
            erro = true;
        }
        else {
            this.errorCPF2 = false;
        }
        if (!erro) {
            this.erroParte1 = 1;
        }
        return erro;
    };
    //validação ao clicar dos campos do segundo formulário
    QueroContribuirPage.prototype.validaCampos2 = function (model, back) {
        var valor = model.Valor2.replace('R$ ', '');
        var erro = false;
        if (valor == '') {
            this.errorValor = true;
            erro = true;
        }
        else {
            this.errorValor = false;
        }
        if (parseFloat(this.model.Valor2.replace("R$ ", "")) < this.configuracao.DoacaoValorMinimo) {
            this.errorValor2 = true;
            erro = true;
        }
        else {
            this.errorValor2 = false;
        }
        if (parseFloat(this.model.Valor2.replace("R$ ", "")) > this.configuracao.DoacaoValorMaximo) {
            this.errorValor3 = true;
            erro = true;
        }
        else {
            this.errorValor3 = false;
        }
        if (!back) {
            if (!model.check1) {
                this.errorCheck1 = true;
                erro = true;
            }
            else {
                this.errorCheck1 = false;
            }
            if (!model.check2) {
                this.errorCheck2 = true;
                erro = true;
            }
            else {
                this.errorCheck2 = false;
            }
        }
        if (!erro) {
            this.erroParte2 = 1;
        }
        return erro;
    };
    //validação ao clicar dos campos do terceiro formulário
    QueroContribuirPage.prototype.validaCampos3 = function (model, fromstorage) {
        var erro = false;
        if (this.model.CEP == '') {
            if (!fromstorage) {
                this.errorCEP = true;
            }
            erro = true;
        }
        else {
            this.errorCEP = false;
        }
        if (this.model.Endereco == '') {
            if (!fromstorage) {
                this.errorEndereco = true;
            }
            erro = true;
        }
        else {
            this.errorEndereco = false;
        }
        if (this.model.Cidade == '') {
            if (!fromstorage) {
                this.errorCidade = true;
            }
            erro = true;
        }
        else {
            this.errorCidade = false;
        }
        if (this.model.Estado == '') {
            if (!fromstorage) {
                this.errorEstado = true;
            }
            erro = true;
        }
        else {
            this.errorEstado = false;
        }
        if (this.model.Bairro == '') {
            if (!fromstorage) {
                this.errorBairro = true;
            }
            erro = true;
        }
        else {
            this.errorBairro = false;
        }
        if (!erro) {
            this.erroParte3 = 1;
        }
        return erro;
    };
    //validação ao clicar dos campos do quarto formulário
    QueroContribuirPage.prototype.validaCampos4 = function (model) {
        var erro = false;
        if (this.model.NumeroDocumento == '') {
            this.errorNumeroDocumento = true;
            erro = true;
        }
        else {
            this.errorNumeroDocumento = false;
        }
        if (this.model.NumeroAutorizacao == '') {
            this.errorNumeroAutorizacao = true;
            erro = true;
        }
        else {
            this.errorNumeroAutorizacao = false;
        }
        if (this.model.NomeDocumento == '') {
            this.errorNomeDocumento = true;
            erro = true;
        }
        else {
            this.errorNomeDocumento = false;
        }
        if (this.model.DataNascimentoDocumento == '') {
            this.errorDataNascimentoDocumento = true;
            erro = true;
        }
        else {
            this.errorDataNascimentoDocumento = false;
        }
        if (this.model.DataNascimentoDocumento != '' &&
            !this.ValidaData(this.model.DataNascimentoDocumento)) {
            this.errorDataNascimentoDocumento2 = true;
            erro = true;
        }
        else {
            this.errorDataNascimentoDocumento2 = false;
        }
        if (this.model.DataNascimentoDocumento != '' &&
            this.ValidaData(this.model.DataNascimentoDocumento) &&
            this.calcIdade(this.model.DataNascimentoDocumento) < 18) {
            this.errorDataNascimentoDocumento3 = true;
            erro = true;
        }
        else {
            this.errorDataNascimentoDocumento3 = false;
        }
        if (this.model.MesDocumento == '') {
            this.errorMesDocumento = true;
            erro = true;
        }
        else {
            this.errorMesDocumento = false;
        }
        if (this.model.AnoDocumento == '') {
            this.errorAnoDocumento = true;
            erro = true;
        }
        else {
            this.errorAnoDocumento = false;
        }
        if (!this.validarValidadeCartao()) {
            this.errorDataVencimento = true;
            erro = true;
        }
        else {
            this.errorDataVencimento = false;
        }
        if (!erro) {
            this.erroParte4 = 1;
        }
        return erro;
    };
    QueroContribuirPage.prototype.calcIdade = function (data) {
        var d = new Date, ano_atual = d.getFullYear(), mes_atual = d.getMonth() + 1, dia_atual = d.getDate(), split = data.split('/'), novadata = split[1] + "/" + split[0] + "/" + split[2], data_americana = new Date(novadata), vAno = data_americana.getFullYear(), vMes = data_americana.getMonth() + 1, vDia = data_americana.getDate(), ano_aniversario = +vAno, mes_aniversario = +vMes, dia_aniversario = +vDia, quantos_anos = ano_atual - ano_aniversario;
        if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
            quantos_anos--;
        }
        return quantos_anos < 0 ? 0 : quantos_anos;
    };
    //pegando dados atuais da doação
    QueroContribuirPage.prototype.getDoacaoFromStorage = function () {
        if (localStorage.getItem('doacao') != null) {
            this.model = JSON.parse(localStorage.getItem('doacao'));
            console.log(this.model);
            if (this.model.Valor2.indexOf('R$') == -1) {
                this.model.Valor2 = 'R$ ' + this.model.Valor2;
            }
            //chegando validação dos campos
            this.validaCampos1(this.model);
            this.model.check1 = false;
            this.model.check2 = false;
            //this.validaCampos2(this.model);
            this.validaCampos3(this.model, true);
            this.model.NumeroDocumento = '';
            this.model.NomeDocumento = '';
            this.model.DataNascimentoDocumento = '';
            this.model.MesDocumento = '';
            this.model.AnoDocumento = '';
            this.model.NumeroAutorizacao = '';
        }
    };
    QueroContribuirPage.prototype.selecionarValor = function (valor, v) {
        this.valor1 = (v == 1);
        this.valor2 = (v == 2);
        this.valor3 = (v == 3);
        this.valor4 = (v == 4);
        this.model.Valor2 = valor;
    };
    //converção campo valor - segundo formulario
    QueroContribuirPage.prototype.convert = function (amount) {
        var money = amount.target.value.replace(",", ".");
        this.model.Valor2 = this.currencyPipe.transform(money, 'R$', true, '1.2-2');
        this.validaPart2();
    };
    //buscar cep api
    QueroContribuirPage.prototype.getCep = function (cep) {
        var _this = this;
        cep = cep.replace('.', '').replace('-', '');
        if (cep.length == 8) {
            var loading_2 = this.loadingCtrl.create({
                content: 'Carregando...'
            });
            loading_2.present();
            this.utils.getCep(cep).then(function (result) {
                loading_2.dismiss();
                _this.model.Endereco = result.logradouro;
                _this.model.Cidade = result.localidade;
                _this.model.Estado = result.uf;
                _this.model.Bairro = result.bairro;
                _this.model.Complemento = result.complemento;
                _this.validaPart3();
            });
        }
    };
    QueroContribuirPage.prototype.openTse = function () {
        var tseModal = this.modalCtrl.create(TseModalPage, {
            showBackdrop: true,
            cssClass: 'modal-box'
        });
        tseModal.present();
    };
    QueroContribuirPage = QueroContribuirPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-quero-contribuir',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/quero-contribuir/quero-contribuir.html"*/'<!-- @TODO aplicar html e css para iOS -->\n<ion-header color="header">\n  <style>\n   .qc-group-btn button {\n\n    font-weight: 700;\n}\n    </style>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Quero contribuir</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n        <ion-icon name="search"></ion-icon>\n      </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content *ngIf="step === 1">\n\n  <header class="qc-header">\n    <div class="photo"></div>\n    <div class="mask"></div>\n    <h2>Jair Messias Bolsonaro</h2>\n    <h5>Faça sua doação</h5>\n  </header>\n\n  <form class="qc-form" [formGroup]="contribuirForm" novalidate>\n    <h3>Identifique-se</h3>\n\n    <ion-list>\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Nome" name="nome" formControlName="nome" [(ngModel)]= "model.Nome" (keyup)="validaPart1()"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorNome" >O campo Nome é obrigatório</ion-label>\n      <ion-item>\n        <ion-input type="email" placeholder="E-mail" name="email" formControlName="email" [(ngModel)]= "model.Email" (keyup)="validaPart1()"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorEmail" >O campo  E-mail é obrigatório</ion-label>\n      <ion-label class="error" *ngIf="errorEmail2" >O campo E-mail é inválido</ion-label>\n\n      <ion-item no-lines>\n        <ion-input type="tel" placeholder="CPF" [brmasker]="{mask:\'000.000.000-00\', len:14}" name="cpf" formControlName="cpf" [(ngModel)]= "model.CPFCNPJ" (keyup)="validaPart1()"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorCPF" >O campo CPF é obrigatório</ion-label>\n      <ion-label class="error" *ngIf="errorCPF2" >O campo CPF é inválido</ion-label>\n\n      <ion-item no-lines>\n        <ion-input type="tel" placeholder="Celular"   [brmasker]="{phone: true}" name="celular" formControlName="celular" [(ngModel)]= "model.Celular" (keyup)="validaPart1()"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorCelular" >O campo Celular é obrigatório</ion-label>\n\n      <ion-item no-lines>\n        <ion-select name="sexo" placeholder="Sexo" cancelText="Cancelar" formControlName="sexo" [selectOptions]="{title: \'Sexo\'}" [(ngModel)]= "model.Sexo" (ionChange)="validaPart1()" style="padding-left:20px;">\n          <ion-option value="2">Feminino</ion-option>\n          <ion-option value="1">Masculino</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorSexo" >O campo Sexo é obrigatório</ion-label>\n\n      <ion-item no-lines no-margin no-padding class="qc-buttons" style="width: 97%;\n      margin: 0 auto;">\n        <ion-row>\n          <ion-col col-5>\n            <button class="qc-btn-cancel" ion-button block (click)="goBack()">Voltar</button>\n          </ion-col>\n          <ion-col col-7>\n            <button class="qc-btn-submit" ion-button block color="header" (click)="goToStep(2,model)" [style.opacity]="erroParte1">Continuar</button>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <ion-footer class="qc-footer">\n          <ion-toolbar>\n            <ion-title *ngIf="step <= 4">Passo <strong>{{step}} de 4</strong></ion-title>\n            <ion-title *ngIf="step == 5">Doação <strong>Concluída</strong></ion-title>\n          </ion-toolbar>\n        </ion-footer>\n    </ion-list>\n  </form>\n \n</ion-content><!-- / Passo 1 -->\n\n<ion-content *ngIf="step === 2">\n  <header class="qc-header">\n    <div class="photo"></div>\n    <div class="mask"></div>\n    <h2>Jair Messias Bolsonaro</h2>\n    <h5>Faça sua doação</h5>\n  </header>\n\n  <form class="qc-form" [formGroup]="contribuirStep2Form" novalidate>\n    <h3>Selecione o Valor</h3>\n\n    <ion-list>\n\n      <ion-item no-lines class="qc-group-btn">\n        <ion-row>\n          <ion-col col-6>\n            <button ion-button full (click)= "selecionarValor(configuracao.DoacaoValor1,1)" [ngClass]="{\'check\': valor1}">{{configuracao.DoacaoValor1}}</button>\n          </ion-col>\n          <ion-col col-6>\n            <button ion-button full (click)= "selecionarValor(configuracao.DoacaoValor2,2)" [ngClass]="{\'check\': valor2}">{{configuracao.DoacaoValor2}}</button>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <button ion-button full (click)= "selecionarValor(configuracao.DoacaoValor3,3)" [ngClass]="{\'check\': valor3}">{{configuracao.DoacaoValor3}}</button>\n          </ion-col>\n          <ion-col col-6>\n            <button ion-button full  (click)= "selecionarValor(configuracao.DoacaoValor4,4)" [ngClass]="{\'check\': valor4}">{{configuracao.DoacaoValor4}}</button>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <input type="hidden" name="quantia">\n\n      <ion-item no-lines>\n        <ion-label class="qc-label">Outro Valor</ion-label>\n        <ion-input type="tel" placeholder="R$" name="valor" [brmasker]="{ money:true }" formControlName="valor" [(ngModel)]= "model.Valor2" (keyup)="convert($event)"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorValor">O campo Valor é obrigatório</ion-label>\n      <ion-label class="error" *ngIf="errorValor2">{{configuracao.TextoValorMinimo}}</ion-label>\n      <ion-label class="error" *ngIf="errorValor3">{{configuracao.TextoValorMaximo}}</ion-label>\n\n      <ion-item text-wrap no-lines class="qc-info">\n          <ion-icon name="alert" item-start style="    margin: 0!important;\n          color: #333333;    font-size: 1.5em!important;"></ion-icon>\n        <div item-end ><p style="font-size: 1.4rem;\n    color: #333333;\n    font-weight: bold;font-size: 1.5rem!important;">Para valores acima de R$ 1.064,00 a doação deverá ser realizada via TED bancário.</p></div>\n      </ion-item>\n\n      <ion-item text-wrap no-lines>\n        <ion-checkbox align-self-start formControlName="check1" [(ngModel)]= "model.check1" (ionChange)="validaPart2()" [ngClass]="{\'error-check\': errorCheck1}"></ion-checkbox>\n        <ion-label><p class="p33">Declaro ter ciência de que a legislação eleitoral limita as doações a 10% (dez por cento) dos\n            rendimentos brutos auferidos pelo doador no ano anterior à eleição.</p></ion-label>\n      </ion-item>\n\n      <ion-item text-wrap no-lines>\n        <ion-checkbox align-self-start formControlName="check2" [(ngModel)]= "model.check2" (ionChange)="validaPart2()" [ngClass]="{\'error-check\': errorCheck2}"></ion-checkbox>\n        <ion-label><p class="p33">Declaro, ainda, que os recursos as serem doados, além de possuírem origem lícita, não são\n            provenientes de fonte vedada, nos <strong (click)="openTse()">termos do art. 33 da Resolução n<sup>o</sup> 23.553/17 do TSE.</strong></p></ion-label>\n      </ion-item>\n\n      <ion-item no-lines no-margin no-padding class="qc-buttons" style="width: 97%;\n      margin: 0 auto;">\n      <ion-row>\n          <ion-col col-5>\n            <button class="qc-btn-cancel" ion-button block (click)="goToStep(1,model)">Voltar</button>\n          </ion-col>\n          <ion-col col-7>\n            <button class="qc-btn-submit" ion-button block color="header" (click)="goToStep(3,model)" [style.opacity]="erroParte2">Continuar</button>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <ion-footer class="qc-footer">\n          <ion-toolbar>\n            <ion-title *ngIf="step <= 4">Passo <strong>{{step}} de 4</strong></ion-title>\n            <ion-title *ngIf="step == 5">Doação <strong>Concluída</strong></ion-title>\n          </ion-toolbar>\n        </ion-footer>\n    </ion-list>\n  </form>\n</ion-content><!-- / Passo 2 -->\n\n<ion-content *ngIf="step === 3">\n  <header class="qc-header">\n    <div class="photo"></div>\n    <div class="mask"></div>\n    <h2>Jair Messias Bolsonaro</h2>\n    <h5>Faça sua doação</h5>\n  </header>\n  <form class="qc-form" [formGroup]="contribuirStep3Form" novalidate>\n    <h3>Digite seu endereço</h3>\n\n    <ion-list>\n\n      <ion-item no-lines>\n        <ion-input type="tel" [brmasker]="{mask:\'00000-000\', len:10}" placeholder="CEP" name="cep" formControlName="cep"[(ngModel)]= "model.CEP" (keyup)="getCep(model.CEP)"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorCEP">O campo CEP é obrigatório</ion-label>\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Estado" name="estado" formControlName="estado" [(ngModel)]= "model.Estado" (keyup)="validaPart3()"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorEstado">O campo Estado é obrigatório</ion-label>\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Cidade" name="cidade" formControlName="cidade" [(ngModel)]= "model.Cidade" (keyup)="validaPart3()"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorCidade">O campo Cidade é obrigatório</ion-label>\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Bairro" name="bairro" formControlName="bairro" [(ngModel)]= "model.Bairro" (keyup)="validaPart3()"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorBairro">O campo Bairro é obrigatório</ion-label>\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Endereço" name="endereco" formControlName="endereco" [(ngModel)]= "model.Endereco" (keyup)="validaPart3()"></ion-input>\n      </ion-item>\n      <ion-label class="error" *ngIf="errorEndereco">O campo Endereco é obrigatório</ion-label>\n      <ion-item no-lines class="qc-two">\n        <ion-input type="tel" placeholder="Número" name="numero" size="4" formControlName="numero" [(ngModel)]= "model.EnderecoNumero"></ion-input>\n        <ion-input type="text" placeholder="Complemento" name="complemento" formControlName="complemento"[(ngModel)]= "model.Complemento"></ion-input>\n      </ion-item>\n\n      <ion-item no-lines no-margin no-padding class="qc-buttons" style="width: 97%;\n      margin: 0 auto;">\n      <ion-row>\n          <ion-col col-5>\n            <button class="qc-btn-cancel" ion-button block (click)="goToStep(2,model)">Voltar</button>\n          </ion-col>\n          <ion-col col-7>\n            <button class="qc-btn-submit" ion-button block color="header" (click)="goToStep(4,model)" [style.opacity]="erroParte3">Continuar</button>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <ion-footer class="qc-footer">\n          <ion-toolbar>\n            <ion-title *ngIf="step <= 4">Passo <strong>{{step}} de 4</strong></ion-title>\n            <ion-title *ngIf="step == 5">Doação <strong>Concluída</strong></ion-title>\n          </ion-toolbar>\n        </ion-footer>\n    </ion-list>\n  </form>\n</ion-content><!-- / Passo 3 -->\n\n<ion-content *ngIf="step === 4">\n  <header class="qc-header">\n    <div class="photo"></div>\n    <div class="mask"></div>\n    <h2>Jair Messias Bolsonaro</h2>\n    <h5>Faça sua doação</h5>\n  </header>\n\n  <form class="qc-form" [formGroup]="contribuirStep4Form" novalidate>\n    <h3>Forma de Pagamento</h3>\n\n    <!-- <ion-segment [(ngModel)]="formaPgto" [ngModelOptions]="{standalone: true}">\n      <ion-segment-button value="cartao" text-wrap align-items-center (click)="setEspecieRecurso(4)">\n        <ion-icon name="card"></ion-icon>\n        <p style="    line-height: 1;\n        margin-top: 7px;\n        font-size: 1.8rem;\n        width: 55%;">Cartão de Crédito</p>\n      </ion-segment-button>\n      <ion-segment-button value="boleto" text-wrap align-items-center (click)="setEspecieRecurso(6)">\n        <ion-icon name="barcode"></ion-icon>\n        <p>Boleto</p>\n      </ion-segment-button>\n    </ion-segment> -->\n\n    <div [ngSwitch]="formaPgto">\n      <div *ngSwitchCase="\'cartao\'">\n\n        <ion-list padding-top>\n\n          <ion-item no-lines>\n            <ion-label color="dark" stacked>Número do cartão</ion-label>\n            <ion-input type="tel" placeholder="" [brmasker]="{mask:\'0000 0000 0000 0000\', len:19}" name="cartao_numero" formControlName="documento" [(ngModel)]= "model.NumeroDocumento" (keyup)="validaPart4()"></ion-input>\n          </ion-item>\n          <ion-label class="error" *ngIf="errorNumeroDocumento">O campo Número do cartão é obrigatório</ion-label>\n\n          <ion-item no-lines>\n            <ion-label color="dark" stacked>Nome Impresso no Cartão</ion-label>\n            <ion-input type="text" placeholder="" name="cartao_nome" formControlName="nomeimpresso" [(ngModel)]= "model.NomeDocumento" (keyup)="validaPart4()"></ion-input>\n          </ion-item>\n          <ion-label class="error" *ngIf="NomeDocumento">O campo Nome Impresso no Cartão é obrigatório</ion-label>\n          <ion-item no-lines>\n            <ion-label color="dark" stacked>Data de Nascimento</ion-label>\n            <ion-input type="tel" [brmasker]="{mask:\'00/00/0000\', len:10}" placeholder="dd/mm/aaaa" name="cartao_dt_nasc" formControlName="data" [(ngModel)]= "model.DataNascimentoDocumento" (keyup)="validaPart4()"></ion-input>\n          </ion-item>\n          <ion-label class="error" *ngIf="errorDataNascimentoDocumento">O campo Data de Nascimento é obrigatório</ion-label>\n          <ion-label class="error" *ngIf="errorDataNascimentoDocumento2">O campo Data de Nascimento é invalido</ion-label>\n          <ion-label class="error" *ngIf="errorDataNascimentoDocumento3">Titular precisa ser maior de 18 anos</ion-label>\n          <ion-item no-lines class="no-margin-bottom">\n            <ion-label color="dark" stacked>Validade</ion-label>\n          </ion-item>\n          <ion-label class="error" *ngIf="errorCidade">O campo Validade é obrigatório</ion-label>\n          <ion-item no-lines class="qc-two">\n\n            <ion-select name="cartao_val_mes" placeholder="Mês" cancelText="Cancelar" [selectOptions]="{title: \'Mês\'}" formControlName="mes" [(ngModel)]= "model.MesDocumento" (ionChange)="validaPart4()">\n              <ion-option value="01">Janeiro</ion-option>\n              <ion-option value="02">Fevereiro</ion-option>\n              <ion-option value="03">Março</ion-option>\n              <ion-option value="04">Abril</ion-option>\n              <ion-option value="05">Maio</ion-option>\n              <ion-option value="06">Junho</ion-option>\n              <ion-option value="07">Julho</ion-option>\n              <ion-option value="08">Agosto</ion-option>\n              <ion-option value="09">Setembro</ion-option>\n              <ion-option value="10">Outubro</ion-option>\n              <ion-option value="11">Novembro</ion-option>\n              <ion-option value="12">Dezembro</ion-option>\n            </ion-select>\n\n            <ion-select name="cartao_val_ano" placeholder="Ano" cancelText="Cancelar" [selectOptions]="{title: \'Ano\'}" formControlName="ano" [(ngModel)]= "model.AnoDocumento"(ionChange)="validaPart4()">\n              <ion-option *ngFor="let ano of years" [value]="ano">{{ano}}</ion-option>\n            </ion-select>\n          </ion-item>\n          <ion-label class="error" *ngIf="errorMesDocumento">O campo Mês é obrigatório</ion-label>\n          <ion-label class="error" *ngIf="errorAnoDocumento">O campo Ano é obrigatório</ion-label>\n          <ion-label class="error" *ngIf="errorDataVencimento">Cartão vencido</ion-label>\n          <ion-item no-lines>\n            <ion-label color="dark" stacked>Código de segurança</ion-label>\n            <ion-input type="tel" placeholder="CVV" name="cartao_cvv" maxlength="4" formControlName="cvv" [(ngModel)]= "model.NumeroAutorizacao" (keyup)="validaPart4()"></ion-input>\n          </ion-item>\n          <ion-label class="error" *ngIf="errorNumeroAutorizacao">O campo CVV é obrigatório</ion-label>\n          <ion-item no-lines padding text-center>\n            <img src="assets/imgs/visa.png" alt="visa">\n            <img src="assets/imgs/mastercard.png" alt="mastercard">\n          </ion-item>\n\n          <ion-item text-wrap no-lines class="qc-info">\n            <ion-icon name="lock" md="md-lock" item-start></ion-icon>\n            <div item-end>esse ambiente é seguro e os dados do seu cartão não serão armazenados</div>\n          </ion-item>\n\n          <ion-item no-lines no-margin no-padding class="qc-buttons" style="width: 97%;\n          margin: 0 auto;">\n            <ion-row>\n              <ion-col col-5>\n                <button class="qc-btn-cancel" ion-button block (click)="goToStep(3,model, true)">Voltar</button>\n              </ion-col>\n              <ion-col col-7>\n                <button class="qc-btn-submit" ion-button block color="header" (click)="enviarDoacao(model)" [style.opacity]="erroParte4">Continuar</button>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n          <ion-footer class="qc-footer">\n              <ion-toolbar>\n                <ion-title *ngIf="step <= 4">Passo <strong>{{step}} de 4</strong></ion-title>\n                <ion-title *ngIf="step == 5">Doação <strong>Concluída</strong></ion-title>\n              </ion-toolbar>\n            </ion-footer>\n        </ion-list>\n\n      </div>\n\n      <div *ngSwitchCase="\'boleto\'">\n\n        <ion-list>\n          <ion-item>\n            <p text-center>O boleto será enviado para o seu e-mail.</p>\n          </ion-item>\n\n          <ion-item no-lines no-margin no-padding class="qc-buttons">\n            <ion-row>\n              <ion-col col-4>\n                <button class="qc-btn-cancel" ion-button block (click)="goToStep(1)">Voltar</button>\n              </ion-col>\n              <ion-col col-8>\n                <button class="qc-btn-submit" ion-button block color="header" (click)="enviarDoacao(model)">Continuar</button>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n          <ion-footer class="qc-footer">\n              <ion-toolbar>\n                <ion-title *ngIf="step <= 4">Passo <strong>{{step}} de 4</strong></ion-title>\n                <ion-title *ngIf="step == 5">Doação <strong>Concluída</strong></ion-title>\n              </ion-toolbar>\n            </ion-footer>\n        </ion-list>\n      </div>\n    </div>\n  </form>\n</ion-content><!-- / Passo 4 -->\n\n<ion-content *ngIf="step === 5">\n  <ion-grid>\n    <ion-row align-items-center>\n      <ion-col class="qc-confirmation">\n        <h2>Confirmação</h2>\n\n        <div text-center>\n          <ion-icon name="ios-checkmark-circle-outline"></ion-icon>\n        </div>\n\n        <p>Sua doação foi enviada com sucesso!</p>\n\n        <ion-list class="qc-form">\n          <ion-item class="qc-buttons">\n            <button class="qc-btn-submit" ion-button block color="header" (click)="goToStep(1)">Doar novamente</button>\n          </ion-item>\n          \n        </ion-list>\n       \n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-footer class="qc-footer">\n    <ion-toolbar>\n      <ion-title *ngIf="step <= 4">Passo <strong>{{step}} de 4</strong></ion-title>\n      <ion-title *ngIf="step == 5">Doação <strong>Concluída</strong></ion-title>\n    </ion-toolbar>\n  </ion-footer>\n</ion-content>\n\n<!-- <ion-footer class="qc-footer">\n  <ion-toolbar>\n    <ion-title *ngIf="step <= 4">Passo <strong>{{step}} de 4</strong></ion-title>\n    <ion-title *ngIf="step == 5">Doação <strong>Concluída</strong></ion-title>\n  </ion-toolbar>\n</ion-footer> -->\n\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/quero-contribuir/quero-contribuir.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_7__ionic_native_stripe__["a" /* Stripe */], __WEBPACK_IMPORTED_MODULE_9__angular_common__["c" /* CurrencyPipe */]]
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */],
            __WEBPACK_IMPORTED_MODULE_5__providers_doacao_doacao__["a" /* DoacaoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_stripe__["a" /* Stripe */],
            __WEBPACK_IMPORTED_MODULE_8__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_9__angular_common__["c" /* CurrencyPipe */]])
    ], QueroContribuirPage);
    return QueroContribuirPage;
    var QueroContribuirPage_1;
}());

var TseModalPage = /** @class */ (function () {
    function TseModalPage(params, navCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    TseModalPage.prototype.close = function () {
        this.navCtrl.pop();
    };
    TseModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'modal-tse',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/quero-contribuir/modal-tse.html"*/'<div class="container-round">\n        <div class="box">\n            <button class="btnClose" (click)="close()">\n                <ion-icon name="close"></ion-icon>\n            </button>\n            <div class="text">\n                    <p>Art. 33. É vedado a partido político e a candidato receber, direta ou indiretamente, doação em dinheiro ou estimável em dinheiro, inclusive por meio de publicidade de qualquer espécie, procedente de:</p>\n                    <p>I - pessoas jurídicas;</p>\n                    <p>II - origem estrangeira;</p>\n                    <p>III - pessoa física que exerça atividade comercial decorrente de permissão pública.</p>\n                    <p>§ 1º A vedação prevista no inciso III não alcança a aplicação de recursos próprios do candidato em sua campanha.</p>\n                    <p>§ 2º O recurso recebido por candidato ou partido oriundo de fontes vedadas deve ser imediatamente devolvido ao doador, sendo vedada sua utilização ou aplicação financeira.</p>\n                    <p>§ 3º Na impossibilidade de devolução dos recursos ao doador, o prestador de contas deve providenciar imediatamente a transferência dos recursos recebidos ao Tesouro Nacional, por meio de Guia de Recolhimento da União (GRU).</p>\n                    <p>§ 4º Incidirão atualização monetária e juros moratórios, calculados com base na taxa aplicável aos créditos da Fazenda Pública, sobre os valores a serem recolhidos ao Tesouro Nacional, desde a data da ocorrência do fato gerador até a do efetivo recolhimento, salvo se tiver sido determinado de forma diversa na decisão judicial.</p>\n                    <p>§ 5º O disposto no § 4º não se aplica quando o candidato ou o partido político promove espontânea e imediatamente a transferência dos recursos para o Tesouro Nacional, sem deles se utilizar.</p>\n                    <p>§ 6º A transferência de recurso recebido de fonte vedada para outro órgão partidário ou candidato não isenta o donatário da obrigação prevista no § 2º.</p>\n                    <p>§ 7º O beneficiário de transferência cuja origem seja considerada fonte vedada pela Justiça Eleitoral responde solidariamente pela irregularidade, e as consequências serão aferidas por ocasião do julgamento das respectivas contas.</p>\n                    <p>§ 8º A devolução ou a determinação de devolução de recursos recebidos de fonte vedada não impedem, se for o caso, a reprovação das contas, quando constatado que o candidato se beneficiou, ainda que temporariamente, dos recursos ilícitos recebidos, assim como a apuração do fato na forma do art. 30-A da Lei nº 9.504/1997, do art. 22 da Lei Complementar nº 64/1990 e do art. 14, § 10, da Constituição da República.</p>\n                    <p>\n                        § 9º O comprovante de devolução ou de recolhimento, conforme o caso, poderá ser apresentado em qualquer fase da prestação de contas ou até 5 (cinco) dias após o trânsito em julgado da decisão que julgar as contas de campanha, sob pena de encaminhamento das informações à representação estadual ou municipal da Advocacia-Geral da União para fins de cobrança.\n                    </p>\n                    <p>Para visualizar todas as informações da RESOLUÇÃO Nº 23.553, acesse <a target="_blank" href="http://www.tse.jus.br/legislacao-tse/res/2017/RES235532017.html"> http://www.tse.jus.br/legislacao-tse/res/2017/RES235532017.html</a></p>\n            </div>\n            <button class="btnOk" (click)="close()">Ok</button>\n           \n        </div>\n    </div>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/quero-contribuir/modal-tse.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], TseModalPage);
    return TseModalPage;
}());

//# sourceMappingURL=quero-contribuir.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DoacaoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DoacaoProvider = /** @class */ (function () {
    function DoacaoProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    DoacaoProvider.prototype.get = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Doacao/Config?ClienteId=" + 2;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    DoacaoProvider.prototype.post = function (doacao) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Doacao";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, doacao, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    DoacaoProvider.prototype.put = function (doacao) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Doacao/Passo";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, doacao, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    DoacaoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], DoacaoProvider);
    return DoacaoProvider;
}());

//# sourceMappingURL=doacao.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConvideAmigosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConvideAmigosPage = /** @class */ (function () {
    function ConvideAmigosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ConvideAmigosPage.prototype.ionViewDidLoad = function () {
    };
    ConvideAmigosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'convide-amigos',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/programa-governo/modal-convideamigos.html"*/'<div class="container-round" text-center>\n  <header>\n    <h3 class="title-blue">Amigos que estão no movimento</h3>\n    <p>Seus amigos do Facebook</p>\n  </header>\n  <ion-list>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n          <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n          <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n          <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n          <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>\n  </ion-list>\n</div>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/programa-governo/modal-convideamigos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], ConvideAmigosPage);
    return ConvideAmigosPage;
}());

//# sourceMappingURL=convide-amigos.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeralProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GeralProvider = /** @class */ (function () {
    function GeralProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    GeralProvider.prototype.findAll = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos?TipoConteudoId=14";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result[result.length - 1]);
            }, function (error) {
                reject(error);
            });
        });
    };
    // findPrograma() {
    //     var token = this.auth.getToken();
    //     let url = `${CONFIG_PROJECT.baseApi}/Conteudos?TipoConteudoId=16`;
    //     let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };
    //     return new Promise((resolve, reject) => {
    //         this.http.get(url, header)
    //             .map(res => res)
    //             .subscribe((result: any) => {
    //                 resolve(result);
    //             },
    //             (error) => {
    //                 reject(error);
    //             });
    //     });
    // }
    GeralProvider.prototype.findPrograma = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos?TipoConteudoId=16&size=100&paginar=true";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    GeralProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], GeralProvider);
    return GeralProvider;
}());

//# sourceMappingURL=geral.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstitucionalProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InstitucionalProvider = /** @class */ (function () {
    function InstitucionalProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    InstitucionalProvider.prototype.findAcervo = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Acervo";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    InstitucionalProvider.prototype.findAll = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos?TipoConteudoId=13";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    InstitucionalProvider.prototype.findById = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    InstitucionalProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], InstitucionalProvider);
    return InstitucionalProvider;
}());

//# sourceMappingURL=institucional.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PontoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PontoProvider = /** @class */ (function () {
    function PontoProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    PontoProvider.prototype.getExtrato = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pontos/Extrato";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PontoProvider.prototype.getMedalhas = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pontos/Medalhas";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PontoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], PontoProvider);
    return PontoProvider;
}());

//# sourceMappingURL=ponto.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComitesPageModule", function() { return ComitesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__comites__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ComitesPageModule = /** @class */ (function () {
    function ComitesPageModule() {
    }
    ComitesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__comites__["a" /* ComitesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__comites__["a" /* ComitesPage */])
            ]
        })
    ], ComitesPageModule);
    return ComitesPageModule;
}());

//# sourceMappingURL=comites.module.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConvitePageModule", function() { return ConvitePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__convite__ = __webpack_require__(336);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConvitePageModule = /** @class */ (function () {
    function ConvitePageModule() {
    }
    ConvitePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__convite__["a" /* ConvitePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__convite__["a" /* ConvitePage */]),
            ],
        })
    ], ConvitePageModule);
    return ConvitePageModule;
}());

//# sourceMappingURL=convite.module.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailProgramaPageModule", function() { return DetailProgramaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detail_programa__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetailProgramaPageModule = /** @class */ (function () {
    function DetailProgramaPageModule() {
    }
    DetailProgramaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detail_programa__["a" /* DetailProgramaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detail_programa__["a" /* DetailProgramaPage */]),
            ],
        })
    ], DetailProgramaPageModule);
    return DetailProgramaPageModule;
}());

//# sourceMappingURL=detail-programa.module.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailTransmissaoPageModule", function() { return DetailTransmissaoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detail_transmissao__ = __webpack_require__(337);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetailTransmissaoPageModule = /** @class */ (function () {
    function DetailTransmissaoPageModule() {
    }
    DetailTransmissaoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detail_transmissao__["a" /* DetailTransmissaoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detail_transmissao__["a" /* DetailTransmissaoPage */]),
            ],
        })
    ], DetailTransmissaoPageModule);
    return DetailTransmissaoPageModule;
}());

//# sourceMappingURL=detail-transmissao.module.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(143);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */])
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialInstitucionalPageModule", function() { return MaterialInstitucionalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__material_institucional__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MaterialInstitucionalPageModule = /** @class */ (function () {
    function MaterialInstitucionalPageModule() {
    }
    MaterialInstitucionalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__material_institucional__["a" /* MaterialInstitucionalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__material_institucional__["a" /* MaterialInstitucionalPage */]),
            ],
        })
    ], MaterialInstitucionalPageModule);
    return MaterialInstitucionalPageModule;
}());

//# sourceMappingURL=material-institucional.module.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoticiasNoticiaDetalhePageModule", function() { return NoticiasNoticiaDetalhePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__noticias_noticia_detalhe__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NoticiasNoticiaDetalhePageModule = /** @class */ (function () {
    function NoticiasNoticiaDetalhePageModule() {
    }
    NoticiasNoticiaDetalhePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__noticias_noticia_detalhe__["a" /* NoticiasNoticiaDetalhePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__noticias_noticia_detalhe__["a" /* NoticiasNoticiaDetalhePage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], NoticiasNoticiaDetalhePageModule);
    return NoticiasNoticiaDetalhePageModule;
}());

//# sourceMappingURL=noticias-noticia-detalhe.module.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificacoesNotificacoesDetalhePageModule", function() { return NotificacoesNotificacoesDetalhePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notificacoes_notificacoes_detalhe__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NotificacoesNotificacoesDetalhePageModule = /** @class */ (function () {
    function NotificacoesNotificacoesDetalhePageModule() {
    }
    NotificacoesNotificacoesDetalhePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notificacoes_notificacoes_detalhe__["a" /* NotificacoesNotificacoesDetalhePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notificacoes_notificacoes_detalhe__["a" /* NotificacoesNotificacoesDetalhePage */]),
            ],
        })
    ], NotificacoesNotificacoesDetalhePageModule);
    return NotificacoesNotificacoesDetalhePageModule;
}());

//# sourceMappingURL=notificacoes-notificacoes-detalhe.module.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificacoesPageModule", function() { return NotificacoesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notificacoes__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NotificacoesPageModule = /** @class */ (function () {
    function NotificacoesPageModule() {
    }
    NotificacoesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notificacoes__["a" /* NotificacoesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notificacoes__["a" /* NotificacoesPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], NotificacoesPageModule);
    return NotificacoesPageModule;
}());

//# sourceMappingURL=notificacoes.module.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParticipacaoEnviarParticipacaoPageModule", function() { return ParticipacaoEnviarParticipacaoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__participacao_enviar_participacao__ = __webpack_require__(341);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ParticipacaoEnviarParticipacaoPageModule = /** @class */ (function () {
    function ParticipacaoEnviarParticipacaoPageModule() {
    }
    ParticipacaoEnviarParticipacaoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__participacao_enviar_participacao__["a" /* ParticipacaoEnviarParticipacaoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__participacao_enviar_participacao__["a" /* ParticipacaoEnviarParticipacaoPage */]),
            ],
        })
    ], ParticipacaoEnviarParticipacaoPageModule);
    return ParticipacaoEnviarParticipacaoPageModule;
}());

//# sourceMappingURL=participacao-enviar-participacao.module.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPageModule", function() { return PerfilPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__perfil__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PerfilPageModule = /** @class */ (function () {
    function PerfilPageModule() {
    }
    PerfilPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */]),
                __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ],
        })
    ], PerfilPageModule);
    return PerfilPageModule;
}());

//# sourceMappingURL=perfil.module.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PesquisaPageModule", function() { return PesquisaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pesquisa__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PesquisaPageModule = /** @class */ (function () {
    function PesquisaPageModule() {
    }
    PesquisaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pesquisa__["b" /* PesquisaPage */],
                __WEBPACK_IMPORTED_MODULE_2__pesquisa__["a" /* PesquisaModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pesquisa__["b" /* PesquisaPage */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pesquisa__["a" /* PesquisaModalPage */])
            ],
        })
    ], PesquisaPageModule);
    return PesquisaPageModule;
}());

//# sourceMappingURL=pesquisa.module.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgramaGovernoPageModule", function() { return ProgramaGovernoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__programa_governo__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__convide_amigos__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProgramaGovernoPageModule = /** @class */ (function () {
    function ProgramaGovernoPageModule() {
    }
    ProgramaGovernoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__programa_governo__["a" /* ProgramaGovernoPage */],
                __WEBPACK_IMPORTED_MODULE_3__convide_amigos__["a" /* ConvideAmigosPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__programa_governo__["a" /* ProgramaGovernoPage */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__convide_amigos__["a" /* ConvideAmigosPage */])
            ],
        })
    ], ProgramaGovernoPageModule);
    return ProgramaGovernoPageModule;
}());

//# sourceMappingURL=programa-governo.module.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgramaPontosPageModule", function() { return ProgramaPontosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__programa_pontos__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProgramaPontosPageModule = /** @class */ (function () {
    function ProgramaPontosPageModule() {
    }
    ProgramaPontosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__programa_pontos__["a" /* ProgramaPontosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__programa_pontos__["a" /* ProgramaPontosPage */]),
            ],
        })
    ], ProgramaPontosPageModule);
    return ProgramaPontosPageModule;
}());

//# sourceMappingURL=programa-pontos.module.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QueroContribuirPageModule", function() { return QueroContribuirPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__quero_contribuir__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var QueroContribuirPageModule = /** @class */ (function () {
    function QueroContribuirPageModule() {
    }
    QueroContribuirPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__quero_contribuir__["a" /* QueroContribuirPage */],
                __WEBPACK_IMPORTED_MODULE_2__quero_contribuir__["b" /* TseModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__quero_contribuir__["a" /* QueroContribuirPage */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__quero_contribuir__["b" /* TseModalPage */]),
                __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ],
        })
    ], QueroContribuirPageModule);
    return QueroContribuirPageModule;
}());

//# sourceMappingURL=quero-contribuir.module.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TutorialPage = /** @class */ (function () {
    function TutorialPage(navCtrl, navParams, loadingCtrl, auth, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.auth = auth;
        this.events = events;
        this.user = {
            username: '',
            password: ''
        };
        this.hiddenNext = false;
    }
    TutorialPage.prototype.ionViewDidLoad = function () { };
    TutorialPage.prototype.goState = function (user) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        user = {
            username: '',
            password: ''
        };
        this.auth.postLogin(user)
            .then(function (response) {
            setTimeout(function () {
                _this.auth.saveSection(response);
                _this.events.publish('profile', response);
                loading.dismiss();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
            }, 2000);
        });
    };
    TutorialPage.prototype.goToSlide = function () {
        this.slides.slideNext();
    };
    TutorialPage.prototype.slideChanged = function () {
        this.slides.isEnd() == true ? this.hiddenNext = true : this.hiddenNext = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
    ], TutorialPage.prototype, "slides", void 0);
    TutorialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tutorial',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/tutorial/tutorial.html"*/'<ion-content style="padding:10%;"> \n    <ion-slides pager (ionSlideDidChange)="slideChanged()" class="contentprehome" style=" background-image: url(./assets/imgs/bgtutorial.png);\n    background-repeat: no-repeat;\n    background-size: cover;">\n\n      <ion-slide text-center>\n        <img src="./assets/imgs/tutorial1.png" class="ico">\n        <h2 class="title-yellow">Seja um voluntário<br/> pela Pátria!</h2>\n        <p>Faça seu cadastro e fique por dentro das principais informações.</p>\n\n      <button class="bt-skip btntuto" (click)="goState()">Pular</button>\n\n      </ion-slide>\n\n      <ion-slide>\n        <img src="./assets/imgs/tutorial2.png" class="ico">\n        <h2 class="title-yellow">Suas interações\n         <br/> valem pontos! </h2>\n        <p>Preencher seus dados, compartilhar conteúdo e responder pesquisas dão pontos.</p>\n      <button class="bt-skip btntuto" (click)="goState()">Pular</button>\n\n      </ion-slide>\n\n      <ion-slide>\n        <img src="./assets/imgs/tutorial3.png" class="ico">\n        <h2 class="title-yellow">Seus pontos te\n          <br/> dão medalhas!</h2>\n        <p>Quanto mais você participa, mais pontos tem e mais níveis vai subindo.\n        </p>\n      <button class="bt-skip btntuto" (click)="goState()" style="border: none;\n      background: #0276a9;">Começar!</button>\n     \n      </ion-slide>\n\n\n    </ion-slides>\n    <footer class="footer">\n      <!-- <button class="bt-skip" (click)="goState()">Pular</button> -->\n      <!-- <ion-icon name="arrow-forward" [hidden]="hiddenNext" (click)="goToSlide()"></ion-icon> -->\n    </footer>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/tutorial/tutorial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], TutorialPage);
    return TutorialPage;
}());

//# sourceMappingURL=tutorial.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndiqueAmigosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var IndiqueAmigosPage = /** @class */ (function () {
    function IndiqueAmigosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    IndiqueAmigosPage.prototype.ionViewDidLoad = function () {
    };
    IndiqueAmigosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'indique-amigos',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/voluntario/indique-amigos.html"*/'<div class="container-round" text-center>\n  <header>\n    <h3 class="title-blue">Indique amigos</h3>\n    <p>Ganhe X pontos a cada amigo que se cadastrar.</p>\n  </header>\n  <h4 class="title">Digite o e-mail de seu amigo para enviar sua indicação.</h4>\n  <ion-list>\n    <ion-item>\n      <ion-label stacked text-uppercase>E-mail</ion-label>\n      <ion-input type="text" placeholder="Digite o e-mail de seu amigo(a)"></ion-input>\n    </ion-item>\n    <button ion-button clear> <ion-icon name="add"></ion-icon> Adicionar mais um amigo</button>\n    <div margin-top>\n        <button ion-button block>Enviar</button>\n    </div>\n  </ion-list>\n</div>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/voluntario/indique-amigos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], IndiqueAmigosPage);
    return IndiqueAmigosPage;
}());

//# sourceMappingURL=indique-amigos.js.map

/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(291);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login_module__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_voluntario_voluntario_module__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_notificacoes_notificacoes_module__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_comites_comites_module__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_participacao_participacao_module__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_tutorial_tutorial_module__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_pesquisa_pesquisa_module__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_programa_governo_programa_governo_module__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_convite_convite_module__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_noticias_noticia_detalhe_noticias_noticia_detalhe_module__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_perfil_perfil_module__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_material_institucional_material_institucional_module__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_programa_pontos_programa_pontos_module__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_detail_programa_detail_programa_module__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_detail_transmissao_detail_transmissao_module__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_searchbar_searchbar__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_in_app_browser__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_push__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__angular_forms__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_quero_contribuir_quero_contribuir_module__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__angular_http__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35_brmasker_ionic_3__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__providers_agenda_agenda__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__angular_common__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__angular_common_locales_pt__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__providers_conteudo_conteudo__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pipes_pipes_module__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__providers_comite_comite__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__providers_geral_geral__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__providers_voluntario_voluntario__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__providers_institucional_institucional__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__ionic_native_file_transfer__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__ionic_native_file__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__providers_pesquisa_pesquisa__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__providers_ponto_ponto__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__ionic_native_android_permissions__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__ionic_native_camera__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__ionic_native_network__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__ionic_native_social_sharing__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_social_share_social_share__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_social_share_material_social_share_material__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__ionic_native_app_availability__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_modal_sucesso_modal_sucesso__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__ionic_native_geolocation__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__providers_doacao_doacao__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__ionic_native_local_notifications__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__providers_participacao_participacao__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__providers_push_push__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_notificacoes_notificacoes_notificacoes_detalhe_notificacoes_notificacoes_detalhe_module__ = __webpack_require__(218);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

































//Modulos























// import { ModalSucessoPage1 } from '../pages/cadastro/modal-sucesso/modal-sucesso';








Object(__WEBPACK_IMPORTED_MODULE_37__angular_common__["j" /* registerLocaleData */])(__WEBPACK_IMPORTED_MODULE_38__angular_common_locales_pt__["a" /* default */], 'pt');
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_25__components_searchbar_searchbar__["a" /* SearchbarComponent */],
                __WEBPACK_IMPORTED_MODULE_54__pages_social_share_social_share__["a" /* SocialSharePage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_modal_sucesso_modal_sucesso__["a" /* ModalSucessoPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_social_share_material_social_share_material__["a" /* SocialShareMaterialPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__["TextMaskModule"],
                __WEBPACK_IMPORTED_MODULE_31__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login_module__["LoginPageModule"],
                __WEBPACK_IMPORTED_MODULE_11__pages_voluntario_voluntario_module__["a" /* VoluntarioPageModule */],
                __WEBPACK_IMPORTED_MODULE_13__pages_comites_comites_module__["ComitesPageModule"],
                __WEBPACK_IMPORTED_MODULE_12__pages_notificacoes_notificacoes_module__["NotificacoesPageModule"],
                __WEBPACK_IMPORTED_MODULE_63__pages_notificacoes_notificacoes_notificacoes_detalhe_notificacoes_notificacoes_detalhe_module__["NotificacoesNotificacoesDetalhePageModule"],
                __WEBPACK_IMPORTED_MODULE_14__pages_participacao_participacao_module__["a" /* ParticipacaoPageModule */],
                __WEBPACK_IMPORTED_MODULE_15__pages_tutorial_tutorial_module__["a" /* TutorialPageModule */],
                __WEBPACK_IMPORTED_MODULE_16__pages_pesquisa_pesquisa_module__["PesquisaPageModule"],
                __WEBPACK_IMPORTED_MODULE_18__pages_convite_convite_module__["ConvitePageModule"],
                __WEBPACK_IMPORTED_MODULE_17__pages_programa_governo_programa_governo_module__["ProgramaGovernoPageModule"],
                __WEBPACK_IMPORTED_MODULE_20__pages_perfil_perfil_module__["PerfilPageModule"],
                __WEBPACK_IMPORTED_MODULE_19__pages_noticias_noticia_detalhe_noticias_noticia_detalhe_module__["NoticiasNoticiaDetalhePageModule"],
                __WEBPACK_IMPORTED_MODULE_21__pages_material_institucional_material_institucional_module__["MaterialInstitucionalPageModule"],
                __WEBPACK_IMPORTED_MODULE_22__pages_programa_pontos_programa_pontos_module__["ProgramaPontosPageModule"],
                __WEBPACK_IMPORTED_MODULE_23__pages_detail_programa_detail_programa_module__["DetailProgramaPageModule"],
                __WEBPACK_IMPORTED_MODULE_24__pages_detail_transmissao_detail_transmissao_module__["DetailTransmissaoPageModule"],
                __WEBPACK_IMPORTED_MODULE_33__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_40__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_35_brmasker_ionic_3__["a" /* BrMaskerModule */],
                __WEBPACK_IMPORTED_MODULE_30__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_35_brmasker_ionic_3__["a" /* BrMaskerModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                    menuType: 'overlay',
                    backButtonText: ' '
                }, {
                    links: [
                        { loadChildren: '../pages/cadastro/cadastro-tipoperfil.module#CadastroTipoPerfilPageModule', name: 'cadastro-tipoperfil', segment: 'cadastro-tipoperfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cadastro/cadastro.module#CadastroPageModule', name: 'cadastro', segment: 'cadastro', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/comites/comites-comites-detalhe/comites-comites-detalhe.module#ComitesComitesDetalhePageModule', name: 'comites-detalhe', segment: 'comites-detalhe/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/comites/comites.module#ComitesPageModule', name: 'comites', segment: 'comites', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/convite/convite.module#ConvitePageModule', name: 'convite', segment: 'convite', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detail-programa/detail-programa.module#DetailProgramaPageModule', name: 'DetailProgramaPage', segment: 'detail-programa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dados-complementares/dados-complementares.module#DadosComplementaresPageModule', name: 'dados-complementares', segment: 'dados-complementares', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detail-transmissao/detail-transmissao.module#DetailTransmissaoPageModule', name: 'DetailTransmissaoPage', segment: 'detail-transmissao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/evento/evento.module#EventoPageModule', name: 'evento', segment: 'evento/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login-login-esqueci/login-login-esqueci.module#LoginLoginEsqueciPageModule', name: 'esqueci-senha', segment: 'login-login-esqueci', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'login', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/material-institucional/material-institucional.module#MaterialInstitucionalPageModule', name: 'MaterialInstitucionalPage', segment: 'material-institucional', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/noticias-noticia-detalhe/noticias-noticia-detalhe.module#NoticiasNoticiaDetalhePageModule', name: 'noticias', segment: 'noticias/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe.module#NotificacoesNotificacoesDetalhePageModule', name: 'notificacoes', segment: 'notificacoes/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notificacoes/notificacoes.module#NotificacoesPageModule', name: 'NotificacoesPage', segment: 'notificacoes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/participacao/participacao-enviar-participacao/participacao-enviar-participacao.module#ParticipacaoEnviarParticipacaoPageModule', name: 'form-participacao', segment: 'form-participacao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/participacao/participacao-participacao-detalhe/participacao-participacao-detalhe.module#ParticipacaoParticipacaoDetalhePageModule', name: 'participacao-detalhe', segment: 'participacao/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/perfil/perfil.module#PerfilPageModule', name: 'perfil', segment: 'perfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pesquisa/pesquisa-pesquisa-resposta/pesquisa-pesquisa-resposta.module#PesquisaPesquisaRespostaPageModule', name: 'pesquisa-resposta', segment: 'pesquisa-resposta/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pesquisa/pesquisa.module#PesquisaPageModule', name: 'pesquisa-opniao', segment: 'pesquisa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/programa-governo/programa-governo.module#ProgramaGovernoPageModule', name: 'ProgramaGovernoPage', segment: 'programa-governo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/programa-pontos/programa-pontos.module#ProgramaPontosPageModule', name: 'ProgramaPontosPage', segment: 'programa-pontos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/quero-contribuir/quero-contribuir.module#QueroContribuirPageModule', name: 'quero-contribuir', segment: 'quero-contribuir', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/voluntario/voluntario-detalhe/voluntario-detalhe.module#VoluntarioDetalhePageModule', name: 'voluntario-detalhe', segment: 'voluntario-detalhe/:id', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                    name: '__mydb',
                    driverOrder: ['indexeddb', 'sqlite', 'websql']
                }),
                __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__["TextMaskModule"],
                __WEBPACK_IMPORTED_MODULE_32__pages_quero_contribuir_quero_contribuir_module__["QueroContribuirPageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_25__components_searchbar_searchbar__["a" /* SearchbarComponent */],
                __WEBPACK_IMPORTED_MODULE_54__pages_social_share_social_share__["a" /* SocialSharePage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_modal_sucesso_modal_sucesso__["a" /* ModalSucessoPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_social_share_material_social_share_material__["a" /* SocialShareMaterialPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__["a" /* Facebook */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_26__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_27__providers_person_person__["a" /* PersonProvider */],
                __WEBPACK_IMPORTED_MODULE_36__providers_agenda_agenda__["a" /* AgendaProvider */],
                __WEBPACK_IMPORTED_MODULE_34__providers_utils_utils__["a" /* UtilsProvider */],
                __WEBPACK_IMPORTED_MODULE_39__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
                __WEBPACK_IMPORTED_MODULE_41__providers_comite_comite__["a" /* ComiteProvider */],
                __WEBPACK_IMPORTED_MODULE_42__providers_notificacao_notificacao__["a" /* NotificationProvider */],
                __WEBPACK_IMPORTED_MODULE_43__providers_geral_geral__["a" /* GeralProvider */],
                __WEBPACK_IMPORTED_MODULE_44__providers_voluntario_voluntario__["a" /* VoluntarioProvider */],
                __WEBPACK_IMPORTED_MODULE_45__providers_institucional_institucional__["a" /* InstitucionalProvider */],
                __WEBPACK_IMPORTED_MODULE_46__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_52__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_47__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_48__providers_pesquisa_pesquisa__["a" /* PesquisaProvider */],
                __WEBPACK_IMPORTED_MODULE_49__providers_ponto_ponto__["a" /* PontoProvider */],
                __WEBPACK_IMPORTED_MODULE_50__ionic_native_android_permissions__["a" /* AndroidPermissions */],
                __WEBPACK_IMPORTED_MODULE_56__ionic_native_app_availability__["a" /* AppAvailability */],
                __WEBPACK_IMPORTED_MODULE_51__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_53__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_59__providers_doacao_doacao__["a" /* DoacaoProvider */],
                __WEBPACK_IMPORTED_MODULE_60__ionic_native_local_notifications__["a" /* LocalNotifications */],
                __WEBPACK_IMPORTED_MODULE_61__providers_participacao_participacao__["a" /* ParticipacaoProvider */],
                __WEBPACK_IMPORTED_MODULE_58__ionic_native_geolocation__["a" /* Geolocation */],
                [{ provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["LOCALE_ID"], useValue: 'pt' }],
                __WEBPACK_IMPORTED_MODULE_62__providers_push_push__["a" /* PushProvider */],
            ],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["CUSTOM_ELEMENTS_SCHEMA"],
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["NO_ERRORS_SCHEMA"]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConvitePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConvitePage = /** @class */ (function () {
    function ConvitePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ConvitePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConvitePage');
    };
    ConvitePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-convite',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/convite/convite.html"*/'<ion-content>\n  <div padding>\n    <header class="login-header" text-center>\n        <img src="http://via.placeholder.com/100x90" class="brand">\n        <h3 class="title-blue">Você precisa ser convidado!</h3>\n        <div class="block-text">\n          <p>Para se cadastrar em nosso movimento, você precisa ter um número de convite, que pode ser obtido através de um amigo que já faça parte desse movimento.</p>\n        </div>\n    </header>\n    <ion-list>\n      <ion-item>\n        <ion-label floating text-uppercase>Email</ion-label>\n        <ion-input type="email"></ion-input>\n      </ion-item>\n    \n      <ion-item>\n        <ion-label floating text-uppercase>Número do Convite</ion-label>\n        <ion-input type="text"></ion-input>\n      </ion-item>\n\n    </ion-list>\n    <button ion-button full text-uppercase margin-top>Entrar</button>\n  </div>\n  <footer class="container-footer" text-center>\n      <h2 class="title">Não tem convite?</h2>\n      <div class="block-text">\n        <p>Faça login com o Facebook e descubra quais amigos possuem convites para te enviar.</p>\n      </div>\n      <button ion-button outline full text-uppercase margin-top color="light">Ou faça login usando Facebook</button>\n  </footer>\n</ion-content>\n<ion-footer>\n  <ion-toolbar (click)="goPage(\'login\')" text-center>\n    Voltar para a tela de <strong>Login</strong>\n  </ion-toolbar>\n</ion-footer>\n\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/convite/convite.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], ConvitePage);
    return ConvitePage;
}());

//# sourceMappingURL=convite.js.map

/***/ }),

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailTransmissaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_person_person__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DetailTransmissaoPage = /** @class */ (function () {
    function DetailTransmissaoPage(navCtrl, navParams, modalCtrl, _sanitizer, person) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this._sanitizer = _sanitizer;
        this.person = person;
        this.interna = this.navParams.get('interna');
        console.log(this.interna);
        this.VideoIframe = this._sanitizer.bypassSecurityTrustResourceUrl(this.interna.Video);
    }
    DetailTransmissaoPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    DetailTransmissaoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailTransmissaoPage');
    };
    DetailTransmissaoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-detail-transmissao',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/detail-transmissao/detail-transmissao.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <ion-title text-center>{{interna.Titulo}}</ion-title>\n    <ion-buttons end>\n      <button  class="search-header">\n      <i class="fa fa-search" aria-hidden="true"></i>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content style=" background-color: #fff !important;background:#fff;">\n  <style>\n    .has-refresher > .scroll-content {\n    background-color: #fff !important;\n}\n.textovolunt{\n  padding-left:16px;\n  padding-right:16px;\n  padding-bottom: 10px;\n}\n.has-refresher > .scroll-content { background-color: #fff!important; }\n\n\n  </style>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <!-- <img src="{{interna.Imagem}}" class="img-featured" > -->\n  <iframe width="100%" height="400" [src]="VideoIframe" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>\n\n  <div  style="background-color:#fff!important; margin-top: -1%!important; ">\n    <!-- <header class="buttons-event right" style="height: 48px!important;\n    margin-top: -3%!important;padding: 16px;">\n      <button ion-button icon-left clear small [ngClass]="{\'liked\': interna.JaCurtiu}" (click)="postLike(interna)">\n        <ion-icon name="heart"></ion-icon>\n        <div>{{interna.Curtidas}}</div>\n      </button>\n    </header> -->\n    <div class="textovolunt">\n    <h2 class="bold">{{interna.Titulo}}</h2>\n    <div class="block-text" margin-top>\n      <p [innerHtml]="interna.Texto"></p>\n    </div>\n  </div>\n    <!-- <footer padding-top>\n      <button ion-button full text-uppercase (click)="openModal()">Convide Amigos</button>\n    </footer> -->\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/detail-transmissao/detail-transmissao.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_4__providers_person_person__["a" /* PersonProvider */]])
    ], DetailTransmissaoPage);
    return DetailTransmissaoPage;
}());

//# sourceMappingURL=detail-transmissao.js.map

/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoticiasNoticiaDetalhePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_conteudo_conteudo__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__social_share_social_share__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NoticiasNoticiaDetalhePage = /** @class */ (function () {
    function NoticiasNoticiaDetalhePage(navCtrl, navParams, _conteudo, auth, loading, utils, person, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._conteudo = _conteudo;
        this.auth = auth;
        this.loading = loading;
        this.utils = utils;
        this.person = person;
        this.modalCtrl = modalCtrl;
        this.conteudo = {};
        this.noticiasList = [];
        this.tipoConteudo = 2;
        this.IsAnonimo = localStorage.getItem('profileName');
        this.page = 1;
    }
    NoticiasNoticiaDetalhePage.prototype.ionViewDidLoad = function () {
        this.loadDetail();
        this.loadNews();
    };
    NoticiasNoticiaDetalhePage.prototype.loadDetail = function () {
        var _this = this;
        var loading = this.loading.create();
        loading.present();
        this.id = this.navParams.data.id;
        return new Promise(function (resolve) {
            _this._conteudo.getDetalhe(_this.auth.getToken(), _this.id)
                .then(function (data) {
                _this.conteudo = data;
                resolve(true);
                loading.dismiss();
            });
        });
    };
    NoticiasNoticiaDetalhePage.prototype.loadNews = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this._conteudo.getConteudo(_this.auth.getToken(), _this.page, _this.tipoConteudo)
                .then(function (data) {
                var noticias = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Id != _this.conteudo.Id) {
                        noticias.push(data[i]);
                    }
                }
                _this.noticiasList = noticias;
                resolve(true);
            });
        });
    };
    NoticiasNoticiaDetalhePage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, { id: id });
    };
    NoticiasNoticiaDetalhePage.prototype.postLike = function (conteudo) {
        var _this = this;
        if (this.IsAnonimo == 'Anônimo') {
            this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
        }
        else {
            var idPost_1 = conteudo.Id;
            this.person.get()
                .then(function (response) {
                var person = response;
                _this._conteudo.postLike(idPost_1, person.Id)
                    .then(function (response) {
                    if (response.Curtiu && !conteudo.JaCurtiu) {
                        conteudo.Curtidas += 1;
                    }
                    else {
                        conteudo.Curtidas -= 1;
                    }
                    conteudo.JaCurtiu = response.Curtiu;
                    if (response.GanhouPonto) {
                        _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                    }
                });
            });
        }
    };
    NoticiasNoticiaDetalhePage.prototype.postShare = function (conteudo) {
        if (this.IsAnonimo == 'Anônimo') {
            this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
        }
        else {
            console.log(conteudo);
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__social_share_social_share__["a" /* SocialSharePage */], conteudo);
            modal.present();
        }
    };
    NoticiasNoticiaDetalhePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-noticias-noticia-detalhe',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/noticias-noticia-detalhe/noticias-noticia-detalhe.html"*/'<ion-header color="header">\n \n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Notícias</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content style=" background: #fff!important;">\n    <style>\n     section, .scroll-content, .fixed-content, div {\n  background: #fff!important;\n  margin-top: -1%;\n}\n.vejaoutras{\n        background:#fafafa!important;\n    }\n.button-clear-ios {\n    border-color: transparent;\n    color: #bfbfbf!important;\n    background-color: transparent;\n}\n\n        </style>\n  <img *ngIf="conteudo.Imagem"[src]="conteudo.Imagem" class="img-featured">\n  <div>\n    <section class="buttons-event classeheader" style="padding:16px;">\n      <small class="date">{{conteudo.DataHora}}</small>\n      <div class="buttons">\n      <button ion-button icon-left clear small (click)="postLike(conteudo)" [ngClass]="{\'liked\': conteudo.JaCurtiu}" style="border-radius: 0!important;\n      border-right: 1px solid;    height: 16px;">\n      <ion-icon name="heart" [ngClass]="{\'liked\': conteudo.JaCurtiu}"></ion-icon>\n      <!-- <ion-icon name="heart" *ngIf="conteudo.Curtidas > 0" style="color:#004f99"></ion-icon> -->\n      <div [ngClass]="{\'liked\': conteudo.JaCurtiu}">{{conteudo.Curtidas}}</div>\n      <!-- <div *ngIf="conteudo.Curtidas > 0" style="color:#004f99">{{conteudo.Curtidas}}</div> -->\n      </button>\n      <button class="share" ion-button icon-right text-left clear small (click)="postShare(conteudo)">\n        <img src="assets/imgs/sharenot.png" class="imgshare">\n              \n      </button>\n    </div>\n    </section>\n    <section class="firstSec">\n      <small text-uppercase class="yellow-color bold">{{conteudo.CategoriaConteudo?.Nome}}</small>\n      <h2 class="bold">{{conteudo.Titulo}}</h2>\n      <div class="block-text" margin-top [innerHtml]="conteudo.Texto"></div>\n    </section>\n    <section class="padding-section vejaoutras">\n      <h5 class="bold">Veja outras notícias</h5>\n      <ion-slides pager spaceBetween="-40">\n        <ion-slide *ngFor="let noticia of (noticiasList | slice: -3)">\n          <ion-card text-left (click)="openDetail(\'noticias\', noticia.Id)">\n            <ion-card-content>\n              <small text-uppercase class="yellow-color">{{noticia.CategoriaConteudo?.Nome}}</small>\n              <ion-card-title>{{noticia.Titulo}}</ion-card-title>\n              <!-- <div [innerHtml]="noticia.Texto | excerpt:50"></div> -->\n            </ion-card-content>\n            <ion-row class="footer-card">\n              <ion-col>\n                <button ion-button icon-left clear small (click)="postLike(noticia)" [ngClass]="{\'liked\': noticia.JaCurtiu}">\n                  <ion-icon name="heart"></ion-icon>\n                  <div>{{noticia.Curtidas}}</div>\n                </button>\n              </ion-col>\n              <ion-col text-right>\n                <ion-note>{{noticia.Hora}}</ion-note>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-slide>\n      </ion-slides>\n\n    </section>\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/noticias-noticia-detalhe/noticias-noticia-detalhe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], NoticiasNoticiaDetalhePage);
    return NoticiasNoticiaDetalhePage;
}());

//# sourceMappingURL=noticias-noticia-detalhe.js.map

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExcerptPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ExcerptPipe = /** @class */ (function () {
    function ExcerptPipe() {
    }
    ExcerptPipe.prototype.transform = function (text, length) {
        if (!text || !length) {
            return text;
        }
        if (text.length > length) {
            return text.substr(0, length) + '...';
        }
        return text;
    };
    ExcerptPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'excerpt',
            pure: false
        })
    ], ExcerptPipe);
    return ExcerptPipe;
}());

//# sourceMappingURL=excerpt.js.map

/***/ }),

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var NotificationsPipe = /** @class */ (function () {
    function NotificationsPipe() {
    }
    NotificationsPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var valueNum = parseInt(value);
        if (valueNum > 999) {
            return "999+";
        }
        return valueNum.toString();
    };
    NotificationsPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'notifications',
        })
    ], NotificationsPipe);
    return NotificationsPipe;
}());

//# sourceMappingURL=notifications.js.map

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParticipacaoEnviarParticipacaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_participacao_participacao__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_loading_loading_controller__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__participacao__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_diagnostic__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_image_picker__ = __webpack_require__(343);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ParticipacaoEnviarParticipacaoPage = /** @class */ (function () {
    function ParticipacaoEnviarParticipacaoPage(navCtrl, navParams, utils, geo, participacao, camera, loadingCtrl, diagnostic, alertCtrl, imagePicker) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.utils = utils;
        this.geo = geo;
        this.participacao = participacao;
        this.camera = camera;
        this.loadingCtrl = loadingCtrl;
        this.diagnostic = diagnostic;
        this.alertCtrl = alertCtrl;
        this.imagePicker = imagePicker;
        this.tab = "local";
        this.estados = new Array();
        this.solicitacao = {
            Latitute: 0,
            Longitude: 0,
            Rua: '',
            Numero: '',
            Bairro: '',
            Cidade: '',
            Estado: '',
            SolicitacaoCategoriaId: 0,
            Texto: '',
            Base64Files: []
        };
        this.erroPart1 = 0.7;
        this.erroParteFinal = 0.7;
        this.erroEstado = false;
        this.erroCidade = false;
        this.erroRua = false;
        this.erroNumero = false;
        this.erroBairro = false;
        this.ErroTexto = false;
        this.fileArray = new Array();
        this.selectOptEstados = {
            title: 'Selecione o estado',
            subTitle: 'Selecione o estado',
            checked: true
        };
        this.selectOptCidades = {
            title: 'Selecione a cidade',
            subTitle: 'Selecione a cidade',
            checked: true
        };
    }
    ParticipacaoEnviarParticipacaoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ParticipacaoEnviarParticipacaoPage');
        this.getEstados();
        this.getCategorias();
    };
    ParticipacaoEnviarParticipacaoPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            //this.choiceCategorie()
            refresher.complete();
        }, 1000);
    };
    ParticipacaoEnviarParticipacaoPage.prototype.saveLocal = function () {
        if (this.solicitacao.Rua != '' && this.solicitacao.Numero != '' && this.solicitacao.Bairro != '' && this.solicitacao.Cidade != '' && this.solicitacao.Estado != '') {
            this.tab = "categoria";
        }
        else {
            this.validaCampos();
        }
    };
    ParticipacaoEnviarParticipacaoPage.prototype.validaCampos = function () {
        var erro = false;
        if (this.solicitacao.Rua == '') {
            erro = true;
            this.erroRua = true;
        }
        else {
            this.erroRua = false;
        }
        if (this.solicitacao.Numero == '') {
            this.erroNumero = true;
            erro = true;
        }
        else {
            this.erroNumero = false;
        }
        if (this.solicitacao.Bairro == '') {
            this.erroBairro = true;
            erro = true;
        }
        else {
            this.erroBairro = false;
        }
        if (this.solicitacao.Cidade == '') {
            this.erroCidade = true;
            erro = true;
        }
        else {
            this.erroCidade = false;
        }
        if (this.solicitacao.Estado == '') {
            this.erroEstado = true;
            erro = true;
        }
        else {
            this.erroEstado = false;
        }
        if (!erro) {
            this.erroPart1 = 1;
        }
        return erro;
    };
    ParticipacaoEnviarParticipacaoPage.prototype.validaCampos2 = function () {
        var erro = false;
        if (this.solicitacao.Texto == '') {
            this.ErroTexto = true;
            erro = true;
        }
        else {
            this.ErroTexto = false;
        }
        if (!erro) {
            this.erroParteFinal = 1;
        }
        return erro;
    };
    ParticipacaoEnviarParticipacaoPage.prototype.choiceCategorie = function (categ) {
        this.categoriasfilhas = categ.CategoriasFilhas;
        if (categ.CategoriasFilhas.length == 0) {
            this.solicitacao.SolicitacaoCategoriaId = categ.Id;
            this.tab = "descricao";
        }
        else {
            this.tab = "tipo";
        }
    };
    ParticipacaoEnviarParticipacaoPage.prototype.saveTipe = function (id) {
        this.solicitacao.SolicitacaoCategoriaId = id;
        this.tab = "descricao";
    };
    ParticipacaoEnviarParticipacaoPage.prototype.getEstados = function () {
        var _this = this;
        this.utils.getEstados().then(function (result) { _this.estados = result; }, function (error) { });
    };
    ParticipacaoEnviarParticipacaoPage.prototype.onSelectCidade = function (uf) {
        var _this = this;
        this.utils.getCidades(uf).then(function (result) { _this.cidades = result; }, function (error) { });
        if (this.solicitacao.Cidade != '' && this.solicitacao.Cidade != null) {
            this.selectOptCidades.checked = false;
        }
    };
    ParticipacaoEnviarParticipacaoPage.prototype.getLocation = function () {
        var _this = this;
        this.diagnostic.isLocationEnabled().then(function (successCallback) {
            if (successCallback) {
                var loading_1 = _this.loadingCtrl.create({
                    content: 'Carregando...'
                });
                loading_1.present();
                var options = { enableHighAccuracy: true };
                _this.geo.getCurrentPosition(options).then(function (position) {
                    console.log(position);
                    _this.solicitacao.Latitute = position.coords.latitude;
                    _this.solicitacao.Longitude = position.coords.longitude;
                    _this.utils.getGeoLocation(_this.solicitacao.Latitute, _this.solicitacao.Longitude).then(function (result) {
                        for (var i = 0; i < 3; i++) {
                            var resultado = result.results[i];
                            _this.preencheEndereco(resultado);
                        }
                        loading_1.dismiss();
                    });
                }).catch(function (error) {
                    console.log('Error getting location', error);
                });
            }
            else {
                _this.alertCtrl.create({
                    title: '<img src="assets/icon/error.png"/> Erro',
                    subTitle: 'GPS desativado',
                    buttons: [{ text: 'Ok' }],
                    cssClass: 'alertcss'
                }).present();
            }
        }).catch(function (errorCallback) { _this.utils.showModalError('GPS desativado', 'Ative o GPS', 'OK'); });
    };
    ParticipacaoEnviarParticipacaoPage.prototype.preencheEndereco = function (resultado) {
        var _this = this;
        resultado.address_components.forEach(function (address_component) {
            address_component.types.forEach(function (type) {
                if (type == 'route' && _this.solicitacao.Rua == '') {
                    _this.solicitacao.Rua = address_component.short_name;
                }
                else if (type == 'street_number' && _this.solicitacao.Numero == '') {
                    _this.solicitacao.Numero = address_component.long_name;
                }
                else if (type == 'sublocality_level_1' && _this.solicitacao.Bairro == '') {
                    _this.solicitacao.Bairro = address_component.long_name;
                }
                else if (type == 'administrative_area_level_2' && _this.solicitacao.Cidade == '') {
                    _this.solicitacao.Cidade = address_component.long_name;
                }
                else if (type == 'administrative_area_level_1' && _this.solicitacao.Estado == '') {
                    _this.solicitacao.Estado = address_component.short_name;
                }
            });
        });
    };
    ParticipacaoEnviarParticipacaoPage.prototype.getCategorias = function () {
        var _this = this;
        this.participacao.getCategorias().then(function (result) {
            _this.categorias = result;
            console.log(result);
        });
    };
    ParticipacaoEnviarParticipacaoPage.prototype.abrirCamera = function () {
        this.takePicture(this.camera.PictureSourceType.CAMERA);
    };
    ParticipacaoEnviarParticipacaoPage.prototype.abrirGaleria = function () {
        var _this = this;
        var options = {
            maximumImagesCount: 5,
            width: 500,
            height: 500,
            quality: 75,
            outputType: 1 //base64
        };
        this.imagePicker.getPictures(options).then(function (file_uris) {
            for (var i = 0; i < file_uris.length; i++) {
                _this.solicitacao.Base64Files.push(file_uris[i]);
            }
        }, function (err) { return console.log('uh oh'); });
    };
    ParticipacaoEnviarParticipacaoPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 75,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG
        };
        this.camera.getPicture(options).then(function (imagePath) {
            _this.solicitacao.Base64Files.push(imagePath);
        }, function (err) {
            console.log(err);
            // this.presentToast('Erro ao selecionar a imagem');
        });
    };
    ParticipacaoEnviarParticipacaoPage.prototype.send = function () {
        var _this = this;
        if (this.solicitacao.Texto == '') {
            this.ErroTexto = true;
        }
        else {
            var loading_2 = this.loadingCtrl.create({
                content: 'Carregando...'
            });
            loading_2.present();
            this.participacao.post(this.solicitacao).then(function (result) {
                loading_2.dismiss();
                _this.utils.showModalSucesso(result.Titulo, result.Mensagem, "OK", function (data) { _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__participacao__["a" /* ParticipacaoPage */]); });
            }, function (error) {
                loading_2.dismiss();
                _this.alertCtrl.create({
                    title: '<img src="assets/icon/error.png"/> Erro',
                    subTitle: 'Ocorreu um erro, tente mais tarde.',
                    buttons: [{ text: 'Ok' }],
                    cssClass: 'alertcss'
                }).present();
                console.log(error);
            });
        }
    };
    ParticipacaoEnviarParticipacaoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-participacao-enviar-participacao',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/participacao/participacao-enviar-participacao/participacao-enviar-participacao.html"*/'<ion-header color="header">\n  <style>\n  .list-md .item-input:last-child{\n    border-bottom: 0!important;\n  }\n  .card-md .item-md.item-block .item-inner {\n  border-bottom: 1px solid #fff;\n}\n.item-input ion-textarea{\n  border: 1px solid #dedede;\n  margin-bottom: 10%;\n  padding: 5%;\n}\n.alert-title{\n  margin-left: 0!important;\n}\n  </style>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Enviar Participação</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <!-- <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher> -->\n  <section [ngSwitch]="tab">\n      <div *ngSwitchCase="\'local\'">\n        <div class="container-stage">\n          <div class="number">1</div>\n          <div class="text">\n            <strong>Selecione o local</strong>\n          </div>\n        </div>\n        <ion-card>\n\n          <ion-card-content>\n              <button ion-button round full icon-left outline (click)="getLocation()" style="border-right-width: 1px;\n              border-left-width: 1px;"> <ion-icon name="pin" ></ion-icon> Selecionar minha localização</button>\n              <ion-row>\n                <ion-col col-5>\n                  <ion-item>\n                    <ion-label stacked>Estado</ion-label>\n                    <ion-select [(ngModel)]="solicitacao.Estado" multiple="false" (ionChange)="onSelectCidade($event)"  [selectOptions]="selectOptEstados" (ionChange)="validaCampos()">\n                      <ion-option *ngFor="let estado of estados" [value]="estado.sigla" >{{estado.nome}}</ion-option>\n                    </ion-select>\n                  </ion-item>\n                  <ion-label class="error" *ngIf="erroEstado">Selecione um Estado</ion-label>\n                </ion-col>\n                <ion-col col-7>\n                  <ion-item>\n                    <ion-label stacked>Cidade</ion-label>\n                    <ion-select [(ngModel)]="solicitacao.Cidade" [selectOptions]="selectOptCidades" (ionChange)="validaCampos()">\n                      <ion-option *ngFor="let cidade of cidades" value="{{cidade}}" [attr.selected]="(solicitacao.Cidade == cidade) ? true : null" >{{cidade}}</ion-option>\n                    </ion-select>\n                  </ion-item>\n                  <ion-label class="error" *ngIf="erroCidade">Selecione uma Cidade</ion-label>\n                </ion-col>\n                <ion-col col-12>\n                  <ion-item>\n                    <ion-label stacked text-uppercase>Rua</ion-label>\n                    <ion-input type="text" [(ngModel)]="solicitacao.Rua" placeholder="Digite o nome da sua rua" (keyup)="validaCampos()"></ion-input>\n                  </ion-item>\n                  <ion-label class="error" *ngIf="erroRua">Campo Rua obrigatório</ion-label>\n                </ion-col>\n                <ion-col col-5>\n                  <ion-item>\n                    <ion-label stacked text-uppercase>Número</ion-label>\n                    <ion-input type="number" [(ngModel)]="solicitacao.Numero" (keyup)="validaCampos()"></ion-input>\n                  </ion-item>\n                  <ion-label class="error" *ngIf="erroNumero">Campo Número obrigatório</ion-label>\n                </ion-col>\n                <ion-col col-7>\n                  <ion-item>\n                    <ion-label stacked text-uppercase>Bairro</ion-label>\n                    <ion-input type="text" [(ngModel)]="solicitacao.Bairro" (keyup)="validaCampos()"></ion-input>\n                  </ion-item>\n                  <ion-label class="error" *ngIf="erroBairro">Campo Bairro obrigatório</ion-label>\n                </ion-col>\n                <ion-col col-12 text-center margin-top>\n                    <button ion-button round full (click)="saveLocal()" [style.opacity]="erroPart1"> Continuar</button>\n                </ion-col>\n              </ion-row>\n          </ion-card-content>\n\n        </ion-card>\n      </div>\n      <div *ngSwitchCase="\'categoria\'">\n        <div class="container-stage">\n          <div class="number">2</div>\n          <div class="text">\n            <strong>Selecione a Categoria</strong>\n          </div>\n        </div>\n        <ul class="list-categories">\n          <li (click)="choiceCategorie(categ)" *ngFor="let categ of categorias">\n            <img [src]="categ.IconeImagem" *ngIf="categ.IconeImagem" >\n            <p>{{categ.Nome}}</p>\n          </li>\n        \n        </ul>\n      </div>\n      <div *ngSwitchCase="\'tipo\'">\n        <div class="container-stage">\n          <div class="number">3</div>\n          <div class="text">\n            <strong>Selecione o Tipo</strong>\n          </div>\n        </div>\n        <ion-list>\n          <ion-card (click)="saveTipe(filha.Id)" *ngFor="let filha of categoriasfilhas">\n            <ion-card-content class="flex-card">\n              <div>\n                <ion-card-title>{{filha.Nome}}</ion-card-title>\n              </div>\n              <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n            </ion-card-content>\n          </ion-card>\n        </ion-list>\n      </div>\n      <div *ngSwitchCase="\'descricao\'">\n        <div class="container-stage">\n          <div class="number" *ngIf="categoriasfilhas.length != 0">4</div>\n          <div class="number" *ngIf="categoriasfilhas.length == 0">3</div>\n          <div class="text">\n            <strong>Descreva sua Participação</strong>\n          </div>\n        </div>\n        <ion-card>\n          <ion-card-content>\n            <ion-list>\n              <ion-item >\n                <ion-label stacked text-uppercase>Descrição</ion-label>\n                <ion-textarea [(ngModel)]="solicitacao.Texto" (keyup)="validaCampos2()"></ion-textarea>\n              </ion-item>\n              <ion-label class="error" *ngIf="ErroTexto">Campo Descrição obrigatório</ion-label>\n            </ion-list>\n            <ion-buttons end>\n              <button ion-button icon-left full outline margin-top (click)= "abrirGaleria()" style="border-right-width: 1px;\n              border-left-width: 1px;">\n                <ion-icon name="image"></ion-icon>\n                Escolher uma imagem da galeria\n              </button>\n              <button ion-button icon-left full outline margin-top color="basegreen" (click)= "abrirCamera()" style="border-right-width: 1px;\n              border-left-width: 1px;">\n                <ion-icon name="camera"></ion-icon>\n                Fotograr\n              </button>\n            </ion-buttons>\n            <div margin-top>\n              <button ion-button round full (click)="send()" [style.opacity]="erroParteFinal"> Salvar</button>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </div>\n  </section>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/participacao/participacao-enviar-participacao/participacao-enviar-participacao.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_diagnostic__["a" /* Diagnostic */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_image_picker__["a" /* ImagePicker */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_4__providers_participacao_participacao__["a" /* ParticipacaoProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_image_picker__["a" /* ImagePicker */]])
    ], ParticipacaoEnviarParticipacaoPage);
    return ParticipacaoEnviarParticipacaoPage;
}());

//# sourceMappingURL=participacao-enviar-participacao.js.map

/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_PersonDetail__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_util_events__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, person, alertCtrl, utils, loadingCtrl, platform, actionSheetCtrl, camera, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.person = person;
        this.alertCtrl = alertCtrl;
        this.utils = utils;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.events = events;
        this.estados = new Array();
        this.tab = "meus-dados";
        this.selectOptEstados = {
            title: 'Selecione o estado',
            subTitle: 'Selecione o estado',
            checked: true
        };
        this.selectOptCidades = {
            title: 'Selecione a cidade',
            subTitle: 'Selecione a cidade',
            checked: true
        };
        this.user = new __WEBPACK_IMPORTED_MODULE_3__models_PersonDetail__["a" /* PersonDetail */]();
        this.getEstados();
        this.getPersonDetail();
        if (this.user.Estado != '' && this.user.Estado != null) {
            this.selectOptEstados.checked = false;
        }
    }
    PerfilPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad PerfilPage');
    };
    PerfilPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getPersonDetail();
            refresher.complete();
        }, 1000);
    };
    PerfilPage.prototype.getPersonDetail = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.person.get()
            .then(function (response) {
            loading.dismiss();
            _this.user = new __WEBPACK_IMPORTED_MODULE_3__models_PersonDetail__["a" /* PersonDetail */]().fromJSON(response);
            if (_this.user.Estado != '' && _this.user.Estado != null) {
                _this.onSelectCidade(_this.user.Estado);
            }
        }, function (error) {
            loading.dismiss();
            console.log(error);
            _this.utils.showAlert('ocorreu um erro. Tente novamente mais tarde.');
        });
    };
    PerfilPage.prototype.onSelectCidade = function (uf) {
        var _this = this;
        this.utils.getCidades(uf).then(function (result) { _this.cidades = result; }, function (error) { });
        if (this.user.Cidade != '' && this.user.Cidade != null) {
            this.selectOptCidades.checked = false;
        }
    };
    PerfilPage.prototype.getEstados = function () {
        var _this = this;
        this.utils.getEstados().then(function (result) { _this.estados = result; }, function (error) { });
    };
    PerfilPage.prototype.putPerson = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Salvando...'
        });
        loading.present();
        this.person.put(this.user)
            .then(function (response) {
            // loading.dismiss();
            // setTimeout(() => {
            loading.dismiss();
            console.log(response);
            _this.person.get()
                .then(function (response) {
                _this.events.publish('profile', response);
            });
            _this.utils.showModalSucesso(response.Titulo, response.Mensagem, "OK", null);
            // }, 1000);
            // setTimeout(() => {
            //   this.navCtrl.setRoot(HomePage);
            //           }, 3000);
        }, function (error) {
            loading.dismiss();
            console.log(error);
            _this.utils.showAlert(error.error.Message);
        });
    };
    PerfilPage.prototype.putFoto = function (image) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Salvando...'
        });
        loading.present();
        this.person.putFoto(image)
            .then(function (response) {
            // loading.dismiss();
            _this.user.CaminhoFoto = response.Url;
            _this.person.get()
                .then(function (profile) {
                loading.dismiss();
                _this.events.publish('profile', profile);
                _this.utils.showModalSucesso(response.Titulo, response.Mensagem, "OK", null);
            });
        }, function (error) {
            loading.dismiss();
            console.log(error);
            // this.utils.showAlert(error.error.Message);
        });
    };
    PerfilPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Escolha uma imagem',
            buttons: [
                {
                    text: 'Abrir Galeria',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Usar a Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    PerfilPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 75,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
        };
        this.camera.getPicture(options).then(function (imagePath) {
            _this.putFoto(imagePath);
        }, function (err) {
            // this.presentToast('Erro ao selecionar a imagem');
        });
    };
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-perfil',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/perfil/perfil.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Meu Perfil</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <ion-segment [(ngModel)]="tab" color="primary" class="wrap">\n    <ion-segment-button value="meus-dados">Meus <br/> Dados</ion-segment-button>\n    <ion-segment-button value="endereco">Endereço</ion-segment-button>\n    <ion-segment-button value="dados-sociais">Dados Sociais</ion-segment-button>\n    <!-- <ion-segment-button value="minhas-preferencias">Minhas Preferências</ion-segment-button> -->\n  </ion-segment>\n  <div [ngSwitch]="tab" padding>\n    <div *ngSwitchCase="\'meus-dados\'">\n      <ion-row>\n        <ion-col col-4>\n          <div class="avatar-change" (click)="presentActionSheet()">\n            <div *ngIf="user.CaminhoFoto != \'\'" [style.backgroundImage]="\'url(\' + user.CaminhoFoto + \')\'" class="avatar"> </div>\n            <div *ngIf="user.CaminhoFoto == \'\'" [style.backgroundImage]="\'url(http://mypolis.com.br/images/avatar-list.png)\'" class="avatar"> </div>\n            <span>Trocar</span>\n          </div>\n        </ion-col>\n        <ion-col col-8>\n          <ion-item>\n            <ion-label stacked text-uppercase>Nome</ion-label>\n            <ion-input type="text" value="{{user.Nome}}" [(ngModel)]="user.Nome"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>E-mail</ion-label>\n            <ion-input type="text" value="{{user.Email}}"[(ngModel)]="user.Email"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Celular</ion-label>\n            <ion-input type="tel" [brmasker]="{phone: true}" value="{{user.Celular}}" [(ngModel)]="user.Celular"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Telefone Fixo</ion-label>\n            <ion-input type="tel" [brmasker]="{phone: true}" value="{{user.Telefone}}"[(ngModel)]="user.Telefone"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked>Sexo</ion-label>\n            <ion-select [(ngModel)]="user.Sexo" name="Sexo" multiple="false" >\n              <ion-option value="1">Masculino</ion-option>\n              <ion-option value="2">Feminino</ion-option>\n              <ion-option value="0">Outro</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>RG</ion-label>\n            <ion-input type="tel" [brmasker]="{mask:\'00.000.000-0\', len:12}" value="{{user.RG}}"[(ngModel)]="user.RG"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>CPF</ion-label>\n            <ion-input type="tel" [brmasker]="{person: true}" value="{{user.CPF}}"[(ngModel)]="user.CPF"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked>Estado Civil</ion-label>\n            <ion-select [(ngModel)]="user.EstadoCivil" name="EstadoCivil" multiple="false">\n              <ion-option value="1" selected>Solteiro</ion-option>\n              <ion-option value="2">Casado</ion-option>\n              <ion-option value="3">Separado</ion-option>\n              <ion-option value="4">Divorciado</ion-option>\n              <ion-option value="5">Viúvo</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Senha</ion-label>\n            <ion-input type="password" [(ngModel)]="user.Senha"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Confirme sua senha</ion-label>\n            <ion-input type="password" [(ngModel)]="user.ConfirmarSenha"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div *ngSwitchCase="\'endereco\'">\n      <ion-row>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked>Estado</ion-label>\n            <ion-select [(ngModel)]="user.Estado" multiple="false" (ionChange)="onSelectCidade($event)"  [selectOptions]="selectOptEstados">\n              <ion-option *ngFor="let estado of estados" [value]="estado.sigla">{{estado.nome}}</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked>Cidade</ion-label>\n            <ion-select [(ngModel)]="user.Cidade" [selectOptions]="selectOptCidades">\n              <ion-option *ngFor="let cidade of cidades" value="{{cidade}}" [attr.selected]="(user.Cidade == cidade) ? true : null">{{cidade}}</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Rua</ion-label>\n            <ion-input type="text" value="{{user.Rua}}"[(ngModel)]="user.Rua"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Número</ion-label>\n            <ion-input type="tel" value="{{user.Numero}}"[(ngModel)]="user.Numero"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Complemento</ion-label>\n            <ion-input type="text" value="{{user.Complemento}}"[(ngModel)]="user.Complemento"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>CEP</ion-label>\n            <ion-input type="tel" [brmasker]="{mask:\'00.000-000\', len:10}" value="{{user.CEP}}" [(ngModel)]="user.CEP"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Bairro</ion-label>\n            <ion-input type="text" value="{{user.Bairro}}"[(ngModel)]="user.Bairro"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div *ngSwitchCase="\'dados-sociais\'">\n      <ion-row>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Facebook</ion-label>\n            <ion-input type="text" value="{{user.Facebook}}" [(ngModel)]="user.Facebook"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Instagram</ion-label>\n            <ion-input type="text" value="{{user.Instagram}}" [(ngModel)]="user.Instagram"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Twitter</ion-label>\n            <ion-input type="text" value="{{user.Twitter}}" [(ngModel)]="user.Twitter"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n    <!-- <div *ngSwitchCase="\'minhas-preferencias\'">\n      <ion-row>\n        <ul class="list-default">\n          <li>\n            <div>\n              <ion-icon name="body" item-start></ion-icon>Política\n            </div>\n            <ion-icon name="heart" class="heart select" item-end></ion-icon>\n          </li>\n          <li>\n            <div><ion-icon name="videocam" item-start></ion-icon>Cinema</div>\n            <ion-icon name="heart" class="heart" item-end></ion-icon>\n          </li>\n          <li>\n            <div><ion-icon name="person" item-start></ion-icon>Interação</div>\n            <ion-icon name="heart" class="heart" item-end></ion-icon>\n          </li>\n          <li>\n            <div><ion-icon name="list-box" item-start></ion-icon>Educação</div>\n            <ion-icon name="heart" class="heart" item-end></ion-icon>\n          </li>\n        </ul>\n      </ion-row>\n    </div> -->\n    <footer margin-top>\n      <button ion-button full text-uppercase (click)="putPerson()">Salvar</button>\n    </footer>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/perfil/perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular_util_events__["a" /* Events */]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonDetail; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__IModel__ = __webpack_require__(346);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PersonDetail = /** @class */ (function (_super) {
    __extends(PersonDetail, _super);
    function PersonDetail() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PersonDetail = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], PersonDetail);
    return PersonDetail;
}(__WEBPACK_IMPORTED_MODULE_1__IModel__["a" /* IModel */]));

//# sourceMappingURL=PersonDetail.js.map

/***/ }),

/***/ 346:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IModel; });
var IModel = /** @class */ (function () {
    function IModel() {
    }
    IModel.prototype.fromJSON = function (json) {
        for (var propName in json)
            this[propName] = json[propName];
        return this;
    };
    return IModel;
}());

//# sourceMappingURL=IModel.js.map

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SearchbarComponent = /** @class */ (function () {
    function SearchbarComponent(navCtrl) {
        this.navCtrl = navCtrl;
        this.searchQuery = '';
        this.initializeItems();
    }
    SearchbarComponent.prototype.initializeItems = function () {
        this.items = [
            'Amsterdam',
            'Bogota'
        ];
    };
    SearchbarComponent.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    SearchbarComponent.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    SearchbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'searchbar',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/components/searchbar/searchbar.html"*/'<ion-searchbar placeholder="Buscar" (ionInput)="getItems($event)" (ionCancel)="onCancel($event)"></ion-searchbar>\n<strong class="title">Sugestões:</strong>\n<ion-list>\n  <ion-item *ngFor="let item of items">\n    {{ item }}\n  </ion-item>\n</ion-list>\n<div>\n  <button ion-button clear icon-left (click)="backPage()"> <ion-icon name="ios-arrow-back"></ion-icon> Voltar</button>\n</div>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/components/searchbar/searchbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]])
    ], SearchbarComponent);
    return SearchbarComponent;
}());

//# sourceMappingURL=searchbar.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_comites_comites__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_voluntario_voluntario__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_notificacoes_notificacoes__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_participacao_participacao__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_pesquisa_pesquisa__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_programa_governo_programa_governo__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_material_institucional_material_institucional__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_notificacoes_notificacoes_notificacoes_detalhe_notificacoes_notificacoes_detalhe__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_programa_pontos_programa_pontos__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_tutorial_tutorial__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_local_notifications__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_push__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_login_login__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__angular_http__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_push_push__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_app_availability__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_in_app_browser__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_storage__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





























var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, auth, person, localNotifications, pushProvider, utils, push, notification, loadingCtrl, appAvailability, iab, alertCtrl, events, http, storage, network) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth = auth;
        this.person = person;
        this.localNotifications = localNotifications;
        this.pushProvider = pushProvider;
        this.utils = utils;
        this.push = push;
        this.notification = notification;
        this.loadingCtrl = loadingCtrl;
        this.appAvailability = appAvailability;
        this.iab = iab;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.http = http;
        this.storage = storage;
        this.network = network;
        this.disconnect = false;
        this.NotificacoesNotificacoesDetalhePage = __WEBPACK_IMPORTED_MODULE_13__pages_notificacoes_notificacoes_notificacoes_detalhe_notificacoes_notificacoes_detalhe__["a" /* NotificacoesNotificacoesDetalhePage */];
        this.IsAnonimo = this.storage.get('profileName');
        this.pages = [
            { title: 'Início', component: __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */], icon: 'home', description: 'Notícias, Eventos, Agenda...', cssClass: 'iniciocss', parameter: 0 },
            { title: 'Notificações', component: __WEBPACK_IMPORTED_MODULE_7__pages_notificacoes_notificacoes__["a" /* NotificacoesPage */], icon: 'notifications', description: null, notificationsNumber: true, parameter: 0 },
            { title: 'Programa de Governo', component: __WEBPACK_IMPORTED_MODULE_10__pages_programa_governo_programa_governo__["a" /* ProgramaGovernoPage */], icon: 'list', description: null, parameter: 0 },
            { title: 'Seja Voluntário', component: __WEBPACK_IMPORTED_MODULE_6__pages_voluntario_voluntario__["a" /* VoluntarioPage */], icon: 'hand', description: null, parameter: 0 },
            { title: 'Minha Participação', component: __WEBPACK_IMPORTED_MODULE_8__pages_participacao_participacao__["a" /* ParticipacaoPage */], icon: 'person', description: null, parameter: 0 },
            { title: 'Pesquisa de Opinião', component: __WEBPACK_IMPORTED_MODULE_9__pages_pesquisa_pesquisa__["b" /* PesquisaPage */], icon: 'text', description: null, parameter: 0 },
            { title: 'Programa de Pontos', component: __WEBPACK_IMPORTED_MODULE_14__pages_programa_pontos_programa_pontos__["a" /* ProgramaPontosPage */], icon: 'star', description: 'Como funciona, histórico de pontos...', cssClass: 'programacss', parameter: 0 },
            { title: 'Material de Campanha', component: __WEBPACK_IMPORTED_MODULE_12__pages_material_institucional_material_institucional__["a" /* MaterialInstitucionalPage */], icon: 'images', description: 'Posts, santinhos, banners…', cssClass: 'materialcss', parameter: 0 },
            { title: 'Comitês', component: __WEBPACK_IMPORTED_MODULE_5__pages_comites_comites__["a" /* ComitesPage */], icon: 'people', description: null, parameter: 0 }
        ];
        console.log(this.IsAnonimo, 'nome do individuo');
        this.storage.get('profileName').then(function (val) {
            console.log('Your age is', val);
            _this.IsAnonimo = val;
        });
        events.subscribe('profile:count', function (profile, count) {
            _this.storage.get('profileName').then(function (val) {
                console.log('Your age is', val);
                _this.IsAnonimo = val;
                if (_this.IsAnonimo == 'Anônimo') {
                    _this.NoLogin = true;
                }
                else {
                    _this.NoLogin = false;
                    _this.profile = profile;
                }
            });
        });
        events.subscribe('profile', function (profile) {
            _this.storage.get('profileName').then(function (val) {
                console.log('Your age is', val);
                _this.IsAnonimo = val;
                if (_this.IsAnonimo == 'Anônimo') {
                    _this.NoLogin = true;
                }
                else {
                    _this.NoLogin = false;
                    _this.profile = profile;
                }
            });
        });
        events.subscribe('count', function (count) {
            _this.storage.get('profileName').then(function (val) {
                console.log('Your age is', val);
                _this.IsAnonimo = val;
                if (_this.IsAnonimo == 'Anônimo') {
                    _this.NoLogin = true;
                }
                else {
                    _this.NoLogin = false;
                    _this.count = count;
                }
            });
        });
        events.subscribe('menu:closed', function () {
            // your action here
        });
        events.subscribe('clickLogin', function () {
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_18__pages_login_login__["a" /* LoginPage */]);
        });
        events.subscribe('NotificacoesLocal', function (notificacaoId) {
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_13__pages_notificacoes_notificacoes_notificacoes_detalhe_notificacoes_notificacoes_detalhe__["a" /* NotificacoesNotificacoesDetalhePage */], notificacaoId);
        });
        this.initializeApp();
    }
    MyApp.prototype.onDisconnect = function () {
        console.log('Sem conexão com internet. O conteúdo mais recente não poderá ser exibido. :-(');
        this.alertCtrl.create({
            title: '',
            subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
            buttons: ['OK'],
            cssClass: 'alertcss'
        }).present();
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            _this.disconnect = true;
            console.log(_this.disconnect + 'caralho não vai passar aqui não');
            _this.onDisconnect();
        });
        console.log('iniciando app');
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            if (_this.auth.isLogged()) {
                console.log('iniciando app logado');
                if (_this.IsAnonimo == 'Anônimo') {
                    _this.NoLogin = true;
                }
                else {
                    _this.NoLogin = false;
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */];
                    _this.pushProvider.initPush();
                    var options = {
                        android: {
                            senderID: '318490138469'
                        },
                        ios: {
                            alert: 'true',
                            badge: false,
                            sound: 'true'
                        },
                        windows: {}
                    };
                    var pushObject = _this.push.init(options);
                    pushObject.on('notification').subscribe(function (notificacao) {
                        console.log('message -> ' + notificacao);
                        [];
                        if (notificacao.additionalData.foreground == true) {
                            _this.localNotifications.schedule({
                                title: 'Voluntários da Pátria',
                                text: notificacao.message,
                                icon: 'ic_notifications',
                                smallIcon: 'ic_notification_small',
                                id: notificacao.additionalData.notificacaoId,
                            });
                            // this.localNotifications.on("click", (notification, state) => {
                            //   this.nav.setRoot(NotificacoesNotificacoesDetalhePage, notification.id);
                            // })
                        }
                        else if (notificacao.additionalData.foreground == false) {
                            console.log(notificacao, 'LoginNotificacaoCOMPONENTS');
                            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_13__pages_notificacoes_notificacoes_notificacoes_detalhe_notificacoes_notificacoes_detalhe__["a" /* NotificacoesNotificacoesDetalhePage */], notificacao.additionalData.notificacaoId);
                        }
                    });
                    pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin' + error); });
                }
            }
            else {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_15__pages_tutorial_tutorial__["a" /* TutorialPage */];
            }
        });
    };
    MyApp.prototype.openPage = function (page) {
        switch (page.component.name) {
            case "NotificacoesPage":
                if (this.IsAnonimo == 'Anônimo') {
                    this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
                }
                else {
                    this.nav.setRoot(page.component, page.parameter);
                }
                break;
            case "VoluntarioPage":
                if (this.IsAnonimo == 'Anônimo') {
                    this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
                }
                else {
                    this.nav.setRoot(page.component, page.parameter);
                }
                break;
            case "ParticipacaoPage":
                if (this.IsAnonimo == 'Anônimo') {
                    this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
                }
                else {
                    this.nav.setRoot(page.component, page.parameter);
                }
                break;
            case "PesquisaPage":
                if (this.IsAnonimo == 'Anônimo') {
                    this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
                }
                else {
                    this.nav.setRoot(page.component, page.parameter);
                }
                break;
            default:
                this.nav.setRoot(page.component, page.parameter);
                break;
        }
    };
    MyApp.prototype.openPageHistorico = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_14__pages_programa_pontos_programa_pontos__["a" /* ProgramaPontosPage */], 'historico');
    };
    MyApp.prototype.openinnap = function () {
        window.open('https://maisquevoto.com.br/psl', '_blank', 'location=no');
    };
    MyApp.prototype.openBrowser = function () {
        window.open('https://maisquevoto.com.br/psl', '_blank', 'location=no');
    };
    MyApp.prototype.openPagePush = function (page, params) {
        this.nav.setRoot(page, params);
    };
    MyApp.prototype.logoutApp = function () {
        this.auth.logoutUser();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_18__pages_login_login__["a" /* LoginPage */]);
    };
    MyApp.prototype.getNotificationsCount = function () {
        var _this = this;
        setTimeout(function () {
            _this.notification.Count().then(function (response) {
                _this.count = response;
                _this.events.publish('count', _this.count);
                console.log("sync notification: " + response);
                _this.getNotificationsCount();
            });
        }, 30000);
    };
    MyApp.prototype.CheckForApp = function (app) {
        // let app;
        console.log(app);
        if (this.platform.is('ios')) {
            if (app == 'facebook') {
                this.fbCheck('fb://', 'fb://profile/280501375845086');
            }
            else if (app == 'instagram') {
                this.instaCheck('instagram://');
            }
            else if (app == 'twitter') {
                this.twitterCheck('twitter://');
            }
            else if (app == 'youtube') {
                this.youtubeCheck('youtube://');
            }
        }
        else if (this.platform.is('android')) {
            if (app == 'facebook') {
                this.fbCheck('com.facebook.katana', 'fb://page/280501375845086');
            }
            else if (app == 'instagram') {
                this.instaCheck('com.instagram.android');
            }
            else if (app == 'twitter') {
                this.twitterCheck('com.twitter.android');
            }
            else if (app == 'youtube') {
                this.youtubeCheck('com.youtube');
            }
        }
    };
    MyApp.prototype.fbCheck = function (facebook, urlscheme) {
        this.appAvailability.check(facebook)
            .then(function (yes) {
            console.log(facebook + ' is available');
            window.open(urlscheme, '_system', 'location=no');
        }, function (no) {
            console.log(facebook + ' is NOT available');
            window.open('https://www.facebook.com/PartidoSocialLiberalBR', '_system', 'location=no');
            console.log("náo tem instalado");
        });
    };
    MyApp.prototype.instaCheck = function (instagram) {
        var _this = this;
        this.appAvailability.check(instagram)
            .then(function (yes) {
            console.log(instagram + ' is available');
            _this.appExistStatus = instagram + 'is available';
            window.open('instagram://user?username=PSL_Nacional', '_system', 'location=no');
        }, function (no) {
            console.log(instagram + ' is NOT available');
            window.open('https://www.instagram.com/PSL_Nacional', '_system', 'location=no');
        });
    };
    MyApp.prototype.youtubeCheck = function (youtube) {
        var _this = this;
        this.appAvailability.check(youtube)
            .then(function (yes) {
            console.log(youtube + ' is available');
            _this.appExistStatus = youtube + 'is available';
            window.open('youtube://UCj6-AU8CsA4VBR4_sQn3Hug', '_system', 'location=no');
        }, function (no) {
            console.log(youtube + ' is NOT available');
            window.open('https://www.youtube.com/channel/UCj6-AU8CsA4VBR4_sQn3Hug', '_system', 'location=no');
        });
    };
    MyApp.prototype.twitterCheck = function (twitter) {
        var _this = this;
        this.appAvailability.check(this.app)
            .then(function (yes) {
            console.log(twitter + ' is available');
            _this.appExistStatus = _this.app + 'is available';
            {
                window.open('twitter://user?screen_name=PSL_Nacional', '_system', 'location=no');
            }
        }, function (no) {
            console.log("veio no else do availabity");
            window.open('https://twitter.com/PSL_Nacional', '_system', 'location=no');
            console.log("náo tem instalado");
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]) === "function" && _a || Object)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header color="header">\n    <ion-toolbar class="menu-header">\n      <div class="container-profile" [hidden]="NoLogin">\n        <div *ngIf="profile && profile.CaminhoFoto != \'\'" [style.backgroundImage]="\'url(\' + profile.CaminhoFoto + \')\'" class="avatar"> </div>\n          <div *ngIf="profile && profile.CaminhoFoto == \'\'" [style.backgroundImage]="\'url(http://mypolis.com.br/images/avatar-list.png)\'" class="avatar"> </div>\n          <div class="info" style="color:#0276a9!important">\n            <ion-title>\n              <p *ngIf="profile" style="color:#0276a9!important; margin:0; width: 100%!important;">{{profile.Nome}}</p>\n              <nav>\n                <a menuClose (click)="openPagePush(\'perfil\')">Meu Perfil</a>\n                <a menuClose (click)="logoutApp()" class="logout">Sair</a>\n              </nav>\n            </ion-title>\n            \n        </div>\n      </div>\n      <div class="container-profile" *ngIf="NoLogin">\n\n        <div class="info" style="color:#0276a9!important;\n        margin: 0 auto;\n       display: block;" >\n          <ion-title>\n            <p style="color:#0276a9!important; margin:0; ">\n              <!-- Ainda não se cadastrou? -->\n              <img src="assets/imgs/logoVoluntarios.png" class="logoVoluntariosMenu" alt="">\n            </p>\n          </ion-title>\n        </div>\n      </div>\n    </ion-toolbar>\n\n  </ion-header>\n\n  <ion-content>\n    <style>\n      .list-ios>.item-block {\n        border-radius: 0 !important;\n        margin-bottom: 0 !important;\n      }\n      /* .iniciocss .pzin{\n\n    margin-left: -14%!important;\n      } */\n\n      .item-ios {\n        padding-left: 0;\n      }\n      /* ion-icon{\n        font-size: 1.5em;\n      } */\n      .quero-contribuir{\n        background:#ecc222;\n        opacity: 190;\n      }\n      .quero-contribuir .item-inner{\n        background-image:none!important;\n      }\n      .quero-contribuir p{\n        color: #fff!important;\n      }\n      .badge-md-colorblue{\n        background-color: #f0ce42;\n      }\n      .toolbar-title-md{\ncolor:#0276a9!important;\n/* width: 100%!important; */\n      }\n      .toolbar-title-ios{\n        color:#0276a9!important;\n        /* width: 100%!important; */\n      }\n      .badge-ios-colorblue{\n        background-color: #f0ce42;\n      }\n    </style>\n    <div style="background: #f9f9f9; text-align: center; padding: 11px 0;" *ngIf="NoLogin" >\n        <button menuClose ion-button class="ButtonEfetueLogin" (click)="openPagePush(\'login\')" >\n            EFETUE SEU LOGIN\n          </button>\n    </div>\n    <div class="points" padding  style="background:#f9f9f9;" [hidden]="NoLogin">\n     \n      <img *ngIf="profile && profile.Medalha" src="{{profile.Medalha.ImagemUrl}}" class="medalha">\n      <div menuClose class="info" (click)="openPageHistorico()">\n        <strong *ngIf="profile">{{profile.TotalPontos}} pontos</strong>\n        <small *ngIf="profile && profile.Medalha" text-uppercase>{{profile.Medalha.Nome}}</small>\n      </div>\n    </div>\n    <ion-list>\n        <button menuClose ion-item  (click)="openinnap()" class="quero-contribuir">\n            <ion-icon name="logo-usd" style="float: left;"></ion-icon> <p style="float: left;color:#333;\n            ">Quero Contribuir </p><br>\n            <p  class="pzin" style="float: left;\n            margin-left: 10.5%;"><small style="    padding-left: 0;\n            margin: 0;" ></small></p>\n          </button>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" [ngClass]="p.cssClass ? p.cssClass : \'\'" style="background: #fff;\n      color: #333;">\n        <ion-icon name="{{p.icon}}" style="float: left;"></ion-icon> <p style="float: left;color:#333;\n        ">{{p.title}}</p><br>\n        <p  class="pzin" style="float: left;\n        margin-left: 10.5%;"><small style="    padding-left: 0;\n        margin: 0;" >{{p.description}}</small></p>\n        <ion-badge color="colorblue" *ngIf="count > 0 && p.notificationsNumber" item-end>{{count}}</ion-badge>\n      </button>\n    </ion-list>\n  </ion-content>\n\n  <ion-footer class="socials">\n    <ion-toolbar color="white">\n      <div class="flex">\n        <nav>\n          <div href="#" target="_blank" (click)="CheckForApp(\'facebook\')">\n            <img src="assets/imgs/ico-facebook.svg">\n          </div>\n          <div href="#" target="_blank" (click)="CheckForApp(\'twitter\')">\n            <img src="assets/imgs/ico-twitter.svg">\n          </div>\n          <div href="#" target="_blank" (click)="CheckForApp(\'youtube\')">\n            <img src="assets/imgs/ico-youtube.svg">\n          </div>\n          <div href="#" target="_blank" (click)="CheckForApp(\'instagram\')">\n            <img src="assets/imgs/ico-instagram.svg">\n          </div>\n        </nav>\n        <div href="#" class="signature" (click)="openinnap()" target="_blank" style="    width: 34%;">\n          <img src="assets/imgs/logo-black.png">\n        </div>\n      </div>\n    </ion-toolbar>\n  </ion-footer>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_20__providers_auth_auth__["a" /* AuthProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_20__providers_auth_auth__["a" /* AuthProvider */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_21__providers_person_person__["a" /* PersonProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_21__providers_person_person__["a" /* PersonProvider */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_16__ionic_native_local_notifications__["a" /* LocalNotifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_16__ionic_native_local_notifications__["a" /* LocalNotifications */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_22__providers_push_push__["a" /* PushProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_22__providers_push_push__["a" /* PushProvider */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_24__providers_utils_utils__["a" /* UtilsProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_24__providers_utils_utils__["a" /* UtilsProvider */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_17__ionic_native_push__["a" /* Push */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_17__ionic_native_push__["a" /* Push */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_23__providers_notificacao_notificacao__["a" /* NotificationProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_23__providers_notificacao_notificacao__["a" /* NotificationProvider */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_25__ionic_native_app_availability__["a" /* AppAvailability */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_25__ionic_native_app_availability__["a" /* AppAvailability */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_26__ionic_native_in_app_browser__["a" /* InAppBrowser */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_26__ionic_native_in_app_browser__["a" /* InAppBrowser */]) === "function" && _p || Object, typeof (_q = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]) === "function" && _q || Object, typeof (_r = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]) === "function" && _r || Object, typeof (_s = typeof __WEBPACK_IMPORTED_MODULE_19__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_19__angular_http__["b" /* Http */]) === "function" && _s || Object, typeof (_t = typeof __WEBPACK_IMPORTED_MODULE_27__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_27__ionic_storage__["b" /* Storage */]) === "function" && _t || Object, typeof (_u = typeof __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__["a" /* Network */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__["a" /* Network */]) === "function" && _u || Object])
    ], MyApp);
    return MyApp;
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoluntarioPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__voluntario__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__indique_amigos__ = __webpack_require__(270);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var VoluntarioPageModule = /** @class */ (function () {
    function VoluntarioPageModule() {
    }
    VoluntarioPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__voluntario__["a" /* VoluntarioPage */],
                __WEBPACK_IMPORTED_MODULE_3__indique_amigos__["a" /* IndiqueAmigosPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__voluntario__["a" /* VoluntarioPage */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__indique_amigos__["a" /* IndiqueAmigosPage */])
            ],
        })
    ], VoluntarioPageModule);
    return VoluntarioPageModule;
}());

//# sourceMappingURL=voluntario.module.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParticipacaoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__participacao__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__participacao_enviar_participacao_participacao_enviar_participacao_module__ = __webpack_require__(220);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ParticipacaoPageModule = /** @class */ (function () {
    function ParticipacaoPageModule() {
    }
    ParticipacaoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__participacao__["a" /* ParticipacaoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__participacao_enviar_participacao_participacao_enviar_participacao_module__["ParticipacaoEnviarParticipacaoPageModule"],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__participacao__["a" /* ParticipacaoPage */]),
            ],
        })
    ], ParticipacaoPageModule);
    return ParticipacaoPageModule;
}());

//# sourceMappingURL=participacao.module.js.map

/***/ }),

/***/ 368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutorialPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tutorial__ = __webpack_require__(269);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TutorialPageModule = /** @class */ (function () {
    function TutorialPageModule() {
    }
    TutorialPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tutorial__["a" /* TutorialPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tutorial__["a" /* TutorialPage */]),
            ],
        })
    ], TutorialPageModule);
    return TutorialPageModule;
}());

//# sourceMappingURL=tutorial.module.js.map

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocialSharePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_shared_service_shared_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_person_person__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SocialSharePage = /** @class */ (function () {
    function SocialSharePage(navCtrl, navParams, shared, pessoa, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.shared = shared;
        this.pessoa = pessoa;
        this.viewCtrl = viewCtrl;
        this.conteudo = navParams.data;
    }
    SocialSharePage.prototype.ionViewDidLoad = function () {
    };
    SocialSharePage.prototype.compartilhar = function (conteudo, rede) {
        var _this = this;
        this.pessoa.get().then(function (result) {
            conteudo.PessoaId = result.Id;
            _this.shared.SwitchShare(conteudo, rede);
        }, function (erro) {
        });
    };
    SocialSharePage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    SocialSharePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'social-share',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/social-share/social-share.html"*/'<div class="container-round">\n    <div class="box">\n       <button class="button-close2" (click)="close()"><ion-icon name="close"></ion-icon></button>\n        <div (click)="compartilhar(conteudo, \'whatsapp\')" class="icon-social">\n            <img src="../www/assets/imgs/social/whatsapp.jpg" alt="" width="40">\n        </div>\n        <div (click)="compartilhar(conteudo,\'facebook\')" class="icon-social">\n            <img src="../www/assets/imgs/social/facebook.jpg" alt="" width="40">\n        </div>\n        <div (click)="compartilhar(conteudo,\'twitter\')" class="icon-social">\n            <img src="../www/assets/imgs/social/twitter.jpg" alt="" width="40">\n        </div>\n        <div (click)="compartilhar(conteudo,\'instagram\')" class="icon-social">\n            <img src="../www/assets/imgs/social/instagram.jpg" alt="" width="40">\n        </div>\n    </div>\n</div>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/social-share/social-share.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_shared_service_shared_service__["a" /* SharedService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_shared_service_shared_service__["a" /* SharedService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], SocialSharePage);
    return SocialSharePage;
}());

//# sourceMappingURL=social-share.js.map

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocialShareMaterialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_shared_service_shared_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_person_person__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SocialShareMaterialPage = /** @class */ (function () {
    function SocialShareMaterialPage(navCtrl, navParams, shared, pessoa, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.shared = shared;
        this.pessoa = pessoa;
        this.viewCtrl = viewCtrl;
        this.conteudo = navParams.data;
    }
    SocialShareMaterialPage.prototype.ionViewDidLoad = function () {
    };
    SocialShareMaterialPage.prototype.compartilhar = function (conteudo, rede) {
        var _this = this;
        this.pessoa.get().then(function (result) {
            conteudo.PessoaId = result.Id;
            _this.shared.SwitchShare(conteudo, rede);
        }, function (erro) {
        });
    };
    SocialShareMaterialPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    SocialShareMaterialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'social-share-material',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/social-share-material/social-share-material.html"*/'<div class="container-round">\n    <div class="box">\n       <button class="button-close2" (click)="close()"><ion-icon name="close"></ion-icon></button>\n        <div (click)="compartilhar(conteudo, \'whatsappArquivo\')" class="icon-social">\n            <img src="../www/assets/imgs/social/whatsapp.jpg" alt="" width="40">\n        </div>\n        <div (click)="compartilhar(conteudo,\'facebookArquivo\')" class="icon-social">\n            <img src="../www/assets/imgs/social/facebook.jpg" alt="" width="40">\n        </div>\n        <div (click)="compartilhar(conteudo,\'twitterArquivo\')" class="icon-social">\n            <img src="../www/assets/imgs/social/twitter.jpg" alt="" width="40">\n        </div>\n        <div (click)="compartilhar(conteudo,\'instagramArquivo\')" class="icon-social">\n            <img src="../www/assets/imgs/social/instagram.jpg" alt="" width="40">\n        </div>\n    </div>\n</div>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/social-share-material/social-share-material.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_shared_service_shared_service__["a" /* SharedService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_shared_service_shared_service__["a" /* SharedService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], SocialShareMaterialPage);
    return SocialShareMaterialPage;
}());

//# sourceMappingURL=social-share-material.js.map

/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConteudoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConteudoProvider = /** @class */ (function () {
    function ConteudoProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    ConteudoProvider.prototype.getConteudo = function (token, pag, tipo) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos?TipoConteudoId=" + tipo + "&Page=" + pag;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider.prototype.getTiposConteudo = function (token) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/TiposConteudo";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider.prototype.getDetalhe = function (token, id) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider.prototype.postLike = function (conteudoId, pessoaId) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/Curtir";
        var token = this.auth.getToken();
        var body = {
            ConteudoId: conteudoId,
            PessoaId: pessoaId
        };
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                result.Titulo == "Conteúdo curtido com sucesso!" ? result.Curtiu = true : result.Curtiu = false;
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider.prototype.postShare = function (conteudoId, acervoId, pessoaId, rede) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/Compartilhar";
        var token = this.auth.getToken();
        var body = {
            ConteudoId: conteudoId,
            PessoaId: pessoaId,
            RedeSocial: rede,
            AcervoId: acervoId
        };
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], ConteudoProvider);
    return ConteudoProvider;
}());

//# sourceMappingURL=conteudo.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificationProvider = /** @class */ (function () {
    function NotificationProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    NotificationProvider.prototype.Count = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Mensagens";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                var count = 0;
                for (var i = 0; i < result.length; i++) {
                    if (!result[i].Lido) {
                        count++;
                    }
                }
                resolve(count);
            }, function (error) {
                resolve(0);
            });
        });
    };
    NotificationProvider.prototype.Get = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Mensagens";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    NotificationProvider.prototype.GetDetail = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Mensagens/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    NotificationProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], NotificationProvider);
    return NotificationProvider;
}());

//# sourceMappingURL=notificacao.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_agenda_agenda__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_conteudo_conteudo__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_person_person__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_push_push__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_social_sharing__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_shared_service_shared_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__social_share_social_share__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_comites_comites__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_quero_contribuir_quero_contribuir__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_voluntario_voluntario__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_notificacoes_notificacoes__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_participacao_participacao__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_pesquisa_pesquisa__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_programa_governo_programa_governo__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_material_institucional_material_institucional__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_programa_pontos_programa_pontos__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_android_permissions__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_push__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_local_notifications__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_storage__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__notificacoes_notificacoes_notificacoes_detalhe_notificacoes_notificacoes_detalhe__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




























var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, actionsheetCtrl, platform, modalCtrl, agenda, auth, utils, conteudo, person, notification, pushProvider, localNotifications, push, events, loadingCtrl, storage, androidPermissions) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionsheetCtrl = actionsheetCtrl;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.agenda = agenda;
        this.auth = auth;
        this.utils = utils;
        this.conteudo = conteudo;
        this.person = person;
        this.notification = notification;
        this.pushProvider = pushProvider;
        this.localNotifications = localNotifications;
        this.push = push;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.androidPermissions = androidPermissions;
        this.tab = "noticias";
        this.agendaList = [];
        this.noticiasList = [];
        this.pagina1 = 1;
        this.paginaBanner1 = 1;
        this.pagina2 = 1;
        this.start = 3;
        this.skip = 0;
        this.IsAnonimo = this.storage.get('profileName');
        this.FiltroEventos = [
            { name: 'Todos', id: 0, active: true },
            { name: 'Hoje', id: 1, active: false },
            { name: 'Essa Semana', id: 2, active: false },
            { name: 'Esse mês', id: 3, active: false },
        ];
        this.pages = [
            { title: 'Quero contribuir', component: __WEBPACK_IMPORTED_MODULE_13__pages_quero_contribuir_quero_contribuir__["a" /* QueroContribuirPage */], icon: 'logo-usd', description: null, cssClass: 'quero-contribuir', parameter: 0 },
            { title: 'Início', component: HomePage_1, icon: 'home', description: 'Notícias, eventos, agenda...', cssClass: 'iniciocss', parameter: 0 },
            { title: 'Notificações', component: __WEBPACK_IMPORTED_MODULE_15__pages_notificacoes_notificacoes__["a" /* NotificacoesPage */], icon: 'notifications', description: null, notificationsNumber: this.count, parameter: 0 },
            { title: 'Programa de Governo', component: __WEBPACK_IMPORTED_MODULE_18__pages_programa_governo_programa_governo__["a" /* ProgramaGovernoPage */], icon: 'list', description: null, parameter: 0 },
            { title: 'Seja Voluntário', component: __WEBPACK_IMPORTED_MODULE_14__pages_voluntario_voluntario__["a" /* VoluntarioPage */], icon: 'hand', description: null, parameter: 0 },
            { title: 'Minha Participação', component: __WEBPACK_IMPORTED_MODULE_16__pages_participacao_participacao__["a" /* ParticipacaoPage */], icon: 'person', description: null, parameter: 0 },
            { title: 'Pesquisa de Opinião', component: __WEBPACK_IMPORTED_MODULE_17__pages_pesquisa_pesquisa__["b" /* PesquisaPage */], icon: 'text', description: null, parameter: 0 },
            { title: 'Programa de Pontos', component: __WEBPACK_IMPORTED_MODULE_20__pages_programa_pontos_programa_pontos__["a" /* ProgramaPontosPage */], icon: 'star', description: 'Como funciona, histórico de pontos...', cssClass: 'programacss', parameter: 0 },
            { title: 'Material de Campanha', component: __WEBPACK_IMPORTED_MODULE_19__pages_material_institucional_material_institucional__["a" /* MaterialInstitucionalPage */], icon: 'images', description: 'Banners, post...', cssClass: 'materialcss', parameter: 0 },
            { title: 'Comitês', component: __WEBPACK_IMPORTED_MODULE_12__pages_comites_comites__["a" /* ComitesPage */], icon: 'people', description: null, parameter: 0 }
        ];
        // Tipo de Conteudo Noticias
        this.tipoConteudo = 2;
        this.bannerConteudo = 32;
        this.bannerLiveConteudo = 33;
        this.bannerslive = [];
        this.bannersAll = [];
        this.estados = new Array();
        this.filter = {
            cidade: '',
            estado: ''
        };
        this.selectOptEstados = {
            title: 'Selecione o estado',
            subTitle: 'Selecione o estado',
            checked: true
        };
        this.selectOptCidades = {
            title: 'Selecione a cidade',
            subTitle: 'Selecione a cidade',
            checked: true
        };
        this.loadDataUser();
        this.storage.get('profileName').then(function (val) {
            console.log('Your age is', val);
            _this.IsAnonimo = val;
        });
        events.subscribe('profile', function (profile) {
            _this.storage.get('profileName').then(function (val) {
                console.log('Your age is', val);
                _this.IsAnonimo = val;
                if (_this.IsAnonimo != 'Anônimo') {
                    _this.pushProvider.initPush();
                    var options = {
                        android: {
                            senderID: '318490138469'
                        },
                        ios: {
                            alert: 'true',
                            badge: false,
                            sound: 'true'
                        },
                        windows: {}
                    };
                    var pushObject = _this.push.init(options);
                    pushObject.on('notification').subscribe(function (notificacao) {
                        console.log('message -> ' + notificacao);
                        [];
                        if (notificacao.additionalData.foreground == true) {
                            _this.localNotifications.schedule({
                                title: 'Voluntários da Pátria',
                                text: notificacao.message,
                                icon: 'ic_notifications',
                                smallIcon: 'ic_notification_small',
                                id: notificacao.additionalData.notificacaoId,
                            });
                            // this.localNotifications.on("click", (notification, state) => {
                            //   this.navCtrl.setRoot(NotificacoesNotificacoesDetalhePage, notification.id);
                            // })
                        }
                        else if (notificacao.additionalData.foreground == false) {
                            console.log(notificacao, 'LoginNotificacaoCOMPONENTS');
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_26__notificacoes_notificacoes_notificacoes_detalhe_notificacoes_notificacoes_detalhe__["a" /* NotificacoesNotificacoesDetalhePage */], notificacao.additionalData.notificacaoId);
                        }
                    });
                    pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin' + error); });
                }
            });
        });
    }
    HomePage_1 = HomePage;
    HomePage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.loadAgenda();
            _this.loadNews();
            refresher.complete();
        }, 1000);
    };
    HomePage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, { id: id });
    };
    HomePage.prototype.optionsContribuition = function () {
        var _this = this;
        var actionSheet = this.actionsheetCtrl.create({
            cssClass: 'action-sheets-basic-page',
            buttons: [
                {
                    text: 'Colabore para mudanças',
                    icon: 'ios-thumbs-up-outline',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_14__pages_voluntario_voluntario__["a" /* VoluntarioPage */]);
                    }
                },
                {
                    text: 'Responda uma pesquisa',
                    icon: 'ios-paper-outline',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__pages_pesquisa_pesquisa__["b" /* PesquisaPage */]);
                    }
                },
                {
                    text: 'Participe enviando uma sugestão',
                    icon: 'ios-text-outline',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__pages_participacao_participacao__["a" /* ParticipacaoPage */]);
                    }
                },
                {
                    text: 'Conheça a proposta de governo',
                    icon: 'ios-search-outline',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__pages_programa_governo_programa_governo__["a" /* ProgramaGovernoPage */]);
                        __WEBPACK_IMPORTED_MODULE_18__pages_programa_governo_programa_governo__["a" /* ProgramaGovernoPage */];
                    }
                },
                {
                    role: 'cancel',
                    icon: !this.platform.is('ios') ? 'close' : null,
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    HomePage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    HomePage.prototype.loadBanner = function () {
        var _this = this;
        var auxList = [];
        return new Promise(function (resolve) {
            _this.conteudo.getConteudo(_this.auth.getToken(), _this.paginaBanner1, _this.bannerConteudo)
                .then(function (data) {
                _this.banners = data;
                _this.conteudo.getConteudo(_this.auth.getToken(), _this.paginaBanner1, _this.bannerLiveConteudo)
                    .then(function (data2) {
                    // console.log('banner live', data2)
                    _this.bannerslive = data2;
                    for (var x = 0; x < _this.bannerslive.length; x++) {
                        auxList.push(_this.bannerslive[x]);
                    }
                    for (var z = 0; z < _this.banners.length; z++) {
                        auxList.push(_this.banners[z]);
                    }
                    _this.bannersAll = auxList;
                    // this.bannerslive.push(this.banners)
                    // this.bannersAll=this.bannerslive
                    console.log('banners all', _this.bannersAll);
                });
            });
        });
    };
    HomePage.prototype.loadNews = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.conteudo.getConteudo(_this.auth.getToken(), _this.pagina1, _this.tipoConteudo)
                .then(function (data) {
                _this.noticiasList = _this.noticiasList.concat(data);
                console.log(data);
                resolve(true);
            });
        });
    };
    HomePage.prototype.loadAgenda = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.getLista(_this.auth.getToken(), _this.pagina2, 3)
                .then(function (data) {
                _this.agendaList = _this.agendaList.concat(data);
                resolve(true);
            });
        });
    };
    HomePage.prototype.doInfinite = function (infiniteScroll, tab) {
        var _this = this;
        setTimeout(function () {
            if (_this.tab == 'agenda') {
                _this.pagina2 += 1;
                _this.loadAgenda().then(function () {
                    infiniteScroll.complete();
                });
            }
            else {
                _this.pagina1 += 1;
                _this.loadNews().then(function () {
                    infiniteScroll.complete();
                });
            }
        }, 600);
    };
    HomePage.prototype.onSelectCidade = function (uf) {
        var _this = this;
        this.utils.getCidades(uf).then(function (result) { _this.cidades = result; }, function (error) { });
    };
    HomePage.prototype.getEstados = function () {
        var _this = this;
        this.utils.getEstados().then(function (result) { _this.estados = result; }, function (error) { });
    };
    HomePage.prototype.filterByCity = function (filter) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.filterCity(_this.auth.getToken(), filter, _this.start, _this.skip)
                .then(function (data) {
                if (data != null) {
                    _this.agendaList = data;
                }
                else {
                    _this.utils.showAlert("Nenhum resultado encontrado!");
                    _this.start = 2;
                }
                resolve(true);
            });
        });
    };
    HomePage.prototype.postLike = function (conteudo) {
        var _this = this;
        if (this.IsAnonimo == 'Anônimo') {
            this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
        }
        else {
            var idPost_1 = conteudo.Id;
            if (this.auth.getToken()) {
                this.person.get()
                    .then(function (response) {
                    var person = response;
                    _this.conteudo.postLike(idPost_1, person.Id)
                        .then(function (response) {
                        if (response.Curtiu && !conteudo.JaCurtiu) {
                            conteudo.Curtidas += 1;
                        }
                        else {
                            conteudo.Curtidas -= 1;
                        }
                        conteudo.JaCurtiu = response.Curtiu;
                        if (response.GanhouPonto) {
                            _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                        }
                    });
                });
            }
            else {
                console.log('nao ta logado');
            }
        }
    };
    HomePage.prototype.ionViewDidLoad = function () {
        this.loadBanner();
        this.loadNews();
        this.loadAgenda();
        this.getEstados();
    };
    HomePage.prototype.toggleClass = function (element, type) {
        var floorElements = document.getElementsByClassName("filtroeventos");
        for (var i = 0; i < floorElements.length; i++) {
            if (floorElements[i].innerText != element.currentTarget.innerText) {
                floorElements[i].classList.remove('active');
            }
        }
        for (var j = 0; j < floorElements.length; j++) {
            if (floorElements[j].innerText == element.currentTarget.innerText) {
                floorElements[j].classList.add('active');
            }
        }
        this.filterByData(type);
    };
    HomePage.prototype.filterByData = function (type) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.filterData(_this.auth.getToken(), type)
                .then(function (data) {
                _this.agendaList = data;
                resolve(true);
            });
        });
    };
    HomePage.prototype.showShare = function (conteudo) {
        if (this.IsAnonimo == 'Anônimo') {
            this.utils.showModalLogin("Você não está logado! :(", "Você precisa estar logado para continuar.", 'Faça seu login');
        }
        else {
            console.log(conteudo);
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_11__social_share_social_share__["a" /* SocialSharePage */], conteudo);
            modal.present();
        }
    };
    HomePage.prototype.loadDataUser = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.getPersonDetail().then(function (result) {
            _this.getNotificationsCount();
            _this.events.publish('profile', _this.profile);
            loading.dismiss();
        });
    };
    HomePage.prototype.getPersonDetail = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.person.get()
                .then(function (response) {
                _this.profile = response;
                console.log(response);
                resolve(true);
            }, function (error) {
                console.log(error);
            });
        });
    };
    HomePage.prototype.getNotificationsCount = function () {
        var _this = this;
        this.notification.Count().then(function (response) {
            _this.count = response;
            _this.events.publish('count', _this.count);
            console.log("sync notification: " + response);
            _this.notificationLoop();
        });
    };
    HomePage.prototype.notificationLoop = function () {
        var _this = this;
        setTimeout(function () {
            _this.notification.Count().then(function (response) {
                _this.count = response;
                _this.events.publish('count', _this.count);
                console.log("sync notification: " + response);
                _this.getNotificationsCount();
            });
        }, 30000);
    };
    HomePage.prototype.openExternal = function (url, opcao) {
        if (url.indexOf("http://") == 0 || url.indexOf("https://") == 0) {
            window.open(url, '_system', 'location=no');
            console.log('Não tem link');
        }
        else if (url == 'DetailTransmissaoPage') {
            this.navCtrl.push(url, {
                interna: opcao
            });
        }
        else {
            this.navCtrl.push(url);
        }
    };
    HomePage = HomePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/home/home.html"*/'<ion-header color="header">\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title text-center>Voluntários da Pátria</ion-title>\n      <ion-buttons end>\n\n      </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n\n    <ion-slides class="bannerItem">\n      <ion-slide *ngFor="let b of bannersAll" (click)="openExternal(b.Url, b)">\n        <div class="swiper-zoom-container">\n          <img src="{{b.Imagem}}">\n        </div>\n      </ion-slide>\n    </ion-slides>\n\n    \n    <ion-fab bottom right>\n      <button ion-fab mini color="basegreen" (click)="optionsContribuition()"><ion-icon name="add"></ion-icon></button>\n    </ion-fab>\n    <ion-segment [(ngModel)]="tab" color="primary">\n      <ion-segment-button value="noticias" (click)="skip = 0; start = 3">\n        <ion-icon name="paper"></ion-icon>Notícias\n      </ion-segment-button>\n      <ion-segment-button value="agenda" (click)="skip = 0; start = 3">\n        <ion-icon name="calendar"></ion-icon>Agenda\n      </ion-segment-button>\n    </ion-segment>\n    <div [ngSwitch]="tab">\n      <div *ngSwitchCase="\'noticias\'" class="noticias-tab-content tab-content">\n        <section padding>\n          <div>\n            <ion-card *ngFor="let conteudo of noticiasList">\n              <img (click)="openDetail(\'noticias\', conteudo.Id)" [src]="conteudo.Imagem"/>\n              <ion-card-content (click)="openDetail(\'noticias\', conteudo.Id)">\n                <small text-uppercase class="yellow-color">{{conteudo.CategoriaConteudo.Nome}}</small>\n                <ion-card-title>{{conteudo.Titulo}}</ion-card-title>\n                <div [innerHtml]="conteudo.Texto | excerpt:120"></div>\n                <ion-note>{{conteudo.Hora}}</ion-note>\n              </ion-card-content>\n              <ion-row class="footer-card">\n                <ion-col style="padding-right: 15%;">\n                  <button ion-button icon-left clear small (click)="postLike(conteudo)" [ngClass]="{\'liked\': conteudo.JaCurtiu}">\n                    <ion-icon name="heart" [ngClass]="{\'liked\': conteudo.JaCurtiu}"></ion-icon>\n                    <!-- <ion-icon name="heart" *ngIf="conteudo.Curtidas > 0" style="color:#004f99"></ion-icon> -->\n                    <div [ngClass]="{\'liked\': conteudo.JaCurtiu}">{{conteudo.Curtidas}}</div>\n                    <!-- <div *ngIf="conteudo.Curtidas > 0" style="color:#004f99">{{conteudo.Curtidas}}</div> -->\n\n                  </button>\n                </ion-col>\n                <ion-col>\n                  <button ion-button icon-right text-left clear small (click)="showShare(conteudo)">\n                    <div text-right>Compartilhe e <br/> ganhe <strong>{{conteudo.PontuacaoCompartilhamento}} pontos</strong></div>\n                    <!-- <ion-icon name="share"></ion-icon> -->\n                    <img src="assets/imgs/sharenot.png" style=" width: 14%; margin-left: 6%; ">\n                  </button>\n                </ion-col>\n              </ion-row>\n            </ion-card>\n          </div>\n        </section>\n      </div>\n      <div *ngSwitchCase="\'agenda\'" class="agenda-tab-content tab-content">\n        <section padding class="agendaSection">\n\n          <div class="list-cards">\n            <ion-card (click)="openDetail(\'evento\', evento.Id)" *ngFor="let evento of agendaList" >\n              <div class="row">\n                <div class="hour">{{evento.Data | date: \'dd\'}} <strong>{{evento.Data | date: \'MMM\'}}</strong> <small *ngIf="evento.Hora"> {{evento.Hora}}h </small></div>\n                <div class="col">\n                  <ion-card-content>\n                    <ion-card-title>{{evento.Titulo}}</ion-card-title>\n                    {{evento.Local}}\n                  </ion-card-content>\n                </div>\n              </div>\n            </ion-card>\n\n            <div class="wrapError" *ngIf="(agendaList).length === 0">\n              <i class="fa fa-smile-o" aria-hidden="true"></i>\n              <p>Aguarde. Em breve estará disponível a agenda de eventos e encontros.</p>\n            </div>\n          </div>\n        </section>\n      </div>\n      <ion-infinite-scroll (ionInfinite)="doInfinite($event, tab)">\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n      </ion-infinite-scroll>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/home/home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_9__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_10__providers_shared_service_shared_service__["a" /* SharedService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_agenda_agenda__["a" /* AgendaProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_21__providers_notificacao_notificacao__["a" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_push_push__["a" /* PushProvider */],
            __WEBPACK_IMPORTED_MODULE_24__ionic_native_local_notifications__["a" /* LocalNotifications */],
            __WEBPACK_IMPORTED_MODULE_23__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_25__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_22__ionic_native_android_permissions__["a" /* AndroidPermissions */]])
    ], HomePage);
    return HomePage;
    var HomePage_1;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__conteudo_conteudo__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_app_availability__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SharedService = /** @class */ (function () {
    function SharedService(SocialSharing, platform, _alertCtrl, conteudoprovider, appAvailability, utils) {
        this.SocialSharing = SocialSharing;
        this.platform = platform;
        this._alertCtrl = _alertCtrl;
        this.conteudoprovider = conteudoprovider;
        this.appAvailability = appAvailability;
        this.utils = utils;
        console.log('Hello SharedServiceProvider Provider');
    }
    SharedService.prototype.SwitchShare = function (conteudo, rede) {
        switch (rede) {
            case 'facebook':
                this.shareViaFacebook(conteudo);
                break;
            case 'twitter':
                this.shareViaTwitter(conteudo);
                break;
            case 'instagram':
                this.shareViaInstagram(conteudo);
                break;
            case 'whatsapp':
                this.shareViaWhatsApp(conteudo);
                break;
            case 'facebookArquivo':
                this.shareViaFacebookArquivo(conteudo);
                break;
            case 'twitterArquivo':
                this.shareViaTwitterArquivo(conteudo);
                break;
            case 'instagramArquivo':
                this.shareViaInstagramArquivo(conteudo);
                break;
            case 'whatsappArquivo':
                this.shareViaWhatsAppArquivo(conteudo);
                break;
            default:
                this.sharePicker(conteudo);
                break;
        }
    };
    SharedService.prototype.shareViaEmail = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.canShareViaEmail()
                .then(function () {
                _this.SocialSharing.shareViaEmail(conteudo.message + ' ' + conteudo.UrlExterna, conteudo.subject, conteudo.sendTo)
                    .then(function (data) {
                    console.log('Shared via Email');
                })
                    .catch(function (err) {
                    console.log('Not able to be shared via Email');
                });
            })
                .catch(function (err) {
                console.log('Sharing via Email NOT enabled');
            });
        });
    };
    SharedService.prototype.shareViaFacebook = function (conteudo) {
        var _this = this;
        if (this.platform.is('android')) {
            conteudo.Titulo += '#APPVoluntariosdaPatria17';
            this.SocialSharing.shareViaFacebookWithPasteMessageHint(conteudo.Titulo, conteudo.Imagem, conteudo.UrlExterna, conteudo.Titulo + conteudo.UrlExterna)
                .then(function (data) {
                console.log('data', data);
                _this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'facebook')
                    .then(function (response) {
                    setTimeout(function () {
                        _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                    }, 5000);
                });
            }).catch(function (error) {
                console.log('Deu RUim', error);
            });
        }
        else {
            this.SocialSharing.shareViaFacebook(conteudo.Titulo + ' #APPVoluntariosdaPatria17', conteudo.Imagem, conteudo.UrlExterna)
                .then(function (data) {
                _this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'facebook')
                    .then(function (response) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                });
            }).catch(function (error) {
                console.log('caiu no erro', error);
            });
        }
    };
    SharedService.prototype.shareViaFacebookArquivo = function (conteudo) {
        var _this = this;
        this.SocialSharing.shareViaFacebook(conteudo.Titulo + '#APPVoluntariosdaPatria17', conteudo.ArquivoUrl, conteudo.ImagemUrl)
            .then(function (data) {
            // Sharing via email is possible
            _this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'facebook')
                .then(function (response) {
                if (_this.platform.is('ios')) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                }
                else {
                    setTimeout(function () {
                        _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                    }, 5000);
                }
            }, function (error) {
                console.log('não tem facebook');
            });
            console.log(data, 'funcionando');
        }, function (error) {
            console.log('nao funciona');
        });
    };
    SharedService.prototype.shareViaInstagram = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.shareViaInstagram(conteudo.Titulo + ' ' + conteudo.UrlExterna, conteudo.Imagem)
                .then(function (data) {
                _this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'instagram')
                    .then(function (response) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                });
                console.log('Shared via shareViaInstagram');
            })
                .catch(function (err) {
                console.log('Was not shared via Instagram');
            });
        });
    };
    SharedService.prototype.shareViaInstagramArquivo = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.shareViaInstagram(conteudo.Titulo + ' ' + conteudo.ImagemUrl, conteudo.ArquivoUrl)
                .then(function (data) {
                _this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'instagram')
                    .then(function (response) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                });
                console.log('Shared via shareViaInstagram');
            })
                .catch(function (err) {
                console.log('Was not shared via Instagram');
            });
        });
    };
    SharedService.prototype.sharePicker = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.share(conteudo.Titulo, conteudo.UrlExterna, conteudo.Imagem)
                .then(function (data) {
                console.log('Shared via SharePicker');
            })
                .catch(function (err) {
                console.log('Was not shared via SharePicker');
            });
        });
    };
    SharedService.prototype.shareViaTwitter = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.canShareVia('twitter', conteudo.Titulo + '#APPVoluntariosdaPatria17' + ' ' + conteudo.UrlExterna, conteudo.Imagem)
                .then(function (data) {
                _this.SocialSharing.shareViaTwitter(conteudo.Titulo, conteudo.Imagem)
                    .then(function (data) {
                    _this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'twitter')
                        .then(function (response) {
                        // setTimeout(() => {
                        _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                        // }, 5000);
                    }, function (error) {
                        console.log('não tem twitter');
                    });
                    console.log('Shared via Twitter');
                })
                    .catch(function (err) {
                    console.log('Was not shared via Twitter');
                });
            });
        })
            .catch(function (err) {
            console.log('Not able to be shared via Twitter');
        });
    };
    SharedService.prototype.shareViaTwitterArquivo = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.canShareVia('twitter', conteudo.Titulo + ' ' + conteudo.UrlExterna, conteudo.ImagemUrl)
                .then(function (data) {
                _this.SocialSharing.shareViaTwitter(conteudo.Titulo, conteudo.Imagem)
                    .then(function (data) {
                    _this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'twitter')
                        .then(function (response) {
                        // setTimeout(() => {
                        _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                        // }, 5000);
                    }, function (error) {
                        console.log('não tem twitter');
                    });
                    console.log('Shared via Twitter');
                })
                    .catch(function (err) {
                    console.log('Was not shared via Twitter');
                });
            });
        })
            .catch(function (err) {
            console.log('Not able to be shared via Twitter');
        });
    };
    SharedService.prototype.shareViaWhatsApp = function (conteudo) {
        var _this = this;
        this.SocialSharing.shareViaWhatsApp(conteudo.Titulo, conteudo.Imagem, conteudo.UrlExterna)
            .then(function (data) {
            console.log(data, 'sucesso');
            _this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'whatsapp')
                .then(function (response) {
                // setTimeout(() => {
                _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                // }, 5000);
            }, function (error) {
                console.log('não tem whats');
            });
            console.log('Shared via Twitter');
        }, function (error) {
            console.log(error, 'não tem whats');
        });
    };
    SharedService.prototype.shareViaWhatsAppArquivo = function (conteudo) {
        var _this = this;
        this.SocialSharing.shareViaWhatsApp(conteudo.Titulo, conteudo.ArquivoUrl.conteudo.ImagemUrl)
            .then(function (data) {
            console.log(data, 'sucesso');
            _this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'whatsapp')
                .then(function (response) {
                // setTimeout(() => {
                _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                // }, 5000);
            }, function (error) {
                console.log('não tem whats');
            });
            console.log('Shared via Twitter');
        }, function (error) {
            console.log(error, 'não tem whats');
        });
    };
    SharedService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_app_availability__["a" /* AppAvailability */],
            __WEBPACK_IMPORTED_MODULE_4__utils_utils__["a" /* UtilsProvider */]])
    ], SharedService);
    return SharedService;
}());

//# sourceMappingURL=shared-service.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParticipacaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_participacao_participacao__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ParticipacaoPage = /** @class */ (function () {
    //  erro: any;
    function ParticipacaoPage(navCtrl, navParams, utils, participacao, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.utils = utils;
        this.participacao = participacao;
        this.loadingCtrl = loadingCtrl;
        this.nadaEncontrado = false;
    }
    ParticipacaoPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 1000);
    };
    ParticipacaoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad ParticipacaoPage');
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.participacao.getAll().then(function (result) {
            _this.lista = result;
            if (_this.lista.length == 0) {
                _this.nadaEncontrado = true;
            }
            loading.dismiss();
        }, function (error) {
            // let erro = error;
            _this.lista = error.status;
            console.log(_this.lista);
            loading.dismiss();
        });
    };
    ParticipacaoPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, id);
    };
    ParticipacaoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-participacao',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/participacao/participacao.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Minha Participação</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <h4 class="bold">Colabore com a mudança do Brasil</h4>\n  <div class="block-text">\n    <p>Mostre suas ideias e os problemas que acontecem em sua cidade.</p>\n  </div>\n\n  <button ion-button full text-uppercase (click)="openDetail(\'form-participacao\', 1)">Enviar Participação</button>\n\n  <div class="block" margin-top>\n    <h6 class="bold" *ngIf="this.lista != 400">Minhas Contribuições</h6>\n  <h6 class="bold" *ngIf="this.lista == 400" style="text-align: center;">Você ainda não tem participações cadastradas.</h6>\n\n    <ion-list>\n      <ion-card (click)="openDetail(\'participacao-detalhe\', part.Id)" *ngFor="let part of lista">\n        <ion-card-content class="flex-card">\n          <div>\n            <small text-uppercase class="yellow-color">{{part.CategoriaNome}}</small>\n            <ion-card-title>{{part.Local}}</ion-card-title>\n            <ion-note>{{part.DataCompleta}}</ion-note>\n          </div>\n          <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n        </ion-card-content>\n      </ion-card>\n\n      <div class="wrapError" *ngIf="nadaEncontrado">\n        <i class="fa fa-frown-o" aria-hidden="true"></i>\n        <p>Ainda não cadastrou uma participação? Não perca tempo e colabore com a mudança do Brasil.</p>\n      </div>\n    </ion-list>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/participacao/participacao.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_participacao_participacao__["a" /* ParticipacaoProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */]])
    ], ParticipacaoPage);
    return ParticipacaoPage;
}());

//# sourceMappingURL=participacao.js.map

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PesquisaPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PesquisaModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_loading_loading_controller__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_pesquisa_pesquisa__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PesquisaPage = /** @class */ (function () {
    function PesquisaPage(navCtrl, navParams, modalCtrl, loadingCtrl, pesquisa, utils) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.pesquisa = pesquisa;
        this.utils = utils;
    }
    PesquisaPage.prototype.ionViewDidLoad = function () {
        this.getData();
        console.log('ionViewDidLoad PesquisaPage');
    };
    PesquisaPage.prototype.ionViewDidEnter = function () {
        this.getData();
    };
    PesquisaPage.prototype.presentProfileModal = function (pesquisa) {
        this.getData();
        if (pesquisa.Finalizada) {
            if (pesquisa.ExibirGrafico) {
                this.getDetail(pesquisa.Id);
            }
            else {
                if (pesquisa.JaVotou) {
                    this.utils.showModalSucesso("Você já respondeu essa pesquisa.", null, "OK", null);
                }
                else {
                    this.utils.showModalSucesso("Essa pesquisa foi finalizada.", null, "OK", null);
                }
            }
        }
        else {
            if (pesquisa.JaVotou) {
                this.utils.showModalSucesso("Você já respondeu essa pesquisa.", null, "OK", null);
            }
            else {
                var profileModal = this.modalCtrl.create(PesquisaModalPage, pesquisa, {
                    showBackdrop: true,
                    cssClass: 'modal-box'
                });
                profileModal.present();
            }
        }
    };
    PesquisaPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getData();
            refresher.complete();
        }, 1000);
    };
    PesquisaPage.prototype.doInfinite = function (infiniteScroll) {
        console.log('Begin async operation');
        setTimeout(function () {
            infiniteScroll.complete();
        }, 500);
    };
    PesquisaPage.prototype.getData = function () {
        var _this = this;
        console.log('merda1');
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        setTimeout(function () {
            _this.pesquisa.findAll().then(function (result) {
                loading.dismiss();
                _this.listaPesquisa = result;
                console.log(result);
            }, function (error) {
                loading.dismiss();
                console.log(error);
            });
            console.log('passando mto aqui');
        }, 1000);
    };
    PesquisaPage.prototype.getDetail = function (id) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.pesquisa.findById(id).then(function (result2) {
            loading.dismiss();
            console.log(result2);
            _this.navCtrl.push('pesquisa-resposta', result2);
            console.log(result2);
        }, function (error2) {
            loading.dismiss();
            console.log(error2);
        });
    };
    PesquisaPage.prototype.back = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
    };
    PesquisaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-pesquisa',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/pesquisa/pesquisa.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Pesquisa de Opinião</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <ion-list margin-top>\n    <ion-card (click)="presentProfileModal(p.Pesquisa)" *ngFor="let p of listaPesquisa">\n      <ion-card-content class="flex-card">\n        <div>\n          <small class="yellow-color" text-uppercase>{{p.Tema}}</small>\n          <ion-card-title>{{p.Pesquisa.Pergunta}}</ion-card-title>\n        </div>\n        <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n      </ion-card-content>\n    </ion-card>\n    <!-- <ion-card (click)="presentProfileModal()">\n      <ion-card-content class="flex-card">\n        <div>\n          <small class="yellow-color" text-uppercase>Segurança Pública</small>\n          <ion-card-title>Você é a favor ou contra o porte de armas para toda população?</ion-card-title>\n        </div>\n        <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n      </ion-card-content>\n    </ion-card>\n    <ion-card (click)="presentProfileModal()">\n      <ion-card-content class="flex-card">\n        <div>\n          <small class="yellow-color" text-uppercase>Pesquisa de Opinião</small>\n          <ion-card-title>Você gostou do novo site do Jair Bolsonaro?</ion-card-title>\n        </div>\n        <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n      </ion-card-content>\n    </ion-card>\n    <ion-card (click)="presentProfileModal()">\n      <ion-card-content class="flex-card">\n        <div>\n          <small class="yellow-color" text-uppercase>Pesquisa de Opinião</small>\n          <ion-card-title>O que você achou da plataforma de doação MaisQueVoto?</ion-card-title>\n        </div>\n        <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n      </ion-card-content>\n    </ion-card> -->\n  </ion-list>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/pesquisa/pesquisa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_pesquisa_pesquisa__["a" /* PesquisaProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */]])
    ], PesquisaPage);
    return PesquisaPage;
}());

var PesquisaModalPage = /** @class */ (function () {
    function PesquisaModalPage(params, navCtrl, loadingCtrl, pesquisa, utils) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.pesquisa = pesquisa;
        this.utils = utils;
        this.pesquisaDetail = params.data;
        console.log('objeto: ', params.data);
    }
    PesquisaModalPage.prototype.close = function () {
        this.navCtrl.pop();
    };
    PesquisaModalPage.prototype.answerQuestion = function (status) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.pesquisa.post(status).then(function (result) {
            loading.dismiss();
            _this.utils.showModalSucesso(result.Titulo, result.Mensagem, "OK", _this.goToPesquisa());
        }, function (error) {
            loading.dismiss();
            _this.utils.showModalSucesso("Você já respondeu essa pesquisa.", null, "OK", _this.goToPesquisa());
            console.log(error);
        });
    };
    PesquisaModalPage.prototype.getData = function () {
        var _this = this;
        console.log('merda1');
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        setTimeout(function () {
            _this.pesquisa.findAll().then(function (result) {
                loading.dismiss();
                _this.listaPesquisa = result;
                console.log(result);
            }, function (error) {
                loading.dismiss();
                console.log(error);
            });
            console.log('passando mto aqui');
        }, 1000);
    };
    PesquisaModalPage.prototype.goToPesquisa = function () {
        var _this = this;
        this.pesquisa.findAll().then(function (result) {
            _this.listaPesquisa = result;
            console.log(result);
        }, function (error) {
            console.log(error);
        });
        this.navCtrl.pop();
    };
    PesquisaModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'modal-pesquisa',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/pesquisa/pesquisa-modal.html"*/'<div class="container-round" text-center>\n        <button class="button-close" (click)="close()"><ion-icon name="close"></ion-icon></button>\n        <header>\n          <strong text-uppercase>Pesquisa de Opinião</strong>\n          <p>Responda e ganhe {{pesquisaDetail.Pontuacao}} pontos</p>\n        </header>\n        <div class="box">\n          <small class="yellow-color" text-uppercase>Tema</small>\n          <h4 class="bold">{{pesquisaDetail.Tema}}</h4>\n          <div class="block-text">\n            <p>{{pesquisaDetail.Pergunta}}</p>\n          </div>\n          <div class="options-modal" >\n            <button class="negative" (click)="answerQuestion(pesquisaDetail.negative.Id)">\n              <div class="ico">\n                  <ion-icon name="close"></ion-icon>\n              </div>\n              <small text-uppercase>{{pesquisaDetail.negative.Resposta}}</small>\n            </button>\n            <button class="agree" (click)="answerQuestion(pesquisaDetail.agree.Id)">\n              <div class="ico">\n                  <ion-icon name="checkmark"></ion-icon>\n              </div>\n              <small text-uppercase>{{pesquisaDetail.agree.Resposta}}</small>\n            </button>\n          </div>\n        </div>\n    </div>\n    \n    '/*ion-inline-end:"/Users/usuario/Desktop/projetos/mypolis_bolsonaro/src/pages/pesquisa/pesquisa-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_pesquisa_pesquisa__["a" /* PesquisaProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */]])
    ], PesquisaModalPage);
    return PesquisaModalPage;
}());

//# sourceMappingURL=pesquisa.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParticipacaoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ParticipacaoProvider = /** @class */ (function () {
    function ParticipacaoProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    ParticipacaoProvider.prototype.getCategorias = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Participacoes/Categorias";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ParticipacaoProvider.prototype.post = function (solicitacao) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Participacoes/Enviar";
        var header = { "headers": { "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, solicitacao, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ParticipacaoProvider.prototype.getAll = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Participacoes/Minhas";
        var header = { "headers": { "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ParticipacaoProvider.prototype.get = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Participacoes/" + id;
        var header = { "headers": { "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ParticipacaoProvider.prototype.put = function (texto, id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Participacoes/Interagir";
        var header = { "headers": { "Authorization": "Bearer " + token } };
        var body = {
            Id: id,
            Texto: texto
        };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, header).map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ParticipacaoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], ParticipacaoProvider);
    return ParticipacaoProvider;
}());

//# sourceMappingURL=participacao.js.map

/***/ })

},[271]);
//# sourceMappingURL=main.js.map